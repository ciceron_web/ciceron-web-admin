var fakeData = {
  folders: [
    {
      name: '폴더1'
    },
    {
      name: '폴더2'
    }
  ],
  translatedCard: [
    {
      paragraph_seq: 1,
      paragraph_comment: 'paragraph_comment - 1 | a awoiejfoaiwj efjaw efjawlekfjlw kjlksdjflk jlskej flksjel fkjlsekjflksjel fse fsdf',
      sentences: [
        {
          original_text: 'original_text - 1-1 | owi ejfoijaewo ifjao wia weifjoa iwej foiajweoif jaoweijfo aiwjeof ijawoeijf oawieof jaowiejf awe fefiw jeofj welkf jwekjf lwkejf lkwef ',
          sentence_comment: 'sentence_comment - 1-1 | aiwjefoijaweoijfaw ea weiofjaowie jfaoiwejf',
          sentence_seq: 1,
          translated_text: 'translated_text - 1-1 | qwerqwerqwerqwre'
        },
        {
          original_text: 'original_text - 1-2 | owi ejfoijaewo ifjao wiefiw jeofj welkf jwekjf lwkejf lkwef ',
          sentence_comment: 'sentence_comment - 1-2 | qwerqwer',
          sentence_seq: 2,
          translated_text: 'translated_text - 1-2 | qwerqwerqwerqwre'
        }
      ]
    },
    {
      paragraph_seq: 2,
      paragraph_comment: null,
      sentences: [
        {
          original_text: 'original_text - 2-1 | owi ejfoijaewo ifjao wiefiw jeofj welkf jwekjf lwkejf lkwef ',
          sentence_comment: 'sentence_comment - 2-1 | aiwjefoijaweoijfaw ea weiofjaowie jfaoiwejf',
          sentence_seq: 1,
          translated_text: 'translated_text - 2-1 | qwerqwerqwerqwre'
        },
        {
          original_text: 'original_text - 2-2 | owi ejfoijaewo ifjao wiefiw jeofj welkf jwekjf lwkejf lkwef ',
          sentence_comment: 'sentence_comment - 2-2 | aiwjefoijaweoijfaw ea weiofjaowie jfaoiwejf',
          sentence_seq: 2,
          translated_text: 'translated_text - 2-2 | qwerqwerqwerqwre'
        }
      ]
    }
  ],
  tickets: [
    {
      id: 1,
      userName: '김가라',
      userImagePath: '프사경로',
      leftTime: 500,
      originLangCode: 1,
      originLang: '한국어',
      transLangCode: 2,
      transLang: '영어',
      subjectCode: 2,
      subject: '과학',
      contentTypeCode: 1,
      contentType: '출판물',
      wordCount: 300,
      context: '이거슨 가라 데이터',
      data: '2016. 05. 00 00:00:00',
      accepter: [
        {
          name: '번역가1',
          hitRatio: 50
        },
        {
          name: '번역가2',
          hitRatio: 100
        }
      ]
    },
    {
      id: 2,
      userName: '홍길동',
      userImagePath: '프사경로',
      leftTime: 500,
      originLangCode: 1,
      originLang: '한국어',
      transLangCode: 2,
      transLang: '영어',
      wordCount: 300,
      context: '아버지',
      data: '2016. 05. 00 00:00:00',
      accepter: [
        {
          name: '번역가1',
          hitRatio: 50
        },
        {
          name: '번역가2',
          hitRatio: 100
        }
      ]
    }
  ],
  cards: [
    {
      id: 1,
      name: 'name',
      userImage: '/img/flags/round/1.png',
      userAnswer: 'userAnswer',
      originLangCode: 1,
      originLang: 'ko',
      transLangCode: 2,
      transLang: 'en',
      contentTypeCode: 1,
      contentType: 'asdf',
      subjectCode: 1,
      subject: 'asdf',
      wordCount: 123,
      date: '2016. 01. 01. 00:00:00'
    },
    {
      id: 2,
      name: 'name',
      userImage: '/img/flags/round/1.png',
      userAnswer: 'userAnswer',
      originLangCode: 1,
      originLang: 'ko',
      transLangCode: 2,
      transLang: 'en',
      contentTypeCode: 1,
      contentType: 'asdf',
      subjectTypeCode: 1,
      subjectType: 'asdf',
      wordCount: 123,
      date: '2016. 01. 01. 00:00:00'
    }
  ],
  card: {
    id: 1,
    userName: '김가라',
    userImagePath: '프사경로',
    leftTime: 500,
    originLangCode: 1,
    originLang: '한국어',
    transLangCode: 2,
    transLang: '영어',
    subjectCode: 2,
    subject: '과학',
    contentTypeCode: 1,
    contentType: '출판물',
    wordCount: 300,
    context: '이거슨 가라 데이터',
    accepter: [
      {
        name: '번역가1',
        hitRatio: 50
      },
      {
        name: '번역가2',
        hitRatio: 100
      }
    ]
  }
};

