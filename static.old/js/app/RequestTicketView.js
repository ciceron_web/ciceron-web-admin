define(['jquery', 'eventemitter', 'app/RequestService', 'app/AuthService', 'app/TemplateMap', 'app/BeTranslatorView', 'app/I18nResolver', 'jquery-ui/magnific-popup', "jquery-ui/datepicker", "jquery-ui/spinner", "jquery-ui/selectmenu"], function($, EventEmitter, requestService, authService, templateMap, beTranslatorView) {

	var oBeTranslatorView = new beTranslatorView();
	var authService = authService;
	var i18nResolver = i18nResolver;
	
	function RequestTicketView($elContainer, htOption) {
		this.$elContainer = $elContainer;
		this.$elItem;		

		this._htOption = $.extend({}, {
			hasTranslateNow : false,
			hasDelete : false,
			hasAddToCart : false,
			hasCancelRequest: false,
			hasDeleteFromCart : false,
			hasTranslatingLabel : false,
			hasCompleteRequest : false,
			canToggleOriginalText : false
		}, htOption);

		this._htOption.data.originalLangPicPath = templateMap.resourceMapperLangFlag(this._htOption.data.originalLangIndex);
		this._htOption.data.targetLangPicPath = templateMap.resourceMapperLangFlag(this._htOption.data.targetLangIndex);
		this._htOption.data.formatIconPath = templateMap.resourceMapperFormatIcon(this._htOption.data.formatIndex);
		this._htOption.data.subjectIconPath = templateMap.resourceMapperSubjectIcon(this._htOption.data.subjectIndex);

		if(this._htOption.rawData && this._htOption.rawData.request_transStartTime && !this._htOption.data.translatingProcess)
			this._htOption.data.translatingProcess = this.getTranslatingProcess();
			
		this.loadTemplate({
			callback : function() {			
				this.initAfterLoaded();
			}.bind(this)
		});
	}
	

	RequestTicketView.prototype.loadTemplate = function(htParam) {
		templateMap.get("RequestTicketView", function(sTemplate, i18n) {
			this._htOption.i18n = $.extend({}, i18n);
			
			// this._htOption.data 를 이용해서 i18n 모든항목을 한번 더 컴파일한다.
			for(var sKey in this._htOption.i18n) {
				this._htOption.i18n[sKey] = Handlebars.compile(this._htOption.i18n[sKey])(this._htOption.data);
			}
				
			// 맥락 엔터 교체 
			if(this._htOption.data.contextText) 
				this._htOption.data.contextText = this._htOption.data.contextText.replace(/\n/g, "<br/>");	
			
			// 원문 엔터 교체 
			if(this._htOption.data.originalText) 
				this._htOption.data.originalText = this._htOption.data.originalText.replace(/\n/g, "<br/>");
			
			this._template = Handlebars.compile(sTemplate);
			htParam.callback();		
		}.bind(this));
	};

	RequestTicketView.prototype.initAfterLoaded = function() {
		// 값에따라 동적으로 i18n을 적용해야하는 값을 적용한다.
		this._htOption.data.messages = this._htOption.data.messages || {};
		this._htOption.data.messages.originalLang = this._htOption.i18n[templateMap.getKeyForLanguage(this._htOption.data.originalLangIndex)];
		this._htOption.data.messages.targetLang = this._htOption.i18n[templateMap.getKeyForLanguage(this._htOption.data.targetLangIndex)];	
		this._htOption.data.messages.format = this._htOption.i18n[templateMap.getKeyForFormat(this._htOption.data.formatIndex)];
		this._htOption.data.messages.subject = this._htOption.i18n[templateMap.getKeyForSubject(this._htOption.data.subjectIndex)];
			
			
		this._htOption.data.remainingMinutes = this._htOption.data.remainingHours * 60;
		this._htOption.data.remainingHours = parseInt(this._htOption.data.remainingHours);
		this._htOption.data.remainingMinutes = parseInt(this._htOption.data.remainingMinutes);

		this.buildUpElements();
		
		this.initEvents();
	};
	
	RequestTicketView.prototype.buildUpElements = function() {
		var $elRenderedHtml = $(this._template(this._htOption));
		console.log(this._htOption);
		if(this._htOption.isAppending) {
			this.$elContainer.append($elRenderedHtml);
		} else {
			this.$elContainer.html($elRenderedHtml);
		}
		
		this.$elItem = $elRenderedHtml;
		
		if(this._htOption.isConfirmTicket === true) {
			this.$elContainer.find(".actionCol").remove();
			this.$elContainer.find(".contentCol").css("width", "480px");
		};
		
		this.$elTranslateNow = $elRenderedHtml.find(".actionBtn.translate");
		
		// 					hasCancelRequest: true,
		//			hasTranslateNow: false, // Todo authService 를 통해 결정 
		if(this._htOption.hasTranslateNow === false || (this._htOption.rawData && this._htOption.rawData.request_status === 1)) {
			this.$elItem.find(".actionBtn.translate").remove();
		}		
		if(this._htOption.hasCancelRequest === false) {	
			this.$elItem.find(".actionBtn.delete").remove();
		}
		if(this._htOption.hasAddToCart === false) {
			this.$elItem.find(".actionBtn.cart").remove();
		}
		if(this._htOption.hasDeleteFromCart === false) {
			this.$elItem.find(".actionBtn.deleteFromCart").remove();
		}
		
		if(this._htOption.rawData && this._htOption.rawData.request_status === 1) {
			this.$elItem.find(".translating").show();		
		} else {
			this.$elItem.find(".translating").remove();	
		}

		if(this._htOption.hasTranslatingLabel === false) {
			this.$elItem.find(".translating").remove();
		} else {
			this.$elItem.find(".actionCol").remove();
		}
		
		if(this._htOption.hasCompleteRequest === false) {
			this.$elItem.find(".makeTitleContainer").remove();
		}

		// 번역중일 경우 진행율 차트 표시 
		if(this._htOption.data.translatingProcess) {
			var $ppc = this.$elContainer.find('.progress-pie-chart'),
			percent = parseInt($ppc.data('percent')),
			deg = 360*percent/100;
			if (percent > 50) {
			$ppc.addClass('gt-50');
			}
			this.$elContainer.find('.ppc-progress-fill').css('transform','rotate('+ deg +'deg)');			
		}
		
		
		if(!this._htOption.canToggleOriginalText) {
			this.$elContainer.find(".originalTextContainer").remove();
		}
		
		if(this._htOption.isExpired) {
			this.$elItem.find(".header.main").css("background-color", "#FF8C8C");
			this.$elItem.find(".remainingHours .content span").html(this._htOption.i18n.EXPIRED);
		} else {
			this.$elItem.find(".actionBtn.resubmit").remove(); 
		}
		
		if(this._htOption.data.remainingHours === 0) {
			this.$elItem.find(".remainingHours .hour").remove(); 
		} else {
			this.$elItem.find(".remainingHours .minute").remove(); 
		}

	};
	
	RequestTicketView.prototype.openResubmitPopup = function() {

		var resubmitPopupTemplate = 
		'<div class="resubmitPopup">\
			<p>{{i18n.ENTER_RESUBMIT_DEADLINE}}</p>\
			<div class="duedatePicker"></div>\
			<div class="dueLabelContainer">\
				<span class="label">{{i18n.MAKE_REQUEST_DEADLINE}}</span><br/>\
				<span class="dueyearLabel">00</span> / <span class="duemonthLabel">00</span> / <span class="duedateLabel">00</span><br/>\
				<input class="duehourPicker">:\
				<input class="dueminutePicker">\
			</div>\
			<button class="sendBtn noshow">{{i18n.RESUBMIT_DEADLINE}}</button>\
		</div>';
	
		var sRenderedHtml = Handlebars.compile(resubmitPopupTemplate)({
			data: this._htOption,
			i18n: this._htOption.i18n
		});
		
		$.magnificPopup.open({
			items: [{
				src: $(sRenderedHtml),// Dynamically created element
				type: 'inline'
			}]
		});
		
		this.initPopupEvents();		
	};
	
	RequestTicketView.prototype.initPopupEvents = function() {
		this.resubmit = {};
		
		// 마감일 섹션
		$( ".duedatePicker" ).datepicker({ 
			minDate: 0,
			onSelect: $.proxy(function(self, dateText) {
				//self.htData.duedate = dateText;
				self.resubmit.duedate = dateText;
				
				var month = dateText.split('/')[0];
				var date = dateText.split('/')[1];
				var year = dateText.split('/')[2];
						
				self.resubmit.month = month;
				self.resubmit.date = date;
				self.resubmit.year = year;
		
				$(".dueyearLabel").html(year);
				$(".duemonthLabel").html(month);
				$(".duedateLabel").html(date);
				
				// SUBMIT 버튼 활성화 시도
				if(self.resubmit.duedate) {
					$(".resubmitPopup .sendBtn").removeClass("noshow");
				}
			}, undefined, this)
		});
		
		$( ".duehourPicker" ).spinner({
		  min: 0,
		  max: 23,
		  step: 1,
		  value: 0,
		  numberFormat: "n",
		  stop: $.proxy(function(self, event, ui) {
				self.resubmit.duehour = this.value;
		  }, undefined, this)
		});
		$( ".duehourPicker" ).spinner("value", 23);
		this.resubmit.duehour = $( ".duehourPicker" ).val();

		$( ".dueminutePicker" ).spinner({
		  min: 0,
		  max: 59,
		  step: 1,
		  numberFormat: "n",
		  stop: $.proxy(function(self,  event, ui) {
				self.resubmit.dueminute = this.value;
		  }, undefined, this)
		});
		$( ".dueminutePicker" ).spinner("value", 59);
		this.resubmit.dueminute = $( ".dueminutePicker" ).val();

		if(this.resubmit.duedate && this.resubmit.duehour && this.resubmit.dueminute) {
			$(".resubmitPopup .sendBtn").removeClass("noshow");
		}
		
		$(".resubmitPopup .sendBtn").on('click', function() {
			// delta time 을 초 단위로 계산
			var oNow = new Date();
			// 왜 그런진 모르겠지만, Date constructor에서 month 범위는 0~11이라고 함....
			var oDeadline = new Date(this.resubmit.year, this.resubmit.month-1, this.resubmit.date, this.resubmit.duehour, this.resubmit.dueminute, 0, 0);
			
			var seconds = Math.abs(oDeadline - oNow) / 1000;

			requestService.resubmitRequest({
				request_id: this._htOption.rawData.request_id,
				user_addtionalTime: parseInt(seconds), // 초 단위인데 30분을 입력
				success: function(data) {
					this._htOption.resubmitSuccess(data, this);
				}.bind(this)
			});

		}.bind(this));
	};	
	
	RequestTicketView.prototype.initEvents = function() {
		this.$elItem.find(".actionBtn.resubmit").on("click", function(){	
			this.openResubmitPopup();
		}.bind(this));

		if(this._htOption.rawData) {
			this.$elPopup = $('<div class="originalTextPopup"><p style="float:right">'+this._htOption.rawData.request_registeredTime+'</p><h2 style="margin-top:0">'+this._htOption.data.clientName+'<span> '+this._htOption.i18n.TICKET_SOMEONES_REQUEST_CONTENT+'</span></h2><p class="originalText">'+this._htOption.data.originalText+'</p><div class="logoContainer"><img class="logo" src="/static/img/1.1.1logo_web_n.png"/></div></div>');
		}
		
		if(this._htOption.canToggleOriginalText) {
			this.$elItem.on("click", $.proxy(function(self, evt) {
				if(evt.target.tagName !== "BUTTON") {
					self.$elItem.find(".originalTextContainer").slideToggle(300);
				}
			}, undefined, this));
			
			this.$elItem.find(".originalTextContainer .originalTextFooter button").on("click", function() {
				$.magnificPopup.open({
					items: [{
						src: this.$elPopup, // Dynamically created element
						type: 'inline'
					}]
				});			
			}.bind(this));
	/*

    */
		}
		
		
		this.$elItem.on("click", ".makeTitle", function() {
			var sTitle = this.$elItem.find(".makeTitleContainer textarea").val();
			
			if(sTitle && sTitle.length > 0) {
				requestService.setTitleToCompletedRequest({
					request_id: this._htOption.rawData.request_id,
					title_text: sTitle,
					success: function() {
						location.href = "/donerequests";
					}
				});
			} else {
				alert("공백이나, 길이가 0인 제목은 사용할 수 없습니다.");
			}
		}.bind(this));

		// 카드에 담기 버튼에 이벤트를 단다.		
		var isTranslator = authService.state.htUserProfile.user_isTranslator;
		this.$elTranslateNow.on("click", function() {
			if(isTranslator) {
				// 현재 번역중인 의뢰가 있다면
				if(authService.state.htUserProfile.user_numOfTranslationsOngoing > 0) {
					// 번역가님은 현재 작업중인 의뢰물이 있습니다.
					alert("번역가님은 현재 작업중인 의뢰물이 있습니다.\n하나 이상의 의뢰를 동시에 작업할 수 없습니다.");
				} else {
					// 해당의뢰를 번역 시작으로 등록시도하고 
					if(this._htOption.rawData.request_clientId === authService.state.htUserProfile.user_email) {
						alert(this._htOption.i18n.TICKET_CANNOT_TRANSLATE_OWN_TICKET);
					} else { 
						var bSureToTranslate = confirm(this._htOption.i18n.TICKET_ARE_YOU_SURE_TO_TRANSLATE);
					
						if(bSureToTranslate) {
							$.ajax({
								type: "POST",
								url: "/api/user/translations/ongoing",
								data: {
									"request_id": this._htOption.rawData.request_id
								},
								dataType: "JSON",
								success: function(data, status, xhr) {
									location.href="/translating";
									// 성공하면 작업중인 번역 페이지로 리다이렉트 시킴
								}.bind(this),
								error: function(xhr, ajaxOptions, thrownError) {
									// 실패하면 실패이유를 alert으로 알려줌
									if (xhr.status == 406){
										alert(this._htOption.i18n.TICKET_CANNOT_TRANSLATE_OWN_TICKET);
									} else if (xhr.status == 401){
										alert(this._htOption.i18n.TICKET_NO_AUTH_TO_TRANSLATE_LANG);
									} else if (xhr.status == 410){
										alert(this._htOption.i18n.TICKET_ALREADY_WORK_OTHER_TRANSLATOR);
									}
								}.bind(this)
							});						
						}	
					}
				}
			} else {
				// 모국어가 없는 티켓 일 경우
				var nMotherLang = authService.state.htUserProfile.user_motherLang;
				
				if(nMotherLang === this._htOption.data.originalLangIndex || nMotherLang === this._htOption.data.targetLangIndex) {
					// 모국어가 아닌 해당언어의 번역권한을 신청할 수 있는 팝업
					var nNeedLang = (nMotherLang === this._htOption.data.originalLangIndex) ? this._htOption.data.targetLangIndex : this._htOption.data.originalLangIndex;
					beTranslatorView.showTranslatorPopup(nNeedLang);
				}
			}
		}.bind(this));
		
		this.$elItem.find(".actionBtn.delete").on("click", function(){	
			var bSureToDelete = confirm(this._htOption.i18n.TICKET_ARE_YOU_SURE_TO_DELETE);
			if(bSureToDelete) {		
				requestService.deleteRequest({
					request_id: this._htOption.rawData.request_id,
					success: function(data) {
						this._htOption.deleteSuccess(data, this);
					}.bind(this)
				});
			}
		}.bind(this));
		
		this.$elItem.find(".actionBtn.cart").on("click", function() {
			// 카트에 담기 ajax요청을 보낸다
			if(isTranslator) {
				requestService.addRequestToCart({
					request_id: this._htOption.rawData.request_id,
					success: function(data, status, xhr) {
						if (xhr.status == 200){
							alert(this._htOption.i18n.TICKET_ADD_CART);
						} else if (xhr.status == 204){
							alert(this._htOption.i18n.TICKET_ALREADY_IN_CART);
						}
						
						setTimeout(function(){
							location.href='/cart';
						},100);
					}.bind(this),
					error: function(xhr, ajaxOptions, thrownError){
						if (xhr.status == 401){
							alert(this._htOption.i18n.TICKET_NO_AUTH_TO_TRANSLATE_LANG);
						} else if (xhr.status == 406){
							alert(this._htOption.i18n.TICKET_CANNOT_TRANSLATE_OWN_TICKET);
						} else if (xhr.status == 410){
							alert(this._htOption.i18n.TICKET_ALREADY_WORK_OTHER_TRANSLATOR);
						}
					}.bind(this)
				});
			} else {
				//this._htOption.i18n.
				alert(this._htOption.i18n.TICKET_NOT_TRANSLATOR);
			}	
		}.bind(this));
		
		this.$elItem.find(".actionBtn.deleteFromCart").on("click", function(){
			var bSureToDeleteFromCart = confirm(this._htOption.i18n.TICKET_ARE_YOU_SURE_TO_DELETE_FROM_CART);
			if(bSureToDeleteFromCart) {
				requestService.deleteRequestFromCart({
					request_id: this._htOption.rawData.request_id,
					success: function(data) {
						if(this._htOption.deleteFromCartSuccess)
							this._htOption.deleteFromCartSuccess(data, this);
					}.bind(this)
				});
			}
		}.bind(this));					
		
	};
	
	function convertDateStringToISO(sDate) {
		return sDate.replace(" ","T")+"Z";
	}
	
	RequestTicketView.prototype.getTranslatingProcess = function() {
		var sDueTime = convertDateStringToISO(this._htOption.rawData.request_dueTime);
		var sStartTime = convertDateStringToISO(this._htOption.rawData.request_transStartTime);
		var nTotal = new Date(sDueTime) - new Date(sStartTime);
		var nProcess = new Date((new Date()).getTime()) - new Date(sStartTime);

		return parseInt(nProcess / nTotal * 100);
	};
	return RequestTicketView;
});