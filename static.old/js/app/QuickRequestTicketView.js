define(['jquery', 'eventemitter', 'app/RequestService', 'app/AuthService', 'app/TemplateMap', 'app/BeTranslatorView'], function($, EventEmitter, requestService, authService, templateMap, beTranslatorView) {

	var oBeTranslatorView = new beTranslatorView();

	function QuickRequestTicketView($elContainer, htOption) {
		this.$elContainer = $elContainer;
		this.$elItem;	
	
		this._htOption = $.extend({}, {
			hasTranslateNow : false,
			hasDelete : false,
			hasAddToCart : false,
			hasDeleteFromCart : false,
			hasCancelRequest: false,
			hasTranslatingLabel : false,
			hasCompleteRequest : false
		}, htOption);
		
		this._htOption.data.originalLangPicPath = templateMap.resourceMapperLangFlag(this._htOption.data.originalLangIndex);
		this._htOption.data.targetLangPicPath = templateMap.resourceMapperLangFlag(this._htOption.data.targetLangIndex);

		if(this._htOption.rawData && this._htOption.rawData.request_transStartTime)
			this._htOption.data.translatingProcess = this.getTranslatingProcess();

		this.loadTemplate({
			callback : function() {			
				this.initAfterLoaded();
			}.bind(this)
		});
	}
	
	QuickRequestTicketView.prototype.loadTemplate = function(htParam) {
		templateMap.get("QuickRequestTicketView", function(sTemplate, i18n) {
			this._htOption.i18n = $.extend({}, i18n);
			
			
			// this._htOption.data 를 이용해서 i18n 모든항목을 한번 더 컴파일한다.
			for(var sKey in this._htOption.i18n) {
				this._htOption.i18n[sKey] = Handlebars.compile(this._htOption.i18n[sKey])(this._htOption.data);
			}
			
			// 본문 엔터 교체 
			if(this._htOption.data.originalText ) {
				this._htOption.data.originalText = this._htOption.data.originalText.replace(/\n/g, "<br/>");	
			}
			
			this._template = Handlebars.compile(sTemplate);
			htParam.callback();		
		}.bind(this));
	};	
	
	QuickRequestTicketView.prototype.initAfterLoaded = function() {
		// 값에따라 동적으로 i18n을 적용해야하는 값을 적용한다.
		this._htOption.data.messages = this._htOption.data.messages || {};
		this._htOption.data.messages.originalLang = this._htOption.i18n[templateMap.getKeyForLanguage(this._htOption.data.originalLangIndex)];
		this._htOption.data.messages.targetLang = this._htOption.i18n[templateMap.getKeyForLanguage(this._htOption.data.targetLangIndex)];

		this.buildUpElements();
			
		this.$elContainer.find("img").addClass("beforeReady");
		this.$elContainer.find("img").on("load", function(){
			$(this).removeClass("beforeReady");
		})
					
		this.initEvents();
	};
	
	QuickRequestTicketView.prototype.buildUpElements = function() {
		console.log(this._htOption);
		var $elRenderedHtml = $(this._template(this._htOption));
		
		if(this._htOption.isAppending) {
			this.$elContainer.append($elRenderedHtml);
		} else {
			this.$elContainer.html($elRenderedHtml);
		}
		this.$elItem = $elRenderedHtml;
		
		this.$elTranslateNow = $elRenderedHtml.find(".actionBtn.translate");

		if(this._htOption.hasTranslateNow === false || (this._htOption.rawData && this._htOption.rawData.request_status === 1)) {
			this.$elItem.find(".actionBtn.translate").remove();
		}			
		if(this._htOption.hasCancelRequest === false && this._htOption.isExpired == null) {	
			this.$elItem.find(".actionBtn.delete").remove();
		}
		
		if(this._htOption.hasTranslatingLabel === false) {
			this.$elItem.find(".translating").remove();
		} else {
			this.$elItem.find(".actionCol").remove();
		}

		if(this._htOption.hasAddToCart === false) {
			this.$elItem.find(".actionBtn.cart").remove();
		}
		if(this._htOption.hasDeleteFromCart === false) {
			this.$elItem.find(".actionBtn.deleteFromCart").remove();
		}
		
		if(this._htOption.rawData.request_status === 1) {
			this.$elItem.find(".translating").show();	
			console.log(this._htOption.rawData);
		} else {
			this.$elItem.find(".translating").remove();	
		}		

		if(this._htOption.hasTranslatingLabel === false) {
			this.$elItem.find(".translating").remove();
			//this.$elItem.find(".contentCol").css("width", "490px");
		} else {
			this.$elItem.find(".actionCol").remove();
		}

		if(this._htOption.hasCompleteRequest === false) {
			this.$elItem.find(".makeTitleContainer").remove();
		}
		
		// 번역중일 경우 진행율 차트 표시 
		if(this._htOption.data.translatingProcess) {
			var $ppc = this.$elContainer.find('.progress-pie-chart'),
			percent = parseInt($ppc.data('percent')),
			deg = 360*percent/100;
			if (percent > 50) {
			$ppc.addClass('gt-50');
			}
			this.$elContainer.find('.ppc-progress-fill').css('transform','rotate('+ deg +'deg)');			
		}
		
		if(this._htOption.isExpired) {
			this.$elItem.find(".header.main").css("background-color", "#FF8C8C");
		} else {
			this.$elItem.find(".actionBtn.resubmit").remove(); 
		}
	};
	
	QuickRequestTicketView.prototype.initEvents = function() {
		var isTranslator = authService.state.htUserProfile.user_isTranslator;
		
		this.$elItem.on("click", ".makeTitle", function() {
			var sTitle = this.$elItem.find(".makeTitleContainer textarea").val();
			
			if(sTitle && sTitle.length > 0) {
				requestService.setTitleToCompletedRequest({
					request_id: this._htOption.rawData.request_id,
					title_text: sTitle,
					success: function() {
						location.href = "/donerequests";
					}
				});
			} else {
				alert("공백이나, 길이가 0인 제목은 사용할 수 없습니다.");
			}
		}.bind(this));
		
		this.$elItem.find(".actionBtn.resubmit").on("click", function(){			
			requestService.resubmitRequest({
				request_id: this._htOption.rawData.request_id,
				user_addtionalTime: 1800, // 초 단위인데 30분을 입력
				success: function(data) {
					this._htOption.resubmitSuccess(data, this);
				}.bind(this)
			});
		}.bind(this));	
		
		this.$elTranslateNow.on("click", function() {
			if(isTranslator) {
				// 현재 번역중인 의뢰가 있다면
				if(authService.state.htUserProfile.user_numOfTranslationsOngoing > 0) {
					// 번역가님은 현재 작업중인 의뢰물이 있습니다.
					alert("번역가님은 현재 작업중인 의뢰물이 있습니다.\n하나 이상의 의뢰를 동시에 작업할 수 없습니다.");
				} else {
					// 해당의뢰를 번역 시작으로 등록시도하고 
					if(this._htOption.rawData.request_clientId === authService.state.htUserProfile.user_email) {
						alert("본인의 의뢰는 번역할 수 없습니다.");
					} else {
						var bSureToTranslate = confirm(this._htOption.i18n.TICKET_ARE_YOU_SURE_TO_TRANSLATE);
					
						if(bSureToTranslate) {
							$.ajax({
								type: "POST",
								url: "/api/user/translations/ongoing",
								data: {
									"request_id": this._htOption.rawData.request_id
								},
								dataType: "JSON",
								success: function(data, status, xhr) {
									location.href="/translating";
									// 성공하면 작업중인 번역 페이지로 리다이렉트 시킴
								}.bind(this),
								error: function(xhr, ajaxOptions, thrownError) {
									// 실패하면 실패이유를 alert으로 알려줌
									alert(xhr.message);
								}.bind(this)
							});	
						}					
					}
				}
			} else {
				// 모국어가 없는 티켓 일 경우
				var nMotherLang = authService.state.htUserProfile.user_motherLang;
				
				if(nMotherLang !== this._htOption.data.originalLangIndex && nMotherLang !== this._htOption.data.targetLangIndex) {
					alert("현재는 모국어가 포함된 티켓만 번역가능합니다.");
				}
				
				if(nMotherLang === this._htOption.data.originalLangIndex || nMotherLang === this._htOption.data.targetLangIndex) {
					// 모국어가 아닌 해당언어의 번역권한을 신청할 수 있는 팝업
					var nNeedLang = (nMotherLang === this._htOption.data.originalLangIndex) ? this._htOption.data.targetLangIndex : this._htOption.data.originalLangIndex;
					alert(nNeedLang + "언어의 권한을 신청하는 팝업을 띄웁니다.");
					beTranslatorView.showTranslatorPopup(nNeedLang);
				}
			}
		}.bind(this));
		
		this.$elItem.find(".actionBtn.delete").on("click", function(){
			var bSureToDelete = confirm(this._htOption.i18n.TICKET_ARE_YOU_SURE_TO_DELETE);
			
			if(bSureToDelete) {	
				requestService.deleteRequest({
					request_id: this._htOption.rawData.request_id,
					success: function(data) {
						this._htOption.deleteSuccess(data, this);
					}.bind(this)
				});
			}
		}.bind(this));
		
		var isTranslator = authService.state.htUserProfile.user_isTranslator;
		this.$elItem.find(".actionBtn.cart").on("click", function() {
			// 카트에 담기 ajax요청을 보낸다
			if(isTranslator) {
				requestService.addRequestToCart({
					request_id: this._htOption.rawData.request_id,
					success: function() {
						setTimeout(function(){
							location.href='/cart';
						},100);
					}
				});
			} else {
				setTimeout(function(){
					location.href='/cart';
				},100);
			}	
		}.bind(this));
		
		this.$elItem.find(".actionBtn.deleteFromCart").on("click", function(){
			var bSureToDeleteFromCart = confirm(this._htOption.i18n.TICKET_ARE_YOU_SURE_TO_DELETE_FROM_CART);
			
			if(bSureToDeleteFromCart) {	
				requestService.deleteRequestFromCart({
					request_id: this._htOption.rawData.request_id,
					success: function(data) {
						if(this._htOption.deleteFromCartSuccess)
							this._htOption.deleteFromCartSuccess(data, this);
					}.bind(this)
				});
			}
		}.bind(this));
	};

	function convertDateStringToISO(sDate) {
		return sDate.replace(" ","T")+"Z";
	}
	
	QuickRequestTicketView.prototype.getTranslatingProcess = function() {
		var sDueTime = convertDateStringToISO(this._htOption.rawData.request_dueTime);
		var sStartTime = convertDateStringToISO(this._htOption.rawData.request_transStartTime);
		var nTotal = new Date(sDueTime) - new Date(sStartTime);
		
		var oNow = new Date();
		var oStart = new Date(sStartTime);
		
		var nProcess = new Date((new Date()).getTime()) - new Date(sStartTime);

		return parseInt(nProcess / nTotal * 100);
	};

	return QuickRequestTicketView;
});