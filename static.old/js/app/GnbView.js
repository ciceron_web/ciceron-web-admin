define(['jquery', 'app/AuthService', 'app/TemplateMap', 'app/GlobalNotificationView', "jquery-ui/selectmenu"], function($, authService, templateMap, GlobalNotificationView) {

	var authService = authService;
	
	function GnbView($elContainer) {

		this.$elContainer = $elContainer;
		
		var htSessionStorage = authService.getUserProfile();
		
		this._htOption = {};

		if(htSessionStorage && htSessionStorage.htUserProfile) {
			this._htOption.userName = htSessionStorage.htUserProfile.user_name;
			this._htOption.userPicPath = "/api/access_file/" + htSessionStorage.htUserProfile.user_profilePicPath;
		}
		
		templateMap.get("gnbView", function(sTemplate, i18n) {
			this._htOption.i18n = $.extend({}, i18n);

			this._template = Handlebars.compile(sTemplate);
			this.render();

			this.initEvents();		
		}.bind(this));
	}
	
	GnbView.prototype.render = function() {
		var $elRenderedHtml = $(this._template({
			data: this._htOption,
			i18n: this._htOption.i18n
		}));
		this.$elContainer.html($elRenderedHtml);

		var primaryPath = "/" + location.pathname.split("/")[1];
		
		// 현재 주소를 인식해서 현재 뷰에 해당하는 메뉴를 하일라이팅 시킨다.
		$("a[href=\\"+primaryPath+"] > div").addClass("selected");
		
	};
	
	GnbView.prototype.initEvents = function() {
		var sMenuLanguageIndex = localStorage.getItem('menuLanguageIndex');
		//var sLocale = Intl.DateTimeFormat().resolved.locale;
		var sLocale = navigator.language;
		
		if(!sMenuLanguageIndex) {
			if(sLocale === "ko") {
				sMenuLanguageIndex = 1;
			} else if(sLocale.indexOf("en") !== -1) {
				sMenuLanguageIndex = 1;
			}
		}
		
		
		$("#menu_language_select > option[value="+sMenuLanguageIndex+"]").attr("selected", true);			
		$("#menu_language_select").on("change", function(){
			localStorage.setItem('menuLanguageIndex', $(this).val());
			location.reload();
		});
		
		// GlobalNotificationView 초기화 
		var globalNotificationView = new GlobalNotificationView($(".alarm_container"));
		
		var oBanner = JSON.parse(sessionStorage.getItem('banner'));

		// banner 표시유무 설정
		setTimeout(function(){
			if(oBanner && oBanner.request_id && $("li[data-request-id='"+oBanner.request_id+"']").length > 0) {
				this.$elContainer.find(".banner p").html(oBanner.message);
				this.$elContainer.find(".banner").slideDown(300,  "linear");
			
				var anchorheight = this.$elContainer.find(".global-nav").height() + 57;
				this.$elContainer.find(".anchor").height(anchorheight);									

			} else {
				this.$elContainer.find(".banner").slideUp(300,  "linear");
				var anchorheight = this.$elContainer.find(".global-nav").height();
				this.$elContainer.find(".anchor").height(anchorheight);	
				//this.$elContainer.find(".banner").height(0);
				//this.$elContainer.find(".banner").hide();				
			}
		}.bind(this), 2000);
		
		// anchor 높이를 현재 global-nav, banner 높이의합으로 넣는다.
	//	var anchorheight = this.$elContainer.find(".global-nav").height() + this.$elContainer.find(".banner").height();
	//	this.$elContainer.find(".anchor").height(anchorheight);
		
		
		// X 클릭시 배너지우고 anchor 높이 재설정 
		this.$elContainer.find(".close").on("click", function() {
			this.$elContainer.find(".banner").slideUp(300,  "linear");
			var anchorheight = this.$elContainer.find(".global-nav").height();
			this.$elContainer.find(".anchor").height(anchorheight);	
			
			sessionStorage.setItem('banner', null);
		}.bind(this));

		this.$elContainer.find(".nav .menu img").on("click", function() {
			$(".sideMenuContainer").addClass("show");
		});
		$(".sideMenuContainer .dimm").on("click", function() {
			$(".sideMenuContainer").removeClass("show");
		});	
	};
	
	return GnbView;
});