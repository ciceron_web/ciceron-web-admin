define(['jquery', 'app/AuthService', 'app/RequestService', 'app/TemplateMap', 'app/I18nResolver'], function($, authService, requestService, templateMap, I18nResolver) {

	var authService = authService;
	var requestService = requestService;
	var htUserProfile = null;
	var i18n = I18nResolver.getMap();
	
	function NotiItemView($elContainer, htOption) {
		
		this.$elContainer = $elContainer;
		this.$elItem = null;

		this._htOption = $.extend({
			i18n: {},
			data: {}
		}, htOption);
		
		var target_userProfilePic = this._htOption.data.target_userProfilePic;
		this._htOption.data.targetProfilePic = target_userProfilePic !== null ? target_userProfilePic : 
				this._htOption.data.profilePic;
		this._htOption.data.targetProfilePic = "/api/access_file/" + this._htOption.data.targetProfilePic;		

		this._htOption.data.requesteeName = this._htOption.data.target_username;

		this._htOption.data.translatorName = this._htOption.data.target_username;

		var sTs = this._htOption.data.ts;
		var oDate = new Date(sTs.replace(/-/g,'/')+ " UTC");
		sTs = [oDate.getFullYear(),oDate.getMonth()+1,oDate.getDate()].join("-") + " " +
		[oDate.getHours(),oDate.getMinutes(),oDate.getSeconds()].join(":");
		this._htOption.data.ts = sTs;

		
		this._template = null;
		
		this.loadTemplate({
			callback : function() {			
				this.initAfterLoaded();
			}.bind(this)
		});
	}

	NotiItemView.prototype.loadTemplate = function(htParam) {
		templateMap.get("NotiItemView", function(sTemplate, i18n) {
			this._htOption.i18n = $.extend({}, i18n);
			
			// this._htOption.data 를 이용해서 i18n 모든항목을 한번 더 컴파일한다.
			for(var sKey in this._htOption.i18n) {
				this._htOption.i18n[sKey] = Handlebars.compile(this._htOption.i18n[sKey])(this._htOption.data);
			}
			
			this._template = Handlebars.compile(sTemplate);
			htParam.callback();		
		}.bind(this));
	};	
	
	NotiItemView.prototype.initAfterLoaded = function() {
		this.render();
		this.initEvents();	
	};
	
	NotiItemView.prototype.render = function() {
				
		var $elRenderedHtml = $(this._template(this._htOption));
		this.$elItem = $elRenderedHtml;

		if(this._htOption.isAppend === true) {
			this.$elContainer.append(this.$elItem);
		} else {
			this.$elContainer.html(this.$elItem);
		}
		
		this.$elItem.find(".content > :not(.type"+this._htOption.data.noti_typeId+")").remove();

	};
	
	NotiItemView.prototype.initEvents = function() {
		this.$elItem.find(".respondBtn").on("click", function(evt) {
			evt.stopPropagation();
			this.$elItem.find(".actionContainer").toggleClass("expand");
		}.bind(this));
	
		this.$elItem.on("click", function(event) {
			var href = $(event.target).closest(".notiItemContainer").attr("data-ticket-link");
			if($(event.target).closest(".toggleContent").length === 0 && $(event.target).closest(".respondTime").length === 0 && event.target.tagName !== "button") {
				location.href = href;
			}
		});
		
		this.$elItem.find(".toggleContent p").on("click", function(event) {
			this.$elItem.find(".originalContent").toggleClass("show");
			if(this.$elItem.find(".originalContent").hasClass("show")) {
				this.$elItem.find(".toggleContent p").html(this._htOption.i18n.ALERT_UNVIEW_CONTENT);
			} else {
				this.$elItem.find(".toggleContent p").html(this._htOption.i18n.ALERT_VIEW_CONTENT);	
			}
		}.bind(this));
		
		this.$elItem.find(".respondTime button").on("click", function(event) {
			var nHours = this.$elItem.find(".respondTime input[name='hours']").val();
			var nDays = this.$elItem.find(".respondTime input[name='days']").val();
			var nMinutes = this.$elItem.find(".respondTime input[name='minutes']").val();
			
			var totalSeconds = nMinutes * 60 + nDays * 24 * 60 * 60 + nHours * 60 * 60;
			
			requestService.sendEstimatedTimeForRequest({
				request_id: this._htOption.data.request_id,
				deltaFromNow: parseInt(totalSeconds),
				success: function(data) {
					alert(i18n.ALERT_EXPECTEDTIME_SUBMITTED);
				},
				error: function(){
					alert("Server doesn't work properly. Please wait for a minute, or contact to contact@ciceron.me");
				}
			});
		}.bind(this));
		// 잔액 과 번역건수 표시			
		//$(".revenueValue span").html(htUserProfile.user_revenue.toFixed(2));

		//var $elListContainer = this.$elContainer.find(".translatedItemContainer"); 
		//new TranslatedItemListController($elListContainer);			
	};
	
	return NotiItemView;
});