var observeDOM = (function () {
  var MutationObserver = window.MutationObserver || window.WebKitMutationObserver,
      eventListenerSupported = window.addEventListener;

  return function (obj, callback) {
    if (MutationObserver) {
      var obs = new MutationObserver(function(mutations, observer){
        if (mutations[0].addedNodes.length || mutations[0].removedNodes.length) callback();
      });

      obs.observe(obj, { childList:true, subtree:true });
    }
    else if (eventListenerSupported) {
      obj.addEventListener('DOMNodeInserted', callback, false);
      obj.addEventListener('DOMNodeRemoved', callback, false);
    }
  }
})();

var getText = function () {
  $.ajax({
    url: $apiURL + '/' + $_GET('id'),
    data: '',
    processData: false,
    contentType: false,
    dataType: 'JSON',
    type: 'GET',
    success: function (data) {
      setText(data.realData);
      $(data.data).each(function(i, item) {
        $("#request_context").text(item.request_context);
         
      });
      // alert(JSON.stringify(data.realData));
      // $("#request_context").text(data.)
    },
    error: function (data) {
      setText(fakeData.translatedCard);
    }
  });
};

var setText = function (obj) {
  var textTemplate = '', memoTemplate = '';

  var i = 0;
  for (; i < obj.length; i++) {
    var o = obj[i], originStr = '', transStr = '', hasMemo = '', memoStr = '', opinionStr = '', hasOpinion = '';

    hasOpinion = o.paragraph_comment ? '' : '';
    // hasOpinion = o.paragraph_comment ? '' : 'unactive';
    o.paragraph_comment = o.paragraph_comment || '';
    opinionStr += '<div class="opinion-text-wrapper ' + hasOpinion + '" data-seq-id="' + o.paragraph_seq + '">' +
                  '<div class="opinion-title">' + '문단견해' + '</div>' +
                  '<div class="opinion-text-area">' +
                  '<textarea class="opinion rm-button-style" data-seq-id="' + o.paragraph_seq + '" type="text">' + o.paragraph_comment + '</textarea>' +
                  '</div>' +
                  '</div>';

    var j = 0;
    for (; j < o.sentences.length; j++) {
      var oo = o.sentences[j];
      hasMemo = oo.sentence_comment ? '<span class="has-memo"></span>' : '';
      memoStr += '<div class="sentence-memo none" data-seq-id="' + o.paragraph_seq + '-' + oo.sentence_seq + '">' +
                 '<div class="memo-wrapper">' +
                 '<div class="memo-content"><div>' + (oo.sentence_comment || '<br />' ) + '</div></div>' +
                 '<div class="memo-triangle"></div>' +
                 '</div>' +
                 '</div>';

      originStr += '<span class="sentence" data-seq-id="' + o.paragraph_seq + '-' + oo.sentence_seq + '">' +
                   hasMemo +
                   '<span>' + oo.original_text + '</span>' +
                   '</span>';

      transStr += '<span class="sentence" data-seq-id="' + o.paragraph_seq + '-' + oo.sentence_seq + '">' +
                  oo.translated_text +
                  '</span>';
    }

    textTemplate += '<div class="origin-text-wrapper" data-seq-id="' + o.paragraph_seq + '">' +
                    '<div class="origin-text-area">' +
                    originStr +
                    '</div>' +
                    '</div>' +
                    '<div class="trans-text-wrapper" data-seq-id="' + o.paragraph_seq + '">' +
                    '<div class="trans-text-area">' +
                    transStr +
                    '</div>' +
                    '</div>' +
                    opinionStr;

    memoTemplate += memoStr;
  }
  

  $('#memo-form').append(memoTemplate);
  $('.text-wrapper').append(textTemplate);
  
  
  
  setMemoPosition();
};

var setTextareaHeight = function () {
  var rightHeight = $('#translate-form').outerHeight();
  var leftHeight = $('#memo-form').outerHeight();

  if (rightHeight > leftHeight) {
    // $('#memo-form').css('height', rightHeight + 'px');
  }
};

var showMyWork = function () {
  var obj = {}, paragraph, sentence;

  obj = {
    translatedCard: []
  };

  var i = 0, len = $('.origin-text-wrapper').length;
  for (; i < len; i++) {
    paragraph = {};

    paragraph.paragraph_seq = $('.origin-text-wrapper').eq(i).attr('data-seq-id');
    paragraph.paragraph_comment = $('.opinion-text-wrapper[data-seq-id="' + paragraph.paragraph_seq + '"]').find('textarea').val();
    paragraph.sentences = [];

    var j = 0, $originEl = $('.origin-text-wrapper[data-seq-id="' + paragraph.paragraph_seq + '"]').find('.sentence');
    for (; j < $($originEl).length; j++) {
      sentence = {};

      sentence.sentence_seq = $($originEl).eq(j).attr('data-seq-id').split('-')[1];
      sentence.original_text = $($originEl).eq(j).find('span:not(.has-memo)').text();
      sentence.translated_text = $('.trans-text-wrapper .sentence[data-seq-id="' + $($originEl).eq(j).attr('data-seq-id') + '"]').text();
      sentence.sentence_comment = $('.sentence-memo[data-seq-id="' + $($originEl).eq(j).attr('data-seq-id') + '"] .memo-content').html();
      sentence.sentence_comment = sentence.sentence_comment.replace(/\<div\>/g, '').replace(/\<\/div\>/g, '<br>');

      paragraph.sentences.push(sentence);
    }

    obj.translatedCard.push(paragraph);
  }

  // console.log(obj.translatedCard);
  $('body').append(getCardPopup(obj.translatedCard));
  setMemoPositionInPopup();
};

var getCardPopup = function (obj) {
  var template = '';

  template = '<div class="popup" id="popup-1">' +
             '<div class="popup-dim"></div>' +
             '<div class="detail">' +
             '<div class="detail-close text-right">' +
             '<button class="close-popup rm-button-style">' +
             '<i class="icon ion-ios-close-empty fa-4x"></i>' +
             '</button>' +
             '</div>' +
             '<div class="detail-header clearfix">' +
             '<div class="div_1-3_1"></div>' +
             '<div class="div_1-3_3">' +
             '<span class="subject">번역문</span>' +
             '<hr />' +
             '<div class="left-guidance col-lg-5 col-sm-10 clearfix">' +
             '<button class="toggle-memo fa fa-youtube rm-button-style"></button>' +
             '<button class="show-split select-view-type fa fa-facebook rm-button-style active" data-view-type="split-view"></button>' +
             '<button class="show-vertical select-view-type fa fa-twitter rm-button-style" data-view-type="vertical-view"></button>' +
             '<button class="show-sentence select-view-type fa fa-pinterest rm-button-style" data-view-type="sentence-view"></button>' +
             '</div>' +
             '<div class="right-guidance col-lg-5 col-sm-10 text-right clearfix">' +
             '<div class="color-guide show-origin-text">원문보기</div>' +
             '<div class="color-guide show-trans-text">본문보기</div>' +
             '<div class="color-guide show-opinion">문단견해</div>' +
             '</div>' +
             '</div>' +
             '</div>' +
             '<div class="detail-content split-view clearfix">' +
             showSplitView(obj) +
             '</div>' +
             '</div>' +
             '</div>';

  return template;
};

var showSplitView = function (obj) {
  var template = '';

  var i = 0;
  for (; i < obj.length; i++) {
    var o = obj[i], originStr = '', transStr = '', opinionStr = '', memoStr = '', mixStr = '';

    if (o.paragraph_comment) {
      opinionStr = '<p class="content-opinion" data-seq-id="' + o.paragraph_seq + '">' +
                   '<span>' + o.paragraph_comment + '</span>' +
                   '</p>';
    }

    var j = 0;
    for (; j < o.sentences.length; j++) {
      var oo = o.sentences[j], hasMemoStr = '';

      if (oo.sentence_comment) {
        memoStr += '<div class="sentence-memo none" data-seq-id="' + o.paragraph_seq + '-' + oo.sentence_seq + '">' +
                   '<div class="memo-wrapper">' +
                   '<div class="memo-content">' + oo.sentence_comment + '</div>' +
                   '<div class="memo-triangle"></div>' +
                   '</div>' +
                   '</div>';

        memoStrInToggled = '<div class="sentence-memo none" data-seq-id="' + o.paragraph_seq + '-' + oo.sentence_seq + '">' +
                           '<div class="memo-wrapper">' +
                           '<div class="memo-content">' + oo.sentence_comment + '</div>' +
                           '<div class="memo-triangle"></div>' +
                           '</div>' +
                           '</div>';

        hasMemoStr = '<span class="has-memo"></span>';
      }

      originStr += '<span class="content-sentence" data-seq-id="' + o.paragraph_seq + '-' + oo.sentence_seq + '">' +
                   hasMemoStr +
                   '<span>' + oo.original_text + '</span>' +
                   '<div class="memo-in-toggled">' +
                   (memoStrInToggled.length ? memoStrInToggled : '') +
                   '</div>' +
                   '</span>';

      transStr += '<span class="content-sentence" data-seq-id="' + o.paragraph_seq + '-' + oo.sentence_seq + '">' +
                  '<span>' + oo.translated_text + '</span>' +
                  '</span>';

      mixStr += '<span class="content-sentence origin" data-seq-id="' + o.paragraph_seq + '-' + oo.sentence_seq + '">' +
                hasMemoStr +
                '<span>' + oo.original_text + '</span>' +
                '</span>' +
                '<div class="memo-in-toggled">' +
                (memoStrInToggled.length ? memoStrInToggled : '') +
                '</div>' +
                '<span class="content-sentence trans" data-seq-id="' + o.paragraph_seq + '-' + oo.sentence_seq + '">' +
                '<span>' + oo.translated_text + '</span>' +
                '</span>';
    }

    template += '<div class="content-paragraph clearfix">' +
                '<div class="div_1-3_1">' +
                memoStr +
                '</div>' +
                '<div class="div_1-3_3">' +
                '<div class="origin-paragraph col-lg-5 col-sm-5">' +
                originStr +
                '</div>' +
                '<div class="trans-paragraph col-lg-5 col-sm-5">' +
                transStr +
                opinionStr +
                '</div>' +
                '<div class="mix-paragraph col-lg-5 col-sm-5">' +
                mixStr +
                opinionStr +
                '</div>' +
                '</div>' +
                '</div>';
  }

  template += '<div class="submit-button-wrapper text-center">' +
              '<button class="rm-button-style" id="submit-workpiece">제출하기</button>' +
              '</div>' +
              '';

  return template;
};

var ready = function () {
  i18n.setLanguage($.cookie($userInfomation.user_email + "translate"));
  getText();

  $('#content-text-area').on('keyup keydown change',function () {
    var $nowScroll = $(document).scrollTop();
    $(this).css('height', 'auto' );
    $(this).height( this.scrollHeight );
    $(document).scrollTop($nowScroll + this.scrollHeight - $preScrollHeight);
    $preScrollHeight = this.scrollHeight;
    $('.character-count').text($(this).val().length + " 글자");
    setOfferPrice();
  });

  $('body').on('submit', '.offering-wrapper form', function (e) {
    e.preventDefault();
  });

  var createEditor = function (el) {
    var seqId = $(el).attr('data-seq-id');
    var txt = $(el).text().trim();

    if ($('.sentence[contenteditable=true]').length && $('.sentence[contenteditable=true]').attr('data-seq-id') !== $(el).attr('data-seq-id')) {
      var openedId = $('.sentence[contenteditable=true]').attr('data-seq-id');

      $('.sentence[data-seq-id="' + openedId + '"]').removeClass('selected');
      $('.trans-text-area .sentence[data-seq-id="' + openedId + '"]').attr('contenteditable', 'false');
      $('.sentence-memo[data-seq-id="' + openedId + '"]').addClass('none');
      $('.sentence-memo[data-seq-id="' + openedId + '"] .memo-content').attr('contenteditable', 'false');
      // $('.sentence[data-seq-id="' + openedId + '"]').click();
    }

    if ($(el).hasClass('selected')) {
      $('.sentence[data-seq-id="' + seqId + '"]').removeClass('selected');
      $('.trans-text-area .sentence[data-seq-id="' + seqId + '"]').attr('contenteditable', 'false');
      $('.sentence-memo[data-seq-id="' + seqId + '"] .memo-content').attr('contenteditable', 'false');
      $('.sentence-memo[data-seq-id="' + seqId + '"]').addClass('none');
    } else {
      var y1 = -1 * ($('.origin-text-area .sentence[data-seq-id="' + seqId + '"]').closest('.text-wrapper').offset().top - $('.origin-text-area .sentence[data-seq-id="' + seqId + '"]').offset().top) + $('.memo-title').outerHeight();
      var y2 = $('.requirement').outerHeight() > 1 ? $('.requirement').outerHeight() + 40 : 0;

      var y = y1 + y2;

      $('.sentence[data-seq-id="' + seqId + '"]').addClass('selected');
      $('.trans-text-area .sentence[data-seq-id="' + seqId + '"]').attr('contenteditable', 'true').focus();
      $('.sentence-memo[data-seq-id="' + seqId + '"]  .memo-content').attr('contenteditable', 'true');
      $('.sentence-memo[data-seq-id="' + seqId + '"]').removeClass('none').css('top', y + 'px');
    }
  };

  $('body').on('click', '.memo-content', function (e) {
    // console.log(e);
    $('*').blur();
    $(this).focus();
  });

  $('body').on('click', '.sentence[contenteditable=true]', function (e) {
    // console.log(e);
    $('*').blur();
    $(this).focus();
  });

  $('body').on('keydown', '.sentence[contenteditable=true]', function (e) {
    if (e.which === 13 || e.keyCode === 13) {
      e.preventDefault();

      var seqId = $(this).attr('data-seq-id') || $(this).closest('.sentence-memo').attr('data-seq-id');

      $('.sentence[data-seq-id="' + seqId + '"]').removeClass('selected');
      $('.trans-text-area .sentence[data-seq-id="' + seqId + '"]').attr('contenteditable', 'false');
      $('.sentence-memo[data-seq-id="' + seqId + '"] .memo-content').attr('contenteditable', 'false');
      $('.sentence-memo[data-seq-id="' + seqId + '"]').addClass('none');

      /********** 엔터 쳤을 때, 다음 문장으로 포커스 이동하기 **********/
      var seqIdP = seqId.split('-')[0] >> 0;
      var seqIdS = seqId.split('-')[1] >> 0;

      var nextSentence = $('.sentence[data-seq-id="' + seqIdP + '-' + (seqIdS + 1) + '"]')[0] || $('.sentence[data-seq-id="' + (seqIdP + 1) + '-' + 1 + '"]')[0] || null;

      if (nextSentence) {
        var nextSeqId = $(nextSentence).attr('data-seq-id');
        return $('.sentence[data-seq-id="' + nextSeqId + '"]').click();
      }

      return false;
    }
  });

  $('body').on('click', '.sentence:not([contenteditable=true])', function () {
    createEditor(this);
  });

  $('body').on('click', '.popup .toggle-memo', function () {
    $(this).closest('.popup').find('.div_1-3_1').toggleClass('collapsed');
    $(this).closest('.popup').find('.div_1-3_3').toggleClass('opened');
  });

  $('body').on('click', '.select-view-type', function () {
    $('.select-view-type').removeClass('active');
    $(this).addClass('active');
    $('.detail-content').removeClass('split-view vertical-view sentence-view').addClass($(this).attr('data-view-type'));

    $('.detail-content .selected').removeClass('selected');
    $('.detail-content .has-memo').removeClass('none').removeClass('active');
    $('.detail-content .sentence-memo').addClass('none');

    setMemoPositionInPopup();
  });

  $('body').on('click', '.popup .origin-paragraph .content-sentence span:not(.has-memo)', function () {
    var sentenceId = $(this).parent().attr('data-seq-id');
    var $another = $(this).closest('.div_1-3_3').find('.trans-paragraph .content-sentence[data-seq-id="' + sentenceId + '"] span:not(.has-memo)');
    var $memo = $(this).closest('.content-paragraph').find('.sentence-memo[data-seq-id="' + sentenceId + '"]');
    var $memoButton = $(this).parent().find('.has-memo');

    $(this).toggleClass('selected');
    $($another).toggleClass('selected');

    $($memoButton).toggleClass('active');

    toggleSentenceMemo($memo, this);
    setMemoPositionInPopup();
  });

  $('body').on('click', '.popup .trans-paragraph .content-sentence span:not(.has-memo)', function () {
    var sentenceId = $(this).parent().attr('data-seq-id');
    var $another = $(this).closest('.div_1-3_3').find('.origin-paragraph .content-sentence[data-seq-id="' + sentenceId + '"] span:not(.has-memo)');
    var $memo = $(this).closest('.content-paragraph').find('.sentence-memo[data-seq-id="' + sentenceId + '"]');
    var $memoButton = $($another).parent().find('.has-memo');

    $(this).toggleClass('selected');
    $($another).toggleClass('selected');

    $($memoButton).toggleClass('active');

    toggleSentenceMemo($memo, $another);
    setMemoPositionInPopup();
  });

  $('body').on('click', '.popup .origin-paragraph .content-sentence .has-memo', function () {
    var sentenceId = $(this).parent().attr('data-seq-id');
    var $anotherOne = $(this).closest('.div_1-3_3').find('.origin-paragraph').find('.content-sentence[data-seq-id="' + sentenceId + '"] span:not(.has-memo)');
    var $anotherTwo = $(this).closest('.div_1-3_3').find('.trans-paragraph').find('.content-sentence[data-seq-id="' + sentenceId + '"] span:not(.has-memo)');
    var $memo = $(this).closest('.content-paragraph').find('.sentence-memo[data-seq-id="' + sentenceId + '"]');

    $($anotherOne).toggleClass('selected');
    $($anotherTwo).toggleClass('selected');

    $(this).toggleClass('active');

    toggleSentenceMemo($memo, $anotherOne);
    setMemoPositionInPopup();
  });

  $('body').on('click', '.popup .mix-paragraph .content-sentence.origin span:not(.has-memo)', function () {
    var sentenceId = $(this).parent().attr('data-seq-id');
    var $another = $(this).closest('.div_1-3_3').find('.mix-paragraph .content-sentence.trans[data-seq-id="' + sentenceId + '"] span:not(.has-memo)');
    var $memo = $(this).closest('.content-paragraph').find('.sentence-memo[data-seq-id="' + sentenceId + '"]');
    var $memoButton = $(this).parent().find('.has-memo');

    $(this).toggleClass('selected');
    $($another).toggleClass('selected');

    $($memoButton).toggleClass('none');

    toggleSentenceMemo($memo, this);
    setMemoPositionInPopup();
  });

  $('body').on('click', '.popup .mix-paragraph .content-sentence.trans span:not(.has-memo)', function () {
    var sentenceId = $(this).parent().attr('data-seq-id');
    var $another = $(this).closest('.div_1-3_3').find('.mix-paragraph .content-sentence.origin[data-seq-id="' + sentenceId + '"] span:not(.has-memo)');
    var $memo = $(this).closest('.content-paragraph').find('.sentence-memo[data-seq-id="' + sentenceId + '"]');
    var $memoButton = $($another).parent().find('.has-memo');

    $(this).toggleClass('selected');
    $($another).toggleClass('selected');

    $($memoButton).toggleClass('none');

    toggleSentenceMemo($memo, $another);
    setMemoPositionInPopup();
  });

  $('body').on('click', '.popup .mix-paragraph .content-sentence .has-memo', function () {
    var sentenceId = $(this).parent().attr('data-seq-id');
    var $anotherOne = $(this).closest('.div_1-3_3').find('.mix-paragraph').find('.content-sentence.origin[data-seq-id="' + sentenceId + '"] span:not(.has-memo)');
    var $anotherTwo = $(this).closest('.div_1-3_3').find('.mix-paragraph').find('.content-sentence.trans[data-seq-id="' + sentenceId + '"] span:not(.has-memo)');
    var $memo = $(this).closest('.content-paragraph').find('.sentence-memo[data-seq-id="' + sentenceId + '"]');

    $($anotherOne).toggleClass('selected');
    $($anotherTwo).toggleClass('selected');

    $(this).toggleClass('none');

    toggleSentenceMemo($memo, $anotherOne);
    setMemoPositionInPopup();
  });

  var toggleSentenceMemo = function (memo, origin) {
    var y = -1 * ($(origin).closest('.div_1-3_3').offset().top - $(origin).offset().top);

    $(memo).toggleClass('none').css('top', y + 'px');
  };

  $('body').on('click', '.opinion-title', function () {
    $(this).closest('.opinion-text-wrapper').removeClass('unactive');
    $(this).closest('.opinion-text-wrapper').find('textarea').click();
  });

  $('.requirement .triangle').css('right', ($('.show-requirement').outerWidth() / 2 - 10) + 'px');

  $('body').on('click', '.show-requirement', function () {
    $('.requirement-wrapper').slideToggle();
  });


  var changeSentenceCommentTimer = null;
  var changeSentenceComment = function (o){
    var formData = new FormData();
    formData.append('comment_string', o.text());
    
    $.ajax({
        url: $apiURL + '/comment/'+$_GET('id')+'/paragraph/'+o.attr('data-seq-id').split('-')[0]+'/sentence/' + o.attr('data-seq-id').split('-')[1],
        data: formData,
        processData: false,
        contentType: false,
        dataType: 'JSON',
        type: 'PUT',
        success: function (data) {
          // setText(data.realData);
          // $(data.data).each(function(i, item) {
          //   $("#request_context").text(item.request_context);
            
          // });
          // alert(JSON.stringify(data.realData));
          // $("#request_context").text(data.)
        },
        error: function (data) {
          // setText(fakeData.translatedCard);
        }
      });
  }
  
  $('body').on('DOMSubtreeModified', '.sentence-memo', function () {
    // if (!$(this).val().length) $(this).closest('.opinion-text-wrapper').addClass('unactive');
    // alert(".memo-content");
    // alert($(this).parent().parent().parent().attr('memo-content'));
    // alert($(this).attr('data-seq-id'));
    // alert($(this).text());
    
    // changeSentenceComment($(this));
    
    if (changeSentenceCommentTimer) {
        clearTimeout(changeSentenceCommentTimer);   // clear any previous pending timer
    }
    var what = $(this);
    changeSentenceCommentTimer = setTimeout(function(){changeSentenceComment(what);}, 300);
    
  });
  
  var changeOpinionTimer = null;
  var changeOpinion = function (o){
    var formData = new FormData();
    formData.append('comment_string', o.val());
    
    $.ajax({
        url: $apiURL + '/comment/'+$_GET('id')+'/paragraph/'+o.attr('data-seq-id'),
        data: formData,
        processData: false,
        contentType: false,
        dataType: 'JSON',
        type: 'PUT',
        success: function (data) {
          // setText(data.realData);
          // $(data.data).each(function(i, item) {
          //   $("#request_context").text(item.request_context);
            
          // });
          // alert(JSON.stringify(data.realData));
          // $("#request_context").text(data.)
        },
        error: function (data) {
          // setText(fakeData.translatedCard);
        }
      });
  }
  
  $('body').on('change', 'textarea.opinion', function () {
    // if (!$(this).val().length) $(this).closest('.opinion-text-wrapper').addClass('unactive');
    // alert("textarea.opinion");
    
    if (changeOpinionTimer) {
        clearTimeout(changeOpinionTimer);   // clear any previous pending timer
    }
    var what = $(this);
    changeOpinionTimer = setTimeout(function(){changeOpinion(what);}, 300);
    
    
  });
  
    var changeSentenceTimer = null;
    var changeSentence = function (o){
    var formData = new FormData();
    formData.append('text', o.text());
    
    $.ajax({
        url: $apiURL + '/ongoing/'+$_GET('id')+'/paragraph/'+o.attr('data-seq-id').split('-')[0]+'/sentence/' + o.attr('data-seq-id').split('-')[1],
        data: formData,
        processData: false,
        contentType: false,
        dataType: 'JSON',
        type: 'PUT',
        success: function (data) {
          // setText(data.realData);
          // $(data.data).each(function(i, item) {
          //   $("#request_context").text(item.request_context);
            
          // });
          // alert(JSON.stringify(data.realData));
          // $("#request_context").text(data.)
        },
        error: function (data) {
          // setText(fakeData.translatedCard);
        }
      });
  }
  
  
  $('body').on('DOMSubtreeModified', '.sentence', function () {
    // if (!$(this).val().length) $(this).closest('.opinion-text-wrapper').addClass('unactive');
    // alert("textarea.opinion");
    // alert("ㅅㄹㅇㄴㅁ");
    
    if (changeSentenceTimer) {
        clearTimeout(changeSentenceTimer);   // clear any previous pending timer
    }
    var what = $(this);
    changeSentenceTimer = setTimeout(function(){changeSentence(what);}, 300);
    
  });
  
  
  $("#frmSubmit").submit(function(e){
      e.preventDefault();
      startLoading();
      
      
      $("#request_id").val($_GET('id'));
      
      var form = $(this)[0];
      var formData = new FormData(form);
      
      $.ajax({
          url: $apiURL + '/complete',
          data: formData,
          processData: false,
          contentType: false,
          dataType: "JSON",
          type: 'POST',
          success: function(data){
              endLoading();
              location.href = "/stoa";
          },
          error: function(xhr, ajaxOptions, thrownError){
              // if (xhr.status == 417) {
              //     alert("올바르지 않은 이메일 주소입니다.");
              // }
              // else if(xhr.status == 412){
              //     alert("이미 존재하는 이메일 주소입니다.");
              // }
              alert("작성 에러!");
              endLoading();
          }
      });
  });
  
  
  
  
  //sentence 
  
  // $('body').on('blur', 'textarea.opinion', function () {
  //   // if (!$(this).val().length) $(this).closest('.opinion-text-wrapper').addClass('unactive');
  // });

  $('body').on('click', '#submit-button', function () {
    // showMyWork();
  });
  
  $('body').on('click', '.show-full-text', function(){
    showTicket($_GET('id'));
  });

  $('body').on('click', '.show-origin-text', function () {
    if ($('.show-trans-text').hasClass('selected') && $('.show-opinion').hasClass('selected')) {
      return;
    }

    if ($(this).hasClass('selected')) {
      $('.origin-paragraph *, .content-sentence.origin, .content-sentence.origin *').removeClass('transparent');
      $(this).removeClass('selected');
    } else {
      $('.origin-paragraph *, .content-sentence.origin, .content-sentence.origin *').addClass('transparent');
      $(this).addClass('selected');
    }
  });

  $('body').on('click', '.show-trans-text', function () {
    if ($('.show-origin-text').hasClass('selected') && $('.show-opinion').hasClass('selected')) {
      return;
    }

    if ($(this).hasClass('selected')) {
      $('.trans-paragraph .content-sentence, .trans-paragraph .content-sentence *, .content-sentence.trans, .content-sentence.trans *').removeClass('transparent');
      $(this).removeClass('selected');
    } else {
      $('.trans-paragraph .content-sentence, .trans-paragraph .content-sentence *, .content-sentence.trans, .content-sentence.trans *').addClass('transparent');
      $(this).addClass('selected');
    }
  });

  $('body').on('click', '.show-opinion', function () {
    if ($('.show-origin-text').hasClass('selected') && $('.show-trans-text').hasClass('selected')) {
      return;  
    }

    if ($(this).hasClass('selected')) {
      $('.trans-paragraph .content-opinion, .trans-paragraph .content-opinion *, .mix-paragraph .content-opinion, .mix-paragraph .content-opinion *').removeClass('transparent');
      $(this).removeClass('selected');
    } else {
      $('.trans-paragraph .content-opinion, .trans-paragraph .content-opinion *, .mix-paragraph .content-opinion, .mix-paragraph .content-opinion *').addClass('transparent');
      $(this).addClass('selected');
    }
  });

  // 순서바꾸기 기능
  $(".trans-text-area.sortable").sortable({
    delay: 150,
    placeholder: 'text-align-highlight',
    connectWith: 'span',
    update: function (event, el) {
      // console.log(event, el);
    }
  });
};

var setMemoPosition = function () {
  var els = $('.origin-text-wrapper .has-memo');

  var i = 0;
  for (; i < els.length; i++) {
    var el = $(els.eq(i));
    var y = -1 * ($(el).closest('.origin-text-wrapper').offset().top - $(el).parent().find('span:not(.has-memo)').offset().top);

    $(el).css('top', (y + 5) + 'px');
  }
};

var setMemoPositionInPopup = function () {
  var els = $('.popup .has-memo');

  var i = 0;
  for (; i < els.length; i++) {
    var el = $(els.eq(i));
    var y = -1 * ($(el).closest('.div_1-3_3').offset().top - $(el).parent().find('span:not(.has-memo)').offset().top);

    $(el).css('top', (y + 5) + 'px');
  }
};

$(document).ready(ready);
$(window).on('load', setTextareaHeight);
$(window).on('resize', setTextareaHeight);
