var i18nData;
var timer1 = null;
var _i18n = function () { };
var settings = {};


_i18n.prototype.getI18n = function(type){
    try {
        
        var str = i18nData[type];
        
        if(arguments.length>1){
            
            if(typeof arguments[1] == 'object'){
                $.each(arguments[1], function(key,value){
                    str = str.replace('{' + key + '}', value);
                });
            }
            else{
                for (var index = 1; index < arguments.length; index++) {
                    str = str.replace('{' + (index - 1) + '}', arguments[index]);
                }
            }
            
        }
        return str;
        
    } catch (error) {
        
        return _i18n.prototype.getI18n(type);
        
    }
    
    

}

_i18n.prototype.setLanguage = function(language){
    
    
    
    $.ajax({
        url: 'js/json/i18n.json',
        dataType: 'json',
        async: false
    }).done(function(data){
        
        settings = data.settings;

        if($.isNumeric(language)){
            language = Object.keys(data)[parseInt(language)];
        }
        
        i18nData = data[language];
        $('*').each(function(){
            var attr = $(this).attr('i18n'); 
            var target = $(this).attr('i18n_attr');
            
            if (typeof attr !== typeof undefined && attr !== false) {
                    
                if (typeof target !== typeof undefined && target !== false) {
                    $(this).attr(target, i18nData[attr]);
                }
                else{
                    $(this).html(i18nData[attr]);
                }
            }
        });
        
        
        $(document).off('DOMNodeInserted');
        
        $(document).on('DOMNodeInserted', function(e) {
            
            if(timer1){
                clearTimeout(timer1);
            }
            
            timer1 = setTimeout(function() {
                $('*').each(function(){
                    var attr = $(this).attr('i18n'); 
                    var target = $(this).attr('i18n_attr');
                    
                    if (typeof attr !== typeof undefined && attr !== false) {
                            
                        if (typeof target !== typeof undefined && target !== false) {
                            $(this).attr(target, i18nData[attr]);
                        }
                        else{
                            $(this).html(i18nData[attr]);
                        }
                    }
                });
                clearTimeout(timer1);
            }, 100);
        });
    }).fail(function( jqXHR, textStatus, errorThrown ) {
        alert("i18n Failed");
    });
    
    
    
    // $.getJSON('js/json/i18n.json', function(data){
        
    // })
    // .fail(function() {
        
    // });
    
}


var i18n = new _i18n();
