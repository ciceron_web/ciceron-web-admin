$(document).ready(function() {

    /*------------------------------------------
    변수 :
        nowClickedLi : 클릭된 태그의 순번 값을 받는 변수
    ------------------------------------------*/
    var nowClickedLi = 0;

    
    /* ----------------------- tagData(JSONArray) 설정 시작 ----------------------- */

    tagData = [
        {
            name: "tag1", 
            category_level_1: 0, 
            category_level_2: 0
            
        },
        {
            name: "tag2", 
            category_level_1: 0,
            category_level_2: 0
        },
        {
            name: "tag3", 
            category_level_1: 0, 
            category_level_2: 0
        },
        {
            name: "tag4",
            category_level_1: 0, 
            category_level_2: 0
        },
        {
            name: "tag5",
            category_level_1: 0, 
            category_level_2: 0
        },
        {
            name: "tag6",
            category_level_1: 0, 
            category_level_2: 0
        },
        {
            name: "tag7",
            category_level_1: 0, 
            category_level_2: 0
        }
    ];

    // 태그마다의 option API
    categoryData = [
        {
            id: "장소",
            name: ["장소","활동","한식","중식", "일식", "요리/Others", "육류", "어류", "조류", "채소", "과일", "공산품", "동물", "식물", "물건", "자연경관"]
        },
        {
            id: "활동",
            name: ["장소","활동","한식","중식", "일식", "요리/Others", "육류", "어류", "조류", "채소", "과일", "공산품", "동물", "식물", "물건", "자연경관"]
        },
        {
            id: "요리",
            name: ["장소","활동","한식","중식", "일식", "요리/Others", "육류", "어류", "조류", "채소", "과일", "공산품", "동물", "식물", "물건", "자연경관"]
        },
        {
            id: "재료",
            name: ["장소","활동","한식","중식", "일식", "요리/Others", "육류", "어류", "조류", "채소", "과일", "공산품", "동물", "식물", "물건", "자연경관"]
        },
        {
            id: "Others",
            nname: ["장소","활동","한식","중식", "일식", "요리/Others", "육류", "어류", "조류", "채소", "과일", "공산품", "동물", "식물", "물건", "자연경관"]
        }
    ];


    /* ----------------------- 해시태그 정렬 시작 ----------------------- */
    $.each(tagData, function(i, item){
        $("#tagView ul").append("<li value='"+i+"'><input type='checkbox' name='tagCheck'/>"+item.name+"</li>");
    });

    /* ----------------------- click 이벤트 시작 ----------------------- */
    $("body").on("click", "#tagView ul li", function(){
        $("#tagName").text($(this).text());
        $("#tagView ul li").removeClass("selected");
        $(this).addClass("selected");
        nowClickedLi = $(this).attr("value");
        // console.log($(this).attr("value"))
        // console.log($(".selected").val())
        $("select[name='category_level_1'] option").remove();
        $("select[name='category_level_2'] option").remove();
        
        $.each(categoryData, function(index, item) {
            $("select[name='category_level_1']").append("<option value='"+index+"'>"+item.id+"</option>");
        })
        $.each(categoryData, function(index, item){
            if(tagData[nowClickedLi].category_level_1 == index) {
                for(var i=0; i<item["name"].length; i++) {
                    $("select[name='category_level_2']").append("<option value='"+i+"'>"+item.name[i]+"</option>");
                }
            }
        });

        $("select[name='category_level_1']").val(tagData[$(this).attr("value")].category_level_1).prop("selected", true);
        $("select[name='category_level_2']").val(tagData[$(this).attr("value")].category_level_2).prop("selected", true);
        // console.log($("select[name='category_level_1']").val(tagData[$(this).attr("value")].category_level_1).prop("selected", true))
        // console.log($("select[name='category_level_1'] option:selected").val())
        // console.log($("select[name='category_level_2'] option:selected").val())
    });
    $("#tagView ul li").first().click();


    

    /* ----------------------- option의 체인지 이벤트 시작 ----------------------- */
    $("body").on("change", "[name=category_level_1]", function() {
        var num = $(this).val();
        tagData[$("li[value="+nowClickedLi+"]").attr("value")].category_level_1 = $(this).val();
        
        $("select[name='category_level_2'] option").remove();
        $.each(categoryData, function(index, item){
            if(num == index) {
                for(var i=0; i<item["name"].length; i++) {
                    $("select[name='category_level_2']").append("<option value='"+i+"'>"+item.name[i]+"</option>");
                }
            }
        });
    });
    $("body").on("change", "[name=category_level_2]", function() {
        tagData[$("li[value="+nowClickedLi+"]").attr("value")].category_level_2 = $(this).val();
    });


    /* ----------------------- 정보 관리 이벤트 시작 ----------------------- */
    $("body").on("click", "#tagData_submit", function(){
        var val = $("#tagData_submit").prev().prev().prev().text();
        $(".selected").remove();
        $("#tagView ul li").eq(0).click();
    });


    $("body").on("click", "#tag_delete", function(){
        $("input[name='tagCheck']:checked").parent().remove();
        $("#tagView ul li").eq(0).click();
    });
});
