var requestedData = {};
var isAuthInProgress = false;
var token = "";
var count_span;
var original_language_val;
var deadline;

// word counter
counter = function () {
    var value = $(".text_counter_popup #text").val();

    if (value.length <= 0) {
        $('#wordCount').html(" " + 0 + " ");
        $('#totalChars').html(" " + 0 + " ");
        $('#charCountNoSpace').html(" " + 0 + " ");
        return;
    }

    var regex = /\s+/gi;
    var regex2 = value.match(/\S+/g);
    // var wordCount = $.trim(value).replace(regex, " ").split(" ").length;
    var wordCount = regex2 ? regex2.length : 0;
    var totalChars = value.replace(/\./g, "").length;
    var charCountNoSpace = value.replace(regex, "").replace(/\./g, "").length;

    $("#wordCount").html(" " + wordCount + " ");
    $("#totalChars").html(" " + totalChars + " ");
    $("#charCountNoSpace").html(" " + charCountNoSpace + " ");
};

// text delete
TextReset = function () {
    var reset_confirm = confirm('작성한 내용을 모두 삭제하시겠습니까?');
    if (reset_confirm) {
        $("#text").val("");
        $('#wordCount').html(" " + 0 + " ");
        $('#totalChars').html(" " + 0 + " ");
        $('#charCountNoSpace').html(" " + 0 + " ");
    }
    return false;
}

// text copy
copyToClipboard = function (element) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(element).val()).select();
    document.execCommand("copy");
    $temp.remove();
    alert("입력하신  내용이 복사되었습니다.\n\nCtrl + v 키를 사용하여, 붙여 넣기를 사용하실 수 있습니다.");
}

// scroll menu event
onScroll = function (event) {
    var scrollPos = $(document).scrollTop();
    $('#menu a').each(function () {
        var currLink = $(this);
        var refElement = $(currLink.attr("href"));
        /*console.log("top: " + refElement.position().top);
        console.log("scroll: " + scrollPos);*/

        if (refElement.position().top - 501 <= scrollPos && refElement.position().top + refElement.height() - 301 > scrollPos) {
            $('#menu ul li a').removeClass("active");
            currLink.addClass("active");
        } else {
            currLink.removeClass("active");
        }

    });
}

if_mobile = function () {
    var windowWidth = $(window).width();
    if (windowWidth <= 640) {
        //창 가로 크기가 640 미만일 경우         
        // $(".nav-wrapper").addClass("invisible");
        // otherClick();
    } else {
        // $(".nav-wrapper").removeClass("invisible");
    }
}

otherClick = function () {
    $("body").on("click", function (e) {
        // alert("fdsa");
        var $container = $(".fixed_header, .form-header, .nav .next");
        // console.log(!$container.is(e.target));
        // console.log($container.has(e.target).length);

        // if the target of the click isn't the container nor a descendant of the container
        // console.log("fdsa");
        if (!$container.is(e.target) && $container.has(e.target).length === 0) {
            $(".nav-wrapper").addClass("invisible");
            // $(".form-header .nav-wrapper").addClass("invisible");
        } else {
            // $(".nav-wrapper").removeClass("invisible");
        }
    });
}


$(document).ready(function () {

    /*$(window).resize(function () {
        var windowWidth = $(window).width();
        if (windowWidth > 640) {
            $(".fixed_header .nav-wrapper").removeClass("invisible");
            $(".form-header .nav-wrapper").removeClass("invisible");
        }
    });*/

    var flag = false;

    $("body").on("click", ".nav-wrapper li", function () {
        if (!flag) {
            flag = true;
            setTimeout(function () {
                flag = false;
            }, 260);
            $(".nav-wrapper").toggleClass("invisible");
            $(this).addClass("active");
            $(this).siblings("li").removeClass("active");
        }
        return false;
    });
    /*$(".form-header li").removeClass("active");
    $(".nav-wrapper").addClass("invisible");*/

    otherClick();

    // mobile menu
    // if_mobile();

    $("body").on("click", ".fixed_header .button", function () {
        $(this).siblings(".nav-wrapper").toggleClass("invisible");
    });

    $(document).on("scroll", onScroll);

    //smoothscroll
    $("body").on("click", "#menu a[href^='#']", function (e) {
        e.preventDefault();
        // $(document).off("scroll");

        $("a").each(function () {
            $(this).removeClass("active");
        })
        $(this).addClass("active");

        var target = this.hash,
            menu = target;
        $target = $(target);
        $("html, body").stop().animate({
            "scrollTop": $target.offset().top - 10
        }, 500, "swing", function () {
            window.location.hash = target;
            // $(document).on("scroll", onScroll());
        });
    });

    // slide
    /*$('.gallery').each(function () {

        var $gal = $(this),
            $movable = $(".movable", $gal),
            $slides = $(">*", $movable),
            N = $slides.length,
            C = 0,
            itv = null;

        function play() {
            itv = setInterval(anim, 2000);
        }

        function stop() {
            clearInterval(itv);
        }

        function anim() {
            C = ($(this).is(".prev") ? --C : ++C) < 0 ? N - 1 : C % N;
            $movable.css({
                transform: "translateX(-" + (C * 100) + "%)"
            });
        }

        $(".gallery .prev").on("click", stop, anim);
        $(".gallery .next").on("click", stop, anim);
        $gal.hover(stop, play);
        play();

    });*/

    /*var delay = 3000,
        fade = 0;
    var banners = $('.slide');
    var len = banners.length;
    var i = 0;
    var autoslide = setInterval(cycle, delay);

    function cycle() {
        banner_nav;
        $(banners[i % len]).hide("slide", {
            direction: "left"
        });
        $(banners[++i % len]).show("slide", {
            direction: "right"
        });
        banner_nav.find("li").eq(i % len).find("a").addClass("active");
        banner_nav.find("li").eq(i % len).siblings("li").find("a").removeClass("active");
    }
    stopslide = function () {
        $(".form-request .container").on("mouseenter",function(){
            clearInterval(autoslide);
        });
        $(".form-request .container").on("mouseleave",function(){
            autoslide = setInterval(cycle, delay);
        });
    }
    stopslide();*/
    var arr = $(".slide-container");

    var getCurrentSlide = function () {
        var index = $("input[name=radio-btn]").index($("input[name=radio-btn]:checked"));
        return $($(".slide-container")[index]);
    }

    // var current = 0;
    function bgChange() {
        if (arr.length > 0) {
            // arr.removeClass("red");
            // arr.eq(current).find(".nav .next").click();

            // current = (current + 1) % arr.length;

            getCurrentSlide().find(".nav .next").click();
        }
    }
    var autoslide = setInterval(bgChange, 3000);

    $("body").on("mouseenter", ".form-request .container", function () {
        clearInterval(autoslide);
    });
    $("body").on("mouseleave", ".form-request .container", function () {
        autoslide = setInterval(bgChange, 3000);
    });

    var startCoords = 0;
    var endCoords = 0;
    // var tempY = 0;
    $(".slides").bind('touchstart', function (e) {
        // e.preventDefault();
        startCoords = endCoords = e.originalEvent.targetTouches[0].pageX;

        // tempY = e.originalEvent.targetTouches[0].pageY;
    });
    $(".slides").bind('touchmove', function (e) {
        clearInterval(autoslide);
        autoslide = setInterval(bgChange, 3000);

        endCoords = e.originalEvent.targetTouches[0].pageX;

        if (startCoords - endCoords > 60) {
            e.preventDefault();
        }
        if (endCoords - startCoords > 60) {
            e.preventDefault();
        }

    });
    $(".slides").bind('touchend', function (e) {
        e.preventDefault();
        // setTimeout(function () {
        if (startCoords - endCoords > 60) {
            getCurrentSlide().find(".nav .next").click();
        }
        if (endCoords - startCoords > 60) {
            getCurrentSlide().find(".nav .prev").click();
        }
        // }, 2000);

    });


    // word count
    $("#text").change(counter);
    $("#text").keydown(counter);
    $("#text").keypress(counter);
    $("#text").keyup(counter);
    $("#text").blur(counter);
    $("#text").focus(counter);

    var loading = function (isStart) {
        if (isStart == false) {
            $(".global_loading").remove();
        } else {
            $('body').prepend('<div class="global_loading">' +
                '<div class="mask-loading">' +
                '<div class="spinner">' +
                '<div class="double-bounce1"></div>' +
                '<div class="double-bounce2"></div>' +
                '</div>' +
                '</div>' +
                '</div>'
            );
        }
    }

    $("body").on("click", ".text_delete", function () {
        TextReset();
    });

    $("body").on("click", ".text_copy", function () {
        copyToClipboard($('#text'));
    });

    // 스크롤 이벤트
    $(document).on("scroll", function () {
        // console.log(window.pageYOffset);
        if (window.pageYOffset > 80) {
            $(".fixed_header").removeClass("invisible");
            $(".form-header #menu li").removeClass("active");
        } else {
            $(".fixed_header").addClass("invisible");
            $("nav #menu li").removeClass("active");
        }
        if (window.pageYOffset == 0) {
            $(".fixed_header .nav-wrapper").addClass("invisible");
        }

    });
    $(document).scroll();

    $("body").on("click", ".form-header-logo img", function () {
        location.href = "/";
    });


    // menu scroll
    $("body").on("click", ".free_request", function (e) {
        e.preventDefault();
        var targetOffset = $(".form-request-inside").offset().top - 75;
        $('html,body').animate({
            scrollTop: targetOffset
        }, 500);
        $(".form-request-inside").effect("highlight", {}, 850);
    });

    $(document).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $(".btn_top").removeClass("invisible");
        } else {
            $(".btn_top").addClass("invisible");
        }
    });
    $(document).scroll();

    // go to top
    $("body").on("click", ".btn_top", function () {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });

    $("body").on("change", ".original_language", function () {
        original_language_val = $(".original_language option:selected").val();
        count_span = $(".count_span");
        $("input[name='num']").val("");
        if (original_language_val > 0 && original_language_val < 4) {
            count_span.html("<sup>&nbsp;</sup>단어수");
        } else {
            count_span.html("<sup>&nbsp;</sup>글자수");
        };
    });

    $("body").on("submit", "#form-request", function (e) {
        e.preventDefault();
        var formData = new FormData();
        var email = $("input[type='email']").val();
        var name = $("input[name='name']").val();
        var original_language = $(".original_language option:checked").text();
        var target_language = $(".target_language option:checked").text();
        var message = $("[name='context']").val();
        var subject = $("input[name='subject']").val();
        var user_file = $('input[type=file]')[0].files[0];
        var text_counter = $("input[name=num]").val();
        deadline = $("input[name='deadline']").val();
        count_span = $(".count_span").text().trim();
        var coupon = $("input[name='coupon']").val();

        if (original_language == target_language) {
            alert("언어를 다르게 설정해주세요.");
        } else {
            formData.append("email", email);
            formData.append("name", name);
            formData.append("message", message);
            formData.append("subject", subject);
            formData.append("original_language", original_language);
            formData.append("target_language", target_language);
            formData.append("text_counter", text_counter);
            formData.append("deadline_date", deadline);
            formData.append("text_counter", text_counter);
            formData.append("coupon", coupon);
            if (user_file != undefined) {
                formData.append("file", user_file);
            }

            function escapeHtmlEntities(str) {
                if (typeof jQuery !== 'undefined') {
                    // Create an empty div to use as a container,
                    // then put the raw text in and get the HTML
                    // equivalent out.
                    return $(formData).text(str).html();
                }

                // No jQuery, so use string replace.
                return str
                    .replace(/&/g, '&amp;')
                    .replace(/>/g, '&gt;')
                    .replace(/</g, '&lt;')
                    .replace(/"/g, '&quot;');
            }
        }

        $.ajax({
            url: '/api/v2/user/sendSimpleRequest',
            data: formData,
            contentType: false,
            processData: false,
            cache: false,
            // dataType: "JSON",
            beforeSend: function () {
                loading(true);
            },
            type: 'POST'
        }).done(function (data) {
            // console.log(data);
            alert("번역 의뢰 성공");
            location.reload();
        }).fail(function (request, status, error) {
            console.log("code:" + request.status + "\n" + "message:" + request.responseText + "\n" + "error:" + error);
        }).always(function () {
            loading(false);

        });
        return false;
    });

    $("body").on("click", "button.text_counter", function (e) {
        e.preventDefault();
        $(".text_counter_container").removeClass("invisible");
        $("body").css("overflow", "hidden");
        $("#text").val("");
        $('#wordCount').html(" " + 0 + " ");
        $('#totalChars').html(" " + 0 + " ");
        $('#charCountNoSpace').html(" " + 0 + " ");
    });

    $("body").on("click", ".text_counter_popup .close", function (e) {
        e.preventDefault();
        var wordCountTrim = $("#wordCount").text().trim();
        var charCountNoSpaceTrim = $("#charCountNoSpace").text().trim();
        $("body").css("overflow", "visible");
        $(".text_counter_container").addClass("invisible");
        count_span = $(".count_span").text().trim();

        if (parseInt(wordCountTrim) > 0) {

            var testCount_confirm = confirm(count_span + '를 입력하시겠습니까?');
            if (testCount_confirm) {
                if (count_span != "글자수") {
                    $("input[name='num']").val(wordCountTrim);
                } else {
                    $("input[name='num']").val(charCountNoSpaceTrim);
                };
            }
        }
        return false;
    });
    $("body").on("click", ".text_counter_popup .text_input", function (e) {
        e.preventDefault();
        var wordCountTrim = $("#wordCount").text().trim();
        var charCountNoSpaceTrim = $("#charCountNoSpace").text().trim();
        $("body").css("overflow", "visible");
        $(".text_counter_container").addClass("invisible");
        count_span = $(".count_span").text().trim();

        var testCount_confirm = confirm(count_span + '를 입력하시겠습니까?');
        if (testCount_confirm) {
            if (count_span != "글자수") {
                $("input[name='num']").val(wordCountTrim);
            } else {
                $("input[name='num']").val(charCountNoSpaceTrim);
            };
        }
        return false;
    });

    $("body").on("keyup", function (e) {
        if ($(".text_counter_container").css("display") == "none") {
            // console.log("esc 방지");
        } else {
            if (e.keyCode == 27) {
                $(".text_counter_popup .close").click();
            }
        }
    });

    var currentTime = new Date();
    var dp_input, dp_inst;
    // currentTime.setDate(currentTime.getDate()+14);
    $(".input-deadline").datepicker({
        // inline: true,
        showOtherMonths: true,
        dateFormat: "yy-mm-dd",
        minDate: currentTime,
        defaultDate: 14,
        beforeShow: function (input, inst) {
            dp_input = input;
            dp_inst = inst;
            setTimeout(function () {
                dp_inst.dpDiv.css({
                    width: $(dp_input).outerWidth()
                });
                // inst.dpDiv.addClass("ll-skin-nigran");
            }, 0);
        },
        onChangeMonthYear: function (year, month, widget) {
            setTimeout(function () {
                dp_inst.dpDiv.css({
                    width: $(dp_input).outerWidth()
                });
                // inst.dpDiv.addClass("ll-skin-nigran");
            }, 0);
        }
    });

    $("#ui-datepicker-div").addClass("ll-skin-nigran");

});