

$(document).ready(function(){

	$(window).bind("pageshow", function() {
		$("form")[0].reset();

		// $("#setting-year").hide();
		// $("#setting-month").hide();
		
		$(".content-table tbody").empty();
		
		$("select[name=year]").val(new Date().getFullYear());
		$("select[name=month]").val((new Date().getMonth() + 1));
	});
	
	
	$("body").on('click', 'input[name=range]', function(e){
		
		$("#setting-year").show();
		$("#setting-month").show();
		if(e.target.id == "range1"){
			$("#setting-year").hide();
			$("#setting-month").hide();
		}
		else if(e.target.id == "range2"){
			$("#setting-month").hide();
		}
		search();

	});

	$("body").on('click', 'input[name=name]', function(e){
		search();
	});

	$("body").on('click', 'select, option', function(e){
		search();
	});

	// $("body").on('click', '#btnDownload', function(){

	// 	var link = document.createElement('a');
	// 	link.download = "export.xls";
	// 	link.href = 'data:,' + $('.content-table').text();
	// 	link.click();
	// });

	var btn = $('#btnDownload');
	var tbl = 'content-table';

	btn.click(function(e){

		var fileName = "export.xls";

		var a = document.createElement('a');
		var data_type = 'data:application/vnd.ms-excel';
		var table_div = document.getElementById( tbl );
		var table_html = table_div.outerHTML.replace(/ /g, '%20');

		a.href = data_type + ', ' + table_html;
		a.download = fileName;
		a.click();

		e.preventDefault();
	});

	
});

function search(){
	$(".content-table tbody").empty();
	var selectedYear = parseInt($("select[name=year] option:checked").val());
	var selectedMonth = parseInt($("select[name=month] option:checked").val());
	var selectedUser = $("input[name=name]:checked").val();

	if(!selectedUser){
		return;
	}

	$.each(data, function(i,item){
		// console.log(item["이메일주소"]);
		// console.log($("input[name=name]:checked").val());
		item['비고'] = item['비고'] || "";

		// if(item["이메일주소"] == "junhang.lee@ciceron.me")
		if(item["이메일주소"] == selectedUser)
		{
			
			var d = new Date(1900, 0, parseInt(item['날짜입력']) - 1,0,0, Math.round(parseFloat(item['시간입력'])* 86400));
			
			if($("input[name=range]:checked").val() == "byYear" || $("input[name=range]:checked").val() == "byMonth"){
				if(d.getFullYear() != selectedYear){
					return true;
				}
			}
			if($("input[name=range]:checked").val() == "byMonth"){
				if(d.getMonth() + 1 != selectedMonth){
					return true;
				}
			}
			if(!$("table tr[date="+d.format("yyyy-MM-dd")+"]").length){
				$(".content-table tbody").append(
					'<tr date="'+d.format("yyyy-MM-dd")+'">'
						+'<td class="날짜입력">'+ d.format("yyyy-MM-dd")+'</td>'
						+'<td class="출근 시간입력" style="background-color: #ffb2ae;"></td>'
						+'<td class="출근 비고" style="background-color: #ffb2ae;">'
						+'<a target="_blank" href="https://docs.google.com/a/ciceron.me/forms/d/e/1FAIpQLSekHLQEjm-A_EaG7w_H4wm2rMcLKqp884V20taPmmLf07i0Xw/viewform?entry.749571135='+ d.format("yyyy-MM-dd")+'&entry.1992406128=%EC%B6%9C%EA%B7%BC&entry.1130475430=10:00">입력하기</a>'
						+'</td>'
						+'<td class="퇴근 시간입력" style="background-color: #ffb2ae;"></td>'
						+'<td class="퇴근 비고" style="background-color: #ffb2ae;">'
						+'<a target="_blank" href="https://docs.google.com/a/ciceron.me/forms/d/e/1FAIpQLSekHLQEjm-A_EaG7w_H4wm2rMcLKqp884V20taPmmLf07i0Xw/viewform?entry.749571135='+ d.format("yyyy-MM-dd")+'&entry.1992406128=%ED%87%B4%EA%B7%BC(%EC%A1%B0%ED%87%B4)&entry.1130475430=18:00">입력하기</a>'
						+'</td>'
					+'</tr>');
			}
			// $("tr[date="+d.format("yyyy-MM-dd")+"] .날짜입력").text(d.format("yyyy-MM-dd"));
			if(item["출근/퇴근선택"] == "출근"){
				$("tr[date="+d.format("yyyy-MM-dd")+"] td.출근.시간입력").text(d.format("HH:mm"));
				$("tr[date="+d.format("yyyy-MM-dd")+"] td.출근.시간입력").css("background-color", "transparent");
				$("tr[date="+d.format("yyyy-MM-dd")+"] td.출근.비고").text(item['비고']);
				$("tr[date="+d.format("yyyy-MM-dd")+"] td.출근.비고").css("background-color", "transparent");
			}
			else if(item["출근/퇴근선택"] == "퇴근(조퇴)"){
				$("tr[date="+d.format("yyyy-MM-dd")+"] td.퇴근.시간입력").text(d.format("HH:mm"));
				$("tr[date="+d.format("yyyy-MM-dd")+"] td.퇴근.시간입력").css("background-color", "transparent");
				$("tr[date="+d.format("yyyy-MM-dd")+"] td.퇴근.비고").text(item['비고']);
				$("tr[date="+d.format("yyyy-MM-dd")+"] td.퇴근.비고").css("background-color", "transparent");
			}
			else if(item["출근/퇴근선택"] == "휴가"){
				$("tr[date="+d.format("yyyy-MM-dd")+"] td.퇴근.시간입력").text("휴가");
				$("tr[date="+d.format("yyyy-MM-dd")+"] td.퇴근.시간입력").css("background-color", "#bacad0");
				$("tr[date="+d.format("yyyy-MM-dd")+"] td.퇴근.비고").text(item['비고']);
				$("tr[date="+d.format("yyyy-MM-dd")+"] td.퇴근.비고").css("background-color", "#bacad0");
				$("tr[date="+d.format("yyyy-MM-dd")+"] td.출근.시간입력").text("휴가");
				$("tr[date="+d.format("yyyy-MM-dd")+"] td.출근.시간입력").css("background-color", "#bacad0");
				$("tr[date="+d.format("yyyy-MM-dd")+"] td.출근.비고").text(item['비고']);
				$("tr[date="+d.format("yyyy-MM-dd")+"] td.출근.비고").css("background-color", "#bacad0");

			}
			else if(item["출근/퇴근선택"] == "출장"){
				$("tr[date="+d.format("yyyy-MM-dd")+"] td.퇴근.시간입력").text("출장");
				$("tr[date="+d.format("yyyy-MM-dd")+"] td.퇴근.시간입력").css("background-color", "#b6e1e2");
				$("tr[date="+d.format("yyyy-MM-dd")+"] td.퇴근.비고").text(item['비고']);
				$("tr[date="+d.format("yyyy-MM-dd")+"] td.퇴근.비고").css("background-color", "#b6e1e2");

				$("tr[date="+d.format("yyyy-MM-dd")+"] td.출근.시간입력").text("출장");
				$("tr[date="+d.format("yyyy-MM-dd")+"] td.출근.시간입력").css("background-color", "#b6e1e2");
				$("tr[date="+d.format("yyyy-MM-dd")+"] td.출근.비고").text(item['비고']);
				$("tr[date="+d.format("yyyy-MM-dd")+"] td.출근.비고").css("background-color", "#b6e1e2");

			}
			else if(item["출근/퇴근선택"] == "결근"){
				$("tr[date="+d.format("yyyy-MM-dd")+"] td.퇴근.시간입력").text("결근");
				$("tr[date="+d.format("yyyy-MM-dd")+"] td.퇴근.시간입력").css("background-color", "#deaded");
				$("tr[date="+d.format("yyyy-MM-dd")+"] td.퇴근.비고").text(item['비고']);
				$("tr[date="+d.format("yyyy-MM-dd")+"] td.퇴근.비고").css("background-color", "#deaded");
				
				$("tr[date="+d.format("yyyy-MM-dd")+"] td.출근.시간입력").text("결근");
				$("tr[date="+d.format("yyyy-MM-dd")+"] td.출근.시간입력").css("background-color", "#deaded");
				$("tr[date="+d.format("yyyy-MM-dd")+"] td.출근.비고").text(item['비고']);
				$("tr[date="+d.format("yyyy-MM-dd")+"] td.출근.비고").css("background-color", "#deaded");

			}
			else{

			}
		}					
	});

	var firstDate = new Date($("td.날짜입력").first().text());
	// var lastDate = new Date($("td.날짜입력").last().text());
	var lastDate = new Date();

	if($("input[name=range]:checked").val() == "byYear"){
		firstDate = new Date(selectedYear, 0, 1);
		lastDate = new Date(selectedYear, 11, 31);
		
		if(lastDate > new Date()){
			lastDate = new Date();
		}
	}

	if($("input[name=range]:checked").val() == "byMonth"){
		firstDate = new Date(selectedYear, selectedMonth - 1, 1);
		lastDate = new Date(selectedYear, selectedMonth);
		lastDate.setDate(lastDate.getDate() - 1);
	}


	while (firstDate <= lastDate) {
		
		if(firstDate.getDay() >= 1 && firstDate.getDay() <= 5){
			if(!$("table tr[date="+firstDate.format("yyyy-MM-dd")+"]").length){
				$(".content-table tbody").append(
					'<tr date="'+firstDate.format("yyyy-MM-dd")+'">'
						+'<td class="날짜입력" style="background-color: #fef8cc;">'+ firstDate.format("yyyy-MM-dd")+'</td>'
						// +'<td class="출근 시간입력" style="background-color: #fef8cc;"></td>'
						+'<td colspan="2" class="출근 비고" style="background-color: #fef8cc;">'
						+'<a target="_blank" href="https://docs.google.com/a/ciceron.me/forms/d/e/1FAIpQLSekHLQEjm-A_EaG7w_H4wm2rMcLKqp884V20taPmmLf07i0Xw/viewform?entry.749571135='+ firstDate.format("yyyy-MM-dd")+'&entry.1992406128=%EC%B6%9C%EA%B7%BC&entry.1130475430=10:00">입력하기</a>'
						+'</td>'
						// +'<td class="퇴근 시간입력" style="background-color: #fef8cc;"></td>'
						+'<td colspan="2" class="퇴근 비고" style="background-color: #fef8cc;">'
						+'<a target="_blank" href="https://docs.google.com/a/ciceron.me/forms/d/e/1FAIpQLSekHLQEjm-A_EaG7w_H4wm2rMcLKqp884V20taPmmLf07i0Xw/viewform?entry.749571135='+ firstDate.format("yyyy-MM-dd")+'&entry.1992406128=%ED%87%B4%EA%B7%BC(%EC%A1%B0%ED%87%B4)&entry.1130475430=18:00">입력하기</a>'
						+'</td>'
					+'</tr>');
			}
		}

		firstDate.setDate(firstDate.getDate() + 1);
		// console.log(firstDate);
		
	}	


	$(".content-table tbody tr").sort(function(a, b) {
        var a = $(a).find('td:first').text();
        var b = $(b).find('td:first').text();
        if(a == b) return 0;
        return a>b ? 1 : -1;
    }).appendTo('.content-table tbody');

	var sum = 0;
	var totalVacation = 0;

	$.each($(".content-table tbody tr"), function(i, item){
		if($(item).find("td.출근.시간입력").text() == "휴가" 
		|| $(item).find("td.퇴근.시간입력").text() == "휴가"){
			totalVacation += 1;
			return true;
		}
		var a = Date.parse("1970-01-01 " + $(item).find("td.출근.시간입력").text()) / 1000;
		var b = Date.parse("1970-01-01 " + $(item).find("td.퇴근.시간입력").text()) / 1000;

		if(a<0||b<0||isNaN(a)||isNaN(b)) return true;
		sum+=b-a;
	});
	// console.log(sum);
	var sumDate = new Date(0,0,0,0,0,sum);
	$("#statistics-time").text(parseInt(sum / 60 / 60)+ "시간 " + sumDate.getMinutes() + "분");
	$("#statistics-vacation").text(totalVacation + "일");
	// $("#statistics-time").text(parseFloat(sum / 60 / 60).toFixed(2) + "시간");
	
}

Date.prototype.format = function(f) {
	if (!this.valueOf()) return " ";

	var weekName = ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"];
	var d = this;

	
	
	return f.replace(/(yyyy|yy|MM|dd|E|hh|mm|ss|a\/p)/gi, function($1) {
		switch ($1) {
			case "yyyy": return d.getFullYear();
			case "yy": return (d.getFullYear() % 1000).zf(2);
			case "MM": return (d.getMonth() + 1).zf(2);
			case "dd": return d.getDate().zf(2);
			case "E": return weekName[d.getDay()];
			case "HH": return d.getHours().zf(2);
			case "hh": return ((h = d.getHours() % 12) ? h : 12).zf(2);
			case "mm": return d.getMinutes().zf(2);
			case "ss": return d.getSeconds().zf(2);
			case "a/p": return d.getHours() < 12 ? "오전" : "오후";
			default: return $1;
		}
	});
};

String.prototype.string = function(len){var s = '', i = 0; while (i++ < len) { s += this; } return s;};
String.prototype.zf = function(len){return "0".string(len - this.length) + this;};
Number.prototype.zf = function(len){return this.toString().zf(len);};

