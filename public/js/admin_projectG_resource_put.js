//프로젝트 리소스 정보 수정
function get_resource_put(data) {
    $.each(data.data, function(index, item) {
        if(item.id == QueryString.rid) {
            var translator = "";
            var count = 0;
            console.log(data);
            $.each(item.translator_list, function(index2, item2) {
                if(item2.length == 1) translator = item2.name;
                else {
                    count++;
                    if(item2.length > count) {
                        translator += item2.name + ", ";
                    } else {
                        translator += item2.name;
                    }
                }
            });
            $("select[name='target_language_id'] option").eq(item.target_language_id).prop("selected", true);
            $("select[name='tone_id'] option").eq(item.tone_id).prop("selected", true);
            $("input[name='theme']").val(item.theme);
            $("input[name='translator']").val(translator);
            $("input[name='description']").val(item.description);
            $("select[name='read_permission_level']").find("option[value='" + item.read_permission_level + "']").prop("selected", true);
            $("input[name='price']").val(item.price);
            // $("input[name='is_original']").prop("checked", item.is_original);
            $.each(item.resource_info, function(index2, item2) {
                // $("#upload_file_view").append(
                //     "<div class='file_view'>" +
                //         "<span value='" + item2.id + "'>" + item2.file_url + "</span>" +
                //         "<select name='preview_permission'" + index2 + " class='preview_permission'>" + 
                //             "<option value='0'>미리보기 불가</option>" +
                //             "<option value='1'>미리보기 권한</option>" +
                //             "<option value='2'>다운로드 권한</option>" +
                //         "</select>" +
                //         "<label for='file_modify" + item2.id + "'>수정</label>" +
                //         "<input type='file' id='file_modify" + item2.id + "' class='file_modify' />" +
                //         "<img src='/img/xbox.jpeg' width='12' class='xax' />" +
                //     "</div>"
                // );
                var fileName = item2.file_url.split("/").pop()
                $("tbody").append(
                    "<tr class='file_view' value='" + item2.id + "'>" +
                        "<td>" +
                            "<input type='checkbox' id='file_check" + item2.id + "' value='" + item2.id + "' name='resource_file' /> " +
                            "<label for='file_check" + item2.id + "''>" + fileName + "</label>" +
                        "</td>" +
                        "<td> - </td>" +
                        "<td class='text_center'>" +
                            "<select name='preview_permission'" + index2 + " class='preview_permission'>" + 
                                "<option value='0'>미리보기 불가</option>" +
                                "<option value='1'>미리보기 권한</option>" +
                                "<option value='2'>다운로드 권한</option>" +
                            "</select>" +
                        "</td>" +
                        "<td class='text_center'>" +
                            "<label for='file_modify" + item2.id + "'>수정</label>" +
                            "<input type='file' id='file_modify" + item2.id + "' class='file_modify' />" +
                        "</td>" +
                    "</tr>"
                );
                $("select[name^='preview_permission']").eq(index2).children("option[value='" + item2.preview_permission + "']").prop("selected", true);
            });
        }
    });
}