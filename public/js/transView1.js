$(document).ready(function() {
    /*******************************************
    
        처음 시작화면 이벤트

    ********************************************/
    $("#loading1").css("display", "block");
    $("#loading1").delay(3000).slideUp();

    /*******************************************
    
        전역변수 : 
            var language: 언어선택 값 받는 변수.

    ********************************************/
    var language = 0;
    var lang = $("select[name='lang_select'] option:selected").text().trim();
    var arr = lang.split("->")
    
    $("#original_lang").text(arr[0]);
    $("#target_lang").text(arr[1]);

    /*******************************************
    
        번역물 글자수 체크 및 제한

    ********************************************/
    $("body").on("keyup", "textarea", function() {
        var content_count = $(this).val();
        $('.count').text(content_count.length + '/1000');
    });
    
    /*******************************************
    
        번역하기 클릭시 이벤트 내용

    ********************************************/
    $("body").on("click", ".translate button", function() {
        
        // console.log($(".input_translations div textarea").val())
        var formData = new FormData();
        if(language == 0) {
            formData.append("source_lang_id", 2);
            formData.append("target_lang_id", 1);
        } else if(language == 1) {
            formData.append("source_lang_id", 1);
            formData.append("target_lang_id", 2);
        }
        formData.append("sentence", $(".input_translations1 div textarea").val());
        $.ajax({
            url: "/api/v2/user/translator",
            type: "post",
            dataType: "json",
            processData: false,
            contentType: false,
            data: formData
        }).done(function(data) {
            // console.log(data)
            $("div.get_text").text(data.result);
            // if($("body").width() <= 779) {
            //     var offset = $(".get_translations1").offset();
            //     $("html, body").animate({scrollTop : (offset.top - 10)}, 600);
            // }
        }).fail(function(err) {
            console.log(err);
        });
        if($("body").width() <= 779) {
            var offset = $(".get_translations1").offset();
            $("html, body").animate({scrollTop : (offset.top - 10)}, 600);
        }
        $(".get_translations1 a").css("display", "block");
        save();
    });
    
    /*******************************************
    
        언어설정 변경

    ********************************************/
    $("body").on("change", "select[name='lang_select']", function() {
        $("textarea").val("");
        var lang = $("select[name='lang_select'] option:selected").text().trim();
        var langChage = lang.split("->")
        $("#target_lang").text(langChage[1]);
        $("#original_lang").text(langChage[0]);
        // console.log($("#original_lang_list").text())
        // console.log(langChage[0])
        // console.log(typeof(langChage[0]))
        if(langChage[0].indexOf("한국어")>-1) {
            language = 1;
            // console.log(language);
        } else if(langChage[0].indexOf("영어")>-1) {
            language = 0;
            // console.log(language);
        }
    });

    /*******************************************
    
        스크롤 이벤트

    ********************************************/
    var scroll = true;
    $(window).scroll(function() {
        // e.preventDefault();
        // a = $(window).scrollTop()
        console.log($(window).scrollTop());
        // console.log(typeof(a))
        
        if(scroll) {
            if($(window).scrollTop() >= 4){
                // $("aside").removeClass("scrollDown").addClass("scrollUp");
                $("aside").slideUp(200);
            }
            if($(window).scrollTop() <= 0){
                $("aside").slideDown(200);
                // $("aside").removeClass("scrollUp").addClass("scrollDown");
                // $("aside").animate({"margin-top": "0px"}, 200);
            }
        }
    });

    /*******************************************
    
        반응형 이벤트

    ********************************************/
    $(window).resize(function() {
        var screenWidth = $(window).innerWidth() + 15;
        if(screenWidth <= 640) {
            $("#ad img").attr("src", "img/baogao_M.png")
        } else {
            $("#ad img").attr("src", "img/baogao_banner.png")
        }
        console.log(screenWidth);
    });

    /*******************************************
    
        textarea 이벤트

    ********************************************/
    $("body").on("focus", "textarea", function() {
        $(this).parent().css({
            "box-shadow": "0 7px 14px rgba(0,0,0,0.25), 0 5px 10px rgba(0,0,0,0.22)",
            "-webkit-transition": "all 0.05s ease-in-out",
            "-moz-transition": "all 0.05s ease-in-out",
            "transition": "all 0.05s ease-in-out"
        });
    });
    $("body").on("blur", "textarea", function() {
        $(this).parent().css({
            "box-shadow": "1px 1px 1px 1px #bbbdc0",
            "-webkit-transition": "all 0.05s ease-in-out",
            "-moz-transition": "all 0.05s ease-in-out",
            "transition": "all 0.05s ease-in-out"
        });
    });

    /*******************************************
    
        세션에 번역할 내용 저장

    ********************************************/
    function save() {
        var $text = $("textarea");
        var $text_value = $text.val();
        console.log($text_value)
        sessionStorage.setItem("textValue", $text_value);
    }
});