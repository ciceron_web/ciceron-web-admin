$(document).ready(function () {


	var resHtml = $(".projectList").html();
	$(".projectList").empty();
	var nowPage = 1;
	var onLoading = false;
	var projectList = [];

	var loadData = function () {
		onLoading = true;
		$.ajax({
			url: '/api/v2/user/pretranslated/project?page=' + (nowPage++),
			processData: false,
			contentType: false,
			dataType: "json",
			cache: false,
			type: 'GET'
		}).done(function (data) {
			data.data.map(function (item) {
				projectList.push(item);
			});
			var html = projectList.map(data => eval('`' + resHtml + '`')).join('');
			$(".projectList").html(html);
		}).fail(function (xhr, ajaxOptions, thrownError) {
			console.log('error');
		}).always(function () {
			onLoading = false;
		});
	};
	loadData();
	$(window).on('scroll', function (e) {
		//$this.scrollTop() + $this.height() == $results.height()
		// console.log($(this).scrollTop());
		// console.log($(this).height());
		// console.log($(document).height());
		if ($(this).scrollTop() + $(this).height() >= $(document).height() - 50) {
			loadData();
			// console.log("fdsa");
		}

	});
});

var newProject = function () {
	///api/v2/admin/pretranslated/project
	var formData = new FormData();
	formData.append('original_lang_id', '1');
	formData.append('platform', '');
	formData.append('author', '');
	formData.append('cover_photo', new File([''], 'empty.png'), 'empty.png');
	formData.append('quote_url', 'original');

	$.ajax({
		url: '/api/v2/admin/pretranslated/project',
		processData: false,
		contentType: false,
		dataType: "json",
		cache: false,
		type: 'POST',
		data: formData
	}).done(function (data) {
		location.href = '/admin/pretranslated/' + data.project_id;
	}).fail(function (xhr, ajaxOptions, thrownError) {
		console.log('error');
	}).always(function () {
		onLoading = false;
	});
}