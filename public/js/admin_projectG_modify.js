//커버파일 이름 바꾸기
$("body").on("change", "input#cover", function() {
    var cover = $("#cover")[0].files[0];
    $("input.cover").val(cover.name);
})
//프로젝트 정보 수정
function get_project_modify(data) {
    var imgName = "";
    $.each(data.data, function(index, item) {
        if(item.id == QueryString.pid) {
            $("select[name='original_lang']").find("option[value='" + item.original_lang_id + "']").prop("selected", true);
            $("select[name='format']").find("option[value='" + item.format_id + "']").prop("selected", true);
            $("select[name='subject']").find("option[value='" + item.subject_id + "']").prop("selected", true);
            $("#author").val(item.author);
            imgName = item.cover_photo_url.split("/").pop();
            $("input.cover").val(imgName);
            // $("#description").val(item.description);
        }
    });
}