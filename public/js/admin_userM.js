//권한정보
function get_userM(data) {
    // console.log(data);
    var activity_id = 0;
    $.each(data.data, function(index, item) {
        if(item.is_active == true) activity_id = 0;
        else activity_id = 1;
        $(".table_board tbody").append(
            "<tr class='cursor'>" +
                "<td value='" + item.id + "' class='text_center'><span>" + item.id + "</span></td>" +
                "<td class='info'>" + item.name + "</td>" +
                "<td>" + item.email + "</td>" +
                "<td class='text_center'>" + 
                    "<select name='permission_id'>" +
                        "<option value='normal'>의뢰인</option>" +
                        "<option value='translator'>번역가</option>" +
                        "<option value='admin'>관리자</option>" +
                    "</select>" +
                "</td>" +
                "<td class='text_center'>" + item.recent_work_timestamp + "</td>" +
                "<td class='text_center'>" +
                    "<select name='activity_id'>" +
                        "<option value='active'>활동</option>" +
                        "<option value='deactive'>비활동</option>" +
                    "</select>" +
                "</td>" +
            "</tr>" +
            "<tr class='details'></tr>"
        );
        $("select[name='permission_id']").eq(index).children("option").eq(item.permission_level).prop("selected", true);
        $("select[name='activity_id']").eq(index).children("option").eq(activity_id).prop("selected", true);
    });
    

    var totalCnt;
    if((data.data.length/20)<0){
        totalCnt = 1;
    } else {
        totalCnt = Math.ceil(data.data.length/20);
    }

    pageNumber("table", 20, ".cursor", totalCnt);
    $("tr.details").hide();
}