$("body").on("click", "#menu ul li", function() {
	$("#menu ul li").removeClass("menu_select");
	$(this).addClass("menu_select");
});
	

// 이미지 불러오기

function fileInfo(f){
	var file = f.files; // files 를 사용하면 파일의 정보를 알 수 있음

	// 파일의 갯수만큼 반복
	for(var i=0; i<file.length; i++){

		// console.log('파일명: '+file[i].name + '\n사이즈: '+file[i].size+'\n타입: '+file[i].type);


		if(file[i].size > 1024*1024){
			alert('1MB 이하의 파일을 올려주세요.');
			return;
		} else if(file[i].type.indexOf('image')<0){
			alert('이미지 파일이 아닙니다.');
			return;
		}

		var reader = new FileReader(); // FileReader 객체 사용

		reader.onload = function(rst){

			var rtr = rst.target.result;
			$('#img_box').append('<div class="floatImg"><input type="checkbox" class="imgCheck" name="box"><img src="'+rtr+'" class="selectImg" onclick="doImgPop(this)"/><i class="fa fa-times" aria-hidden="true"></i></div>'); // append 메소드를 사용해서 이미지 추가
			// 이미지는 base64 문자열로 추가
			// 이 방법을 응용하면 선택한 이미지를 미리보기 할 수 있음

			//이미지에 호버 버튼 생성
			var selBox = '<div class="selBox"><div class="cropImg">Crop</div><div class="stayImg">Stay</div><div class="delImg">Delete</div></div>'

			
			$('.floatImg').off('mouseenter').off('mouseleave').on(
				{
					mouseenter: function(){

						$(this).append(selBox);

						
						$('.stayImg').on('click', function(){
							console.log('Stay');
						});

						$('.delImg').on('click', function(){
							$(this).parents('.floatImg').remove();
						});

					}, 
					mouseleave: function(){
						$(this).find('.selBox').remove();
					}
				}
			);

			$('#fileUpload').val('');


		}


		reader.readAsDataURL(file[i]); // 파일을 읽는다

	}
}


function doImgPop(rtr){ 
	img1= new Image();
	// console.log($(rtr).attr("src")); 
	// console.log(this); 
	
	img1.src=($(rtr).attr("src")); 
	imgControll(rtr); 
} 

function imgControll(rtr){ 
	if((img1.width!=0)&&(img1.height!=0)){ 
		viewImage(rtr); 
	} 
	else{ 
		controller="imgControll('"+rtr+"')"; 
		intervalID=setTimeout(controller,20); 
	} 
}
function viewImage(rtr){ 
	W=img1.width; 
	H=img1.height; 
	L=(screen.width/2)-(400/2);
	T=(screen.height/2)-(400/2);
	O="width=400,height=400,left=0,top=-100"; 

	
	imgWin=window.open("cropPop.ejs","crop",O); 
	// imgWin.addEventListener("load",function() {
	// 	console.log(imgWin.document);
	// 	alert("fdas");
	// });
	setTimeout(function() {
		var img = imgWin.document.getElementById("img_box");
		console.log(img);
	}, 100);
	// img.src = rtr.src;

	// imgWin.document.write("<html><head><title>이미지 Crop</title><style>*{text-align:center;} img {margin-top:10px;} button {margin-top:10px; width:50px; height:30px;} </style></head>");
	// imgWin.document.write("<body topmargin='0' leftmargin='0'>");
	// imgWin.document.write("<div class='floatImg'><img class='selectImg' width='300' src='"+rtr.src+"' onclick='self.close()'/><br><button class='cropImg' onclick='cropper()'>Crop</button></div>");
	imgWin.document.close();
	// imgWin.document.cropper();
}


//크롭기능 생성
$('body').on('click', '.cropImg', function(){
	$('.selectImg').cropper('destroy');
	$(this).parents('.selBox').siblings('.selectImg').cropper({

	crop: function(e) {
		// Output the result data for cropping image.
		// console.log(e.x);
		// console.log(e.y);
		// console.log(e.width);
		// console.log(e.height);


		$('#imgInfo').off('click').on('click',function(){
				$(this).parents().find('.selectImg').cropper('destroy');
				console.log('X위치: '+e.x+'\nY위치: '+e.y+'\n가로: '+e.width+'\n세로: '+e.height);
				alert('데이터 전송 완료');
				return false;
			});
			
		}
	});

});

//불러온 이미지 삭제
$('body').on('click', '.fa-times', function(){
	$(this).parents('.floatImg').remove();
});

//crop 버튼 새창
// $("body").on("click",".cropImg",function(){
// 	window.open("http://www.naver.com","Naver","width=400px, height=300px");
// });

//전체 checkbox 제어
$("body").on("click","#allCheck", function(event){
	if(this.checked){
		$(":checkbox").each(function(){
			this.checked = true;
		});
	} else {
		$(":checkbox").each(function(){
			this.checked = false;
		});
	}
});

//check된 요소 제어
$("body").on("click", "#stayImg", function(){
	if($("input:checkbox[name=box]").is(":checked")==true){
		var StCheck = $("input:checkbox[name=box]:checked");
		var stayRst = StCheck.parents('.floatImg').length;
		console.log(stayRst+"개 Stay");

	} 
});

$("body").on("click", "#delImg", function(){
	if($("input:checkbox[name=box]").is(":checked")==true){
		var DelCheck = $("input:checkbox[name=box]:checked");
		// $(this).parents('#MyCheck').siblings('#img_box').children('.floatImg').remove();
		DelCheck.parents('.floatImg').remove();

	} 
});