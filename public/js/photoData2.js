$(document).ready(function() {

    var nowClickedLi = 0;
    
    /* ----------------------- photoTagData(JSONArray) 설정 시작 ----------------------- */
    data = [
        {
            id: "tag1", 
            image_url:["/img/icon.png","/img/icon.png","/img/icon.png"]
        },
        {
            id: "tag2", 
            image_url:["/img/icon.png"]
        },
        {
            id: "tag3", 
            image_url:["/img/icon.png","/img/icon.png"]
        },
        {
            id: "tag4", 
            image_url:["/img/icon.png","/img/icon.png","/img/icon.png","/img/icon.png"]
        },
        {
            id: "tag5", 
            image_url:["/img/icon.png","/img/icon.png","/img/icon.png","/img/icon.png"]
        },
        {
            id: "tag6", 
            image_url:["/img/icon.png","/img/icon.png","/img/icon.png","/img/icon.png","/img/icon.png"]
        },
        {
            id: "tag7",
            image_url:["/img/icon.png","/img/icon.png","/img/icon.png","/img/icon.png","/img/icon.png","/img/icon.png","/img/icon.png","/img/icon.png","/img/icon.png","/img/icon.png"]
        }
    ];

        // var formData = new FormData($("#form")[0]);
        // var formData = new FormData();
        
        // formData.append("page", "3");

    /* ----------------------- 해시태그 정렬 시작 ----------------------- */
    $.each(data, function(i, item){
        $("#tagView ul").append("<li value='"+i+"' class='tagli'>"+item.id+"</li>");
    });
    
    /* ----------------------- click 이벤트 시작 ----------------------- */
    $("body").on("click", "#tagView ul li", function(){

        $("input:checkbox[id='allCheck']").prop("checked", false);

        // $("#photoName").text($(this).text());
        $("#tagView ul li").removeClass("selected");
        $(this).addClass("selected");
        nowClickedLi = $(this).attr("value");
        var selBox = "<div class='selBox'><div class='cropImg'>Crop</div><div class='stayImg'>Stay</div><div class='delImg'>Delete</div></div>"

        $("#photo_view #photoName").remove();
        $("#photo_view .photoSort").remove();
        $("#photo_view").prepend("<div id='photoName'>"+$(this).text()+"</div>");

        // console.log(photoTagData[0].class[nowClickedLi])
        for(var x=0; x<data[nowClickedLi].image_url.length; x++) {
            
            $("#photo_view #tagImg").append(
                "<li class='photoSort' value="+x+">" +"<input type='checkbox' class='imgCheck' name='box'>" + "<img width='100' src='"
                + data[nowClickedLi].image_url[x]+"' alt='"+data[nowClickedLi].id+"' >" + selBox + "</li>"
            );

            $("div.holder").jPages({
                containerID : "tagImg",
                perPage : 6,
                keyBrowse : true
            });
        }

        // $.each(categoryData, function(i, item){
        //     $("select[name='category1']").append(
        //         "<div>" +
        //             "<img src='"+categoryData.name+"' alt='"+categoryData.name+"'>" +
        //             "<div><div>Add</div><div>Del</div></div>" +
        //         "</div>"
        //     );
        // })
        // $.each(categoryData, function(i, item){
        //     if(tagData[nowClickedLi].category1 == i) {
        //         for(var i=0; i<item["class"].length; i++) {
        //             $("select[name='category2']").append("<option value='"+i+"'>"+item.class[i]+"</option>");
        //         }
        //     }
        // });

        // $("select[name='category1']").val(tagData[$(this).attr("value")].category1).prop("selected", true);
        // $("select[name='category2']").val(tagData[$(this).attr("value")].category2).prop("selected", true);

    });
    $("#tagView ul li").first().click();

    /* ----------------------- option의 체인지 이벤트 시작 ----------------------- */
    $("body").on("change", "[name=category1]", function() {
        var num = $(this).val();
        data[$("li[value="+nowClickedLi+"]").attr("value")].category1 = $(this).val();
        
        $("select[name='category2'] option").remove();
        $.each(data, function(i, item){
            if(num == i) {
                for(var i=0; i<item["image_url"].length; i++) {
                    $("select[name='category2']").append("<option value='"+i+"'>"+item.image_url[i]+"</option>");
                }
            }
        });
    });
    $("body").on("change", "[name=category2]", function() {
        data[$("li[value="+nowClickedLi+"]").attr("value")].category2 = $(this).val();
    });

    /* ----------------------- 정보 관리 이벤트 시작 ----------------------- */
    $("body").on("click", "#addG", function(){
        $("li.selected").removeClass("bad");
        $("li.selected").addClass("good");
    });
    $("body").on("click", "#addT", function(){
        $("li.selected").removeClass("good");
        $("li.selected").addClass("bad");
    });

    /* ----------------------- 전체 이미지 checkbox 제어 ----------------------- */
    $("body").on("click","#allCheck", function(event){
        event.preventDefault();

        if(this.checked){
            $("input:checkbox[class='imgCheck']").each(function(){
                this.checked = true;
            });
        } else {
            $("input:checkbox[class='imgCheck']").each(function(){
                this.checked = false;
            });
        }
    });

    /* ----------------------- 각 이미지 checkbox 제어 ----------------------- */
    $("body").on("click",".photoSort img", function(e){

        var floatCheck = $(this).siblings("input:checkbox[class='imgCheck']");
        if(floatCheck.prop("checked")==false){
            floatCheck.prop("checked", true);
        } else if (floatCheck.prop("checked")==true) {
            floatCheck.prop("checked", false);
        }
        
    });


    /* ----------------------- 전체 stay 버튼 제어 ----------------------- */
    

    $("body").on("click", "#stayImg", function(){

	
        $("input:checkbox[id='allCheck']").prop("checked", false);
        var stayLi = $(this).parents("#photo_contents").find(".selected");
        
        if($("input:checkbox[name=box]").is(":checked")==true){
            
            var StCheck = $("input:checkbox[name=box]:checked");
            var stayRst = StCheck.parents('.photoSort').length;
            console.log(stayRst+"개 Stay");
            $("input:checkbox[name=box]").prop("checked", false);

        } 

        StCheck.parents(".photoSort").remove();
        // $("#tagView ul li").first().click();
    });


/* ----------------------- 이미지 1개 stay 버튼 ----------------------- */


    $("body").on("click", ".stayImg", function(){

        var stayP = $(this).parents(".photoSort").attr("value");
        // console.log(stayP);
        var stayLi = $(this).parents("#photo_contents").find(".selected");
        
        $("#tagView ul li").first().click();


    })


    /* ----------------------- 전체 delete 버튼 제어 ----------------------- */

    var imgVal = [];
    //이미지 delete
    $("body").on("click", "#delImg", function(){

        $("input:checkbox[id='allCheck']").prop("checked", false);

        var num = $("input[name^='box']:checked").length;
        for (var i=0; i<num; i++){
            var x = $("input[name^='box']:checked").eq(i).parents(".photoSort").attr("value");
            console.log(x);
            imgVal.push(x);
        }
        console.log(imgVal);

        if($("input:checkbox[name=box]").is(":checked")==true){
            var DelCheck = $("input:checkbox[name=box]:checked");
            // $(this).parents('#MyCheck').siblings('#img_box').children('.floatImg').remove();
            DelCheck.parents('.photoSort').remove();
        }


        imgVal = [];
        console.log(imgVal);

    });

    /* ----------------------- 이미지 1개 delete 버튼 제어 ----------------------- */
    $("body").on("click", ".delImg", function(){

        $(this).parents(".photoSort").remove();
    });




/* ----------------------- crop 팝업창 열기 ----------------------- */

    // crop img 정보 보내기


    $("body").on("click", ".cropImg", function() {

        var childImg = $(this).parents('.selBox').siblings('img').attr('src');
        var sameImg = $(this).parents('.photoSort');

        // console.log(sameImg);

        $("#pop_crop").css("display", "block");
        $('#crop_pic img').attr("src",childImg);
        $('#crop_pic img').cropper('destroy');
        $('#crop_pic img').cropper({

            crop: function(e) {
                // Output the result data for cropping image.
                // console.log(e.x);
                // console.log(e.y);
                // console.log(e.width);
                // console.log(e.height);


                $('#cropBtn button').off('click').on('click',function(){
                    $(this).parents('#cropBtn').siblings('#crop_pic').children('img').cropper('destroy');
                    // console.log('X위치: '+e.x+'\nY위치: '+e.y+'\n가로: '+e.width+'\n세로: '+e.height);

                    var cropVal = e.x +", "+e.y+", "+e.width+", "+e.height;
                    
                    // $(this).parents().find(sameImg).attr('data-value',cropVal);

                    var Cropdata = new Array();
                    Cropdata.push(cropVal);
                    console.log(Cropdata);

                    $(this).parents("#pop_crop").css("display","none");
                    return false;
                });
            }
        });
    });

    // crop창 close
    $("body").on("click", "#crop1 #close", function() {
        $("#pop_crop").css("display", "none");
    });

/* ----------------------- crop 데이터 전송 ----------------------- */

// 현재 있는 json data. tag 불러오기.

    var tagData = {
        data: []
    }

    for(i=0; i<data.length; i++){

        var photoContent = data[i].id+" "+data[i].image_url
        // console.log(JSON.stringify(photo[i]));
        tagData.data.push({
            "id":i,
            "name":data[i].id
        })
    }

    console.log(JSON.stringify(tagData));

// test 
    /*$('#photo_view').pagination({
        dataSource: [1-100],
        pageSize: 5,
        pageNumber: 3,
        callback: function(data, pagination) {
            // template method of yourself
            var html = template(data);
            $("#photo_view").html(html);
        }
    });*/

});

