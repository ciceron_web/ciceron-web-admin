$(document).ready(function(){

	$("body").on("click", "#menu ul li", function() {
         $("#menu ul li").removeClass("menu_select");
         $(this).addClass("menu_select");
    });
    
	$('#result .pairing_box').empty().css("border","none");	
	$('.blank1').css("display","none");

	$('#btnParse').on("click", function(){

		$('#result .text_box , .pairing_box, .blank1, .blank2').remove();

		var lang1 = $('#lang1 option:selected').text();
		var lang2 = $('#lang2 option:selected').text();
		
		var lang1Text = $('#userTxt1').val();
		var lang2Text = $('#userTxt2').val();

		var splitedText1 = lang1Text.split('.');
		var splitedText2 = lang2Text.split('.');


		if(lang1 == lang2) {
			alert ("언어를 다르게 설정해 주세요.")
		} else if (lang1Text=='') {
			alert ("언어1 텍스트 박스가 비어있습니다.")
		} else if (lang2Text=='') {
			alert ("언어2 텍스트 박스가 비어있습니다.")
		} else {
			$('.lang_box:nth-child(1) span').text(lang1);
			$('.lang_box:nth-child(2) span').text(lang2);

			var splitedTextVal = splitedText1.length > splitedText2.length ? splitedText1.length : splitedText2.length;
			var textLength1 = splitedText1.length;
			var textLength2 = splitedText2.length;
			

			for(i=0; i<splitedTextVal; i++){

				if ( splitedText1[i] == undefined) {
					splitedText1[i] = '';
					
				} else if ( splitedText2[i] == undefined) {
					splitedText2[i] = '';
				} 

				if(i<splitedTextVal-1 ){

					$('#result').append('<div class="pairing_box"><div class="text_box"><span class="ui-state-default" id="lang1_'+i+'">'
					+splitedText1[i]+'.'+'</span></div>'+
					'<div class="text_box"><span class="ui-state-default" id="lang2_'+i+'">'+splitedText2[i]+'.'+
					'</span></div></div>'+'<div class="blank1"></div>'+'<div class="clearFix"></div>');

				// $('#result').append('<div class="pairing_box"><div class="text_box"><span class="ui-state-default" id="lang1_'+i+'">'
				// +splitedText1[i]+'</span></div>'+
				// '<div class="text_box"><span class="ui-state-default" id="lang2_'+i+'">'+splitedText2[i]+
				// '</span></div></div>'+'<div class="blank1"></div>');
					
				}  else {

					$('#result').append('<div class="pairing_box"><div class="text_box"><span class="ui-state-default" id="lang1_'+i+'">'
					+splitedText1[i]+'</span></div>'+
					'<div class="text_box"><span class="ui-state-default" id="lang2_'+i+'">'+splitedText2[i]+
					'</span></div></div>');

				}
			};

			var spanVal = $(".pairing_box").find("span");
			var spanLength = $(".pairing_box").find("span").length;
			$(spanVal).each(function(index){
				if(index<=spanLength-3){
					if($(this).text()=="."){

						$(this).remove();
					}
				}
				//  else if (index==spanLength){
				// 	if($(this).text()=="."){

				// 		$(this).remove();
				// 	}
				// }
			});

			var textVal = $(".pairing_box").find(".text_box");
			var textLength = $(".pairing_box").find(".text_box").length;
			$(textVal).each(function(index){
				if(index<=textLength){
					if($(this).text()==""){
						$(this).css("border","none");
					} else {
						$(this).css("border","3px solid #f2f2f2");
					}
				}
			});

			var last_pair = $(".pairing_box").last();
			last_pair.remove();

			var last_blank = $(".blank1").last();
			last_blank.remove();

			var Doc1 = $('#kind_Doc option:selected').text();
			var Maj1 = $('#kind_Maj option:selected').text();
			var Tone1 = $('#kind_tone option:selected').text();
			var c_Name = $('#cName').val();

			$('.kind_box:nth-child(1) span').text(Doc1);
			$('.kind_box:nth-child(2) span').text(Maj1);
			$('.kind_box:nth-child(3) span').text(Tone1);
			$('.kind_box:nth-child(4) span').text(c_Name);

			$('.blank1').on('click', function(){
				$(this).toggleClass('blank2').text('문단 나누기 취소');
			});

		$('.blank1').hover(function(){
			$(this).text('문단을 나누시려면 클릭하세요.').css({'font-size':'0.8rem','text-align':'right'});
				}, function(){
			$(this).text('');
		});

		$('.text_box span').hover(function(){
			$(this).css({'background':'rgba(0,0,0,0.2)'});
		}, function(){
			$(this).css({'background':'none'});
		});



        $(function(){

            $('.text_box').droppable({
				accept:'.text_box span',
				drop:function(event, ui){
					//$(this).removeClass('.text_box');
					var dropped = ui.draggable;
					var droppedOn = $(this);
					$(dropped).detach().css({top:0}).prependTo(droppedOn);
					
				}
			});
				
			$('.text_box span').droppable({
				accept:'.text_box span',
				drop:function(event, ui){
					//$(this).removeClass('.text_box');
					var dropped = ui.draggable;
					var droppedOn = $(this);
					$(dropped).detach().css({top:0}).insertAfter(droppedOn);

					//$('.text_box:has(span)').css('border','3px solid #f2f2f2');
					var right_textBox = $('.text_box:nth-child(2)');

					for(i=0; i<right_textBox.length; i++){
						// console.log(right_textBox[i]);
						if(right_textBox[i].children.length==0){
							for(j=i; j<right_textBox.length - 1;j++){
								$(right_textBox[j]).append(right_textBox[j+1].children);
							}
							for(j=i; j<right_textBox.length;j++){
								// console.log(right_textBox[j].children.length);
								if(right_textBox[j].children.length==0){
									$(right_textBox[j]).css({'border':'none'});
								}
							}
							break;
							
						} else {
							// console.log([i]+'그대로 가라')
						}
					}
				}
			});
				
			$('.text_box span').draggable({
				axis:'y',
				start: function(event, ui){
					dropped = false;
				},
				stop: function(event, ui){
					if(dropped==true){
						$(this).remove();
					}
				},
				refreshPositions:true,
				revert:'invalid',
				zIndex:1,
				// connectToSortable:'.text_box'
				
			});

				// var span_box = $('.text_box:has(span)');
				// $(span_box).css('border','3px solid #f2f2f2');

				$('div, span').disableSelection();
			});

		};

		var paragraphNum = 1;
		var sentenceNum = 1;

		var formData = {
			original_string: [],
			translated_string: []
		}

		formData.original_string.push({"sentences": []});

		$.each($("#result").find("[id^='lang1_']"), function(index, item){
			console.log(item);
			formData.original_string[paragraphNum-1].sentences.push({
				"sentence": $($(item)[0]).text()
			});
			sentenceNum++;
		});

		formData.translated_string.push({"sentences": []});

		$.each($("#result").find("[id^='lang2_']"), function(index, item){
			console.log(item);
			formData.translated_string[paragraphNum-1].sentences.push({
				"sentence": $($(item)[0]).text()
			});
			sentenceNum++;
		});

		console.log(JSON.stringify(formData));

	}); 
	//submit 버튼

	$('body').on("click","#btnSubmit",function() { 

		var lang1 = $('#lang1 option:selected').val();	
		var lang2 = $('#lang2 option:selected').val();
        var Doc1 = $('#kind_Doc option:selected').val();
        var Maj1 = $('#kind_Maj option:selected').val();
        var Tone1 = $('#kind_tone option:selected').val();
        var c_Name = $('#cName').val();
		var str = '';
		var spanText = $('.pairing_box');
		//var pairText;
		
		for(i=0; i<spanText.length; i++){
			str += $('.pairing_box').eq(i).text() + "\n";
		}

		// console.log("종류: "+Doc1+" / 분야: "+Maj1+" / 어조: "+Tone1+" / 고객: "+c_Name+'\n'+str);

		var formData = {
			original_langguage_id: lang1,
			target_language_id:lang2,
			subject_id: Maj1,
			format_id: Doc1,
			tone_id: Tone1,
			data: []
		}

		var nowParagraph = 1;
		var nowSentence = 1;
		
		formData.data.push({"paragraph_id": nowParagraph, "sentences": []}); 
		console.log(formData.data);
		$.each($("#result").find(".pairing_box, .blank2"), function(index, item){
			console.log(item);
			if($(item).hasClass("pairing_box")){
				formData.data[nowParagraph - 1].sentences.push({
					"sentence_id": nowSentence,
					"original_sentence": $($(item).find(".text_box")[0]).text(),
					"translated_sentence": $($(item).find(".text_box")[1]).text()
				});
				nowSentence++;
			}
			else if($(item).hasClass("blank2")){
				nowParagraph++;
				nowSentence = 1;
				formData.data.push({"paragraph_id": nowParagraph, "sentences": []}); 
				
			}
		});

		console.log(JSON.stringify(formData, null, 4));

		// $.ajax({
		// 	url : "/api/v2/admin/dataManager/import",
		// 	type : "POST",
		// 	dataType : 'JSON',
		// 	data : formData,
		// 	contentType: application/json,
		// 	processData: false 
		// }).done(function(data){
		// 	console.log(data);
		// }).fail(function(err){
		// 	console.log(err);
		// 	alert("에러!");
		// });
	});
});


