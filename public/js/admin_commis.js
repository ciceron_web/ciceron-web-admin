//의뢰물관리
function get_commis(data) {
    $.each(data.data, function(index, item) {
        $(".table_board tbody").append(
            "<tr class='cursor'>" +
                "<td value='" + item.id + "' class='text_center'><span>" + item.id + "</span></td>" +
                "<td>" + item.requester_name + "</td>" +
                "<td>" + item.requester_email + "</td>" +
                "<td>" + subject[item.subject_id] + "</td>" +
                "<td class='text_center' title='" + lang[item.original_lang_id] + "->" + lang[item.target_lang_id] + "'>" +
                    shortLang[item.original_lang_id] + " <span class='oval'>></span> " + shortLang[item.target_lang_id] +
                "</td>" +
                "<td class='text_center'>" + item.request_time + "</td>" +
                "<td>" + item.order_number + "</td>" +
                "<td class='text_center'>" +
                    "<select name='status'>" +
                        "<option value='ongoing'>진행</option>" +
                        "<option value='complete'>완료</option>" +
                        "<option value='hide'>숨김</option>" +
                        "<option value='stop'>정지</option>" +
                    "</select>" +
                "</td>" +
            "</tr>"
        );
        // pageNumber("table", 8, ".cursor");
        $("select[name='status']").eq(index).children("option").eq(item.status).prop("selected", true);
    });


    var totalCnt;
    if((data.data.length/8)<0){
        totalCnt = 1;
    } else {
        totalCnt = Math.ceil(data.data.length/8);
    }

    pageNumber("table", 8, ".cursor", totalCnt);
}