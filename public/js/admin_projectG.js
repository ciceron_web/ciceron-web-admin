// 파일 이름 넣기
$("body").on("change", "#cover", function () {
    var $files = $(this)[0].files[0];
    $("input[name='cover_name']").val($files.name);
});

var page = 1;
var totalCnt;

var loadData = function (page) {

    $.ajax({
        url: "/api/v2/user/pretranslated/project",
        type: "get",
        contentType: false,
        processData: false,
        cache: false,
        // data: "page=" + page + "&itemPerPage=20"
        data: "page=" + page
    }).done(function (data) {

        // console.log(data);
        $("table tbody").empty();

        if (!totalCnt) {

            totalCnt = data.cnt;
            pageNumber("table", 20, ".cursor", totalCnt);
        }

        $.each(data.data, function (index, item) {
            // console.log(item);
            $("table tbody").append(
                "<tr class='cursor'>" +
                "<td value='" + item.id + "' class='text_center'><span>" + item.id + "</span></td>" +
                "<td class='sel'>" + item.original_lang + "</td>" +
                "<td>" + format[item.format_id] + "</td>" +
                "<td>" + item.author + "</td>" +
                "<td class='text_center'>" + item.project_register_timestamp + "</td>" +
                "<td class='text_center'>" +
                "<a href='/admin/projectG/modify?pid=" + item.id + "'>" +
                "<img src='../img/modify_icon.png' alt='수정' width='30' />" +
                "</a>" +
                "</td>" +
                // "<td class='text_center'><a href='/admin/projectG/modify?pid=" + item.id + "'>수정</a></td>" + //이미지 없어서 바꿈.
                "<td class='text_center'>" +
                "<input type='checkbox' name='choose'>" +
                "</td>" +
                "</tr>"
            );
        });

        return false;

    }).fail(function (err) {
        console.log(err);

    });
}

/*
$("body").on("click", ".prev", function () {
    if (page > 1) {
        page--;
    }
    loadData(page);
});

$("body").on("click", ".next", function () {
    totalPage = totalCnt / 20;
    if (totalPage > page) {
        page++;
    }
    loadData(page);
});

loadData(page);
*/


$("body").on("click", ".prev", function () {
    if (totalCnt > 1) {
        if (page >1) {
            page -- ;
        } else {
            page = 1;
        }
    }

    loadData(page);
});

$("body").on("click", ".next", function () {
    // totalPage = totalCnt / 20;
    var totalPage = parseInt($(".page-number.active").text());
    if (totalPage < totalCnt) {

        if (page > totalCnt) {
            page = totalCnt;
        } else {
            page++;
        }
    }
    loadData(page);
});

loadData(page);