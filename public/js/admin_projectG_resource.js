//프로젝트 리소스 보기
function get_project_resource(data) {
    if (data.data.length <= 0) {
        pageNumber("table", 8, ".cursor");

        setTimeout(function(){
            alert("데이터가 없습니다.");
        },10);
        
    } else {
        $.each(data.data, function (index, item) {
            var translator = "";
            var count = 0;
            $.each(item.translator_list, function (index2, item2) {
                count++;
                if (item2.length == 1) translator = item2.name;
                else {
                    if (item2.length > count) {
                        translator += item2.name + ", ";
                    } else {
                        translator += item2.name;
                    }
                }
            });
            $("table tbody").append(
                "<tr class='cursor'>" +
                "<td value='" + item.id + "' class='text_center'><span>" + item.id + "</span></td>" +
                "<td class='sel'>" + item.theme + "</td>" +
                "<td>" + item.target_language + "</td>" +
                "<td>" + tone[item.tone_id] + "</td>" +
                "<td>" + translator + "</td>" +
                "<td class='text_center'>" +
                "<input type='checkbox' name='choose" + index + "'>" +
                "</td>" +
                "</tr>"
            );
        });

        var totalCnt;
        if((data.data.length/8)<0){
            totalCnt = 1;
        } else {
            totalCnt = Math.ceil(data.data.length/8);
        }

        pageNumber("table", 8, ".cursor", totalCnt);
    }
}