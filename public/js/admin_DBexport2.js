$(document).ready(function() {
	
	completes = [
		{
			"no":"1",
			"origin":"English",
			"target":"Korean",
			"Dkind":"보고서",
			"field":"건축",
			"tone":"철학",
			"client":"의뢰인"
		},
		{
			"no":"2",
			"origin":"Korean",
			"target":"English",
			"Dkind":"설명서",
			"field":"화학",
			"tone":"예의",
			"client":"의뢰인2"
		},
		{
			"no":"3",
			"origin":"English",
			"target":"German",
			"Dkind":"기획서",
			"field":"항공",
			"tone":"예의",
			"client":"의뢰인3"
		},
		{
			"no":"4",
			"origin":"Chinese",
			"target":"English",
			"Dkind":"기획서",
			"field":"건축",
			"tone":"장난",
			"client":"의뢰인4"
		},
		{
			"no":"5",
			"origin":"French",
			"target":"English",
			"Dkind":"보고서",
			"field":"식품",
			"tone":"고전",
			"client":"client2"
		},
		{
			"no":"6",
			"origin":"German",
			"target":"French",
			"Dkind":"제안서",
			"field":"의상",
			"tone":"예의",
			"client":"의뢰인5"
		}
	];


	function getCount(){
		var count = completes.length;
		var lang1 = $('#lang1 option:selected').val();
		var lang2 = $('#lang2 option:selected').val();
		var Doc1 = $('#kind_Doc option:selected').val();
		var Maj1 = $('#kind_Maj option:selected').val();
		var Tone1 = $('#kind_tone option:selected').val();
		var c_Name = $('#cName').val().toLowerCase();


		$.each(completes, function(index, item) {
			
			var origin1 = item.origin;
			var target1 = item.target;
			var Dkind1 = item.Dkind;
			var field1 = item.field;
			var tone1 = item.tone;
			var client1 = item.client.toLowerCase();
			var containName = client1.indexOf(c_Name);



			if(lang1 != origin1 && lang1 != ""){
				count--;
				return true;
			}
			else if(lang2 != target1 && lang2 != ""){
				count--;
				return true;
			}
			else if(Doc1 != Dkind1 && Doc1 != ""){
				count--;
				return true;
			}
			else if(Maj1 != field1 && Maj1 != ""){
				count--;
				return true;
			}
			else if(Tone1 != tone1 && Tone1 != ""){
				count--;
				return true;
			}
			else if(containName == -1 && c_Name != ""){
				count--;
				return true;
			}




		// 	// $("#data_rst").append(
		// 	// 	"<p>" + (index+1)+". " +
		// 	// 	"원문: "+ item.origin+" 타겟: " + item.target+" 종류: " + item.Dkind+" 분야: " + item.field+" 어조: " + item.tone+" 고객: " + item.client + 
		// 	// 	"</p>"
		// 	// );

		});
			// console.log(count);
			$('#data_rst').text("위 조건에 해당하는 데이터는"+count+"건 입니다.")


	}


		$('#lang1').change(function(){
			// console.log($(this).val());
			var originT = $("#lang1 option:selected").text();
			$('.kind_box:nth-child(1) span').text(originT);
			
			getCount();
		});
		

		$('#lang2').change(function(){
			var targetT = $("#lang2 option:selected").text();
			$('.kind_box:nth-child(2) span').text(targetT);
			getCount();
		});

		$('#kind_Doc').change(function(){
			var kindT = $("#kind_Doc option:selected").text();
			$('.kind_box:nth-child(3) span').text(kindT);
			getCount();
		});

		$('#kind_Maj').change(function(){
			var kMaj = $("#kind_Maj option:selected").text();
			$('.kind_box:nth-child(4) span').text(kMaj);
			getCount();
		});

		$('#kind_tone').change(function(){
			var kTon = $("#kind_tone option:selected").text();
			$('.kind_box:nth-child(5) span').text(kTon);
			getCount();
		});

		$('#btnSelect').click(function(){
			var c_Name = $('#cName').val();
			$('.kind_box:nth-child(6) span').text(c_Name);
			getCount();
		});



		//Export 버튼

		$("body").on("click","#btnExport",function() { 

			var originLang = $('#lang1 option:selected').val();
			var targetLang = $('#lang2 option:selected').val();
			var Doc1 = $('#kind_Doc option:selected').val();
			var Maj1 = $('#kind_Maj option:selected').val();
			var Tone1 = $('#kind_tone option:selected').val();
			var c_Name = $('#cName').val();

			getCount();
			console.log('원문언어: '+originLang+" / 타겟언어: "+targetLang+" / 종류: "+Doc1+" / 분야: "+Maj1+" / 어조: "+Tone1+" / 고객: "+c_Name);

			// var formData = {
			// 	original_language_id: originLang,
			// 	target_language_id: targetLang,
			// 	subject_id: Maj1,
			// 	format_id: Doc1,
			// 	tone_id: Tone1
			// }

			// $.ajax({
			// 	url : "/api/v2/admin/dataManager/export",
			// 	type : "POST",
			// 	dataType : 'JSON',
			// 	data : formData,
			// 	contentType: "text/csv", 
			// 	// "application/json; charset=utf-8"
			// 	processData: false 
			// 	}).done(function(data){
			// 		console.log(data);
			// 		var url = "data:application/csv;charset=utf-8," + encodeURIComponent(data);
			// 		$("")
			// 	}).fail(function(err){
			// 		console.log(err);
			// 		alert("에러!");

			// });



			//나중에 csv파일 다운로드 코드

			// $.download=function(url, data, method('GET? POST?')){
			// 	//url과 data를 입력받음.
			// 	if(url && data){
			// 		//data는 string 또는 array/object를 파라미터로 받는다.
			// 		data= typeof data == 'string'?data:$.param(data);
			// 		//파라미터를 form의 input으로 받는다.
			// 		var inputs='';
			// 		$.each(data.split('&'), function(){
			// 			var pair = this.split('=');
			// 			inputs+='<input type="hidden" name="'+pair[0]+'" value="'+pair[1]+'"/>';
			// 		});
			// 		//request를 보낸다.
			// 		$('<form action="'+ url +'" method="'+ (method||'post')+'">'+inputs+'</form>')
			// 		.appendTo('body').submit().remove();
			// 	};
			// };
			// 실행예 $.download('textExcelDownload.do','find=commoncode','post');
		});

});
