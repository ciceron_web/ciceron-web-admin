//관리자 권한 애니메이션 이벤트
$("body").on("click", ".admin", function(e) {
    e.stopPropagation();
    var $this = $(this);
    console.log($this)
    if(!$(this).hasClass("activeEvent")) {
        $(".admin_box").slideUp();
        $(".admin").removeClass("activeEvent");
        $this.addClass("activeEvent");
        $this.next().slideDown();
    } else if($(this).hasClass("activeEvent")) {
        $("div.admin").removeClass("activeEvent");
        $(".admin_box").slideUp();
    }
});
//관리자 권한 애니메이션 이벤트
$('body').on("click", function(e) {
    e.stopPropagation();
    if(!$(e.target).hasClass("area")) {
        $(".admin_box").slideUp();
        $(".admin").removeClass("activeEvent");
    }
});
//관리자 권한 애니메이션 이벤트
$("body").on("click", ".admin_box", function(e) {
    e.stopPropagation();
});

//관리자정보
function get_userM_admin(data) {
    var count = 0;
    $.each(data.data, function(index, item) {
        $("table.table_board tbody").append(
            "<tr class='cursor admin_table'>" +
                "<td value='" + item.id + "' class='text_center'><span>" + item.id + "</span></td>" +
                "<td class='info'>" + item.name + "</td>" +
                "<td>" + item.email + "</td>" +
                "<td class='min_size' class='text_center'>" +
                    "<div class='power_contents'>" +
                        "<div class='admin'>관리자</div>" +
                        "<div class='admin_box'>" +
                            "<input type='checkbox' id='powerAll"+item.id+"' name='powerAll'/>" +
                            " <label for='powerAll"+item.id+"'>모든권한</label><br />" +
                            "<input type='checkbox' id='ciceron"+item.id+"' name='power' value='ciceron' />" +
                            " <label for='ciceron"+item.id+"'>CICERON</label><br />" +
                            "<input type='checkbox' id='kangaroo"+item.id+"' name='power' value='kangaroo' />" +
                            " <label for='kangaroo"+item.id+"'>KANGAROO</label><br />" +
                            "<input type='checkbox' id='projectG"+item.id+"' name='power' value='pretranslated' />" +
                            " <label for='projectG"+item.id+"'>공구번역</label><br />" +
                            "<input type='checkbox' id='translation_money"+item.id+"' name='power' value='expense' />" +
                            " <label for='translation_money"+item.id+"'>번역비환급</label><br />" +
                            "<input type='checkbox' id='DBadmin"+item.id+"' name='power' value='sentences' />" +
                            " <label for='DBadmin"+item.id+"'>DB관리</label>" +
                        "</div>" +
                    "</div>" +
                "</td>" +
            "</tr>" +
            "<tr class='details'></tr>"
        );
        $.each(item.adminAccessablePage, function(index2, item2) {
            $(".cursor").eq(index).find("input[value='" + item2.text + "']").prop("checked", true);
            if(item2) count++;
            // console.log(count)
            if(count == 5) {
                $("tr.cursor").eq(index).find("input[name='powerAll']").prop("checked", true);
                count = 0;
            }
        });
    });


    var totalCnt;
    if((data.data.length/20)<0){
        totalCnt = 1;
    } else {
        totalCnt = Math.ceil(data.data.length/20);
    }

    pageNumber("table", 20, ".cursor", totalCnt);
    $("tr.details").hide();
}