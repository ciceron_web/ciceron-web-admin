//의뢰인정보
function get_userM_requester(data) {
    // console.log(data);
    $.each(data.data, function(index, item) {
        $("table.table_board tbody").append(
            "<tr class='cursor'>" +
                "<td value='" + item.id + "' class='text_center'><span>" + item.id + "</span></td>" +
                "<td class='info'>" + item.name + "</td>" +
                "<td>" + item.email + "</td>" +
                // "<td>" + item.keywords + "</td>" +
                "<td>------</td>" +
                "<td  class='text_right'>" + item.numofrequestcompleted + "</td>" +
                // "<td>" + item.recent_work_timestamp + "</td>" +
                "<td class='text_center'>-</td>" +
                "<td class='text_center'>-</td>" +
            "</tr>" +
            "<tr class='details'></tr>"
        );
    });

    var totalCnt;
    if((data.data.length/20)<0){
        totalCnt = 1;
    } else {
        totalCnt = Math.ceil(data.data.length/20);
    }

    pageNumber("table", 20, ".cursor", totalCnt);
    $("tr.details").hide();
}
