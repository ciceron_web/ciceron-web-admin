//유저정보 상세보기
function get_userM_data(data, option) {
    $this = $(option.target);
    var status = "";
    var activity_id = "";
    var photo = photo = "/api/v2/" + data.profile_pic_path;
    if(data.permission_level == 0) status = "의뢰인";
    else if(data.permission_level == 1) status = "번역가";
    else if(data.permission_level == 2) status = "관리자";
    //* is_active가 profile에 있어야 활동설정이 가능. */
    // if(data.is_active == true) activity_id = 0;
    // else activity_id = 1;
    $this.parent(".cursor").next().append(
        "<td colspan='8' value='" + data.id + "'>" +
            "<div id='userImg'>" +
                "<img src='" + photo + "' alt='이미지'/>" +
            "</div>" +
            "<div id='userData'>" +
                "<div>" +
                    "<span class='circle_box'></span>" +
                    "<span class='bold userMail'>" + data.email + "</span>" + 
                    " (" + status + ")" + 
                "</div>" +
                "<div>" +
                    "<span class='circle_box'></span>" +
                    "<span class='bold'>name :</span> " + data.name +
                "</div>" +
                "<div>" +
                    "<span class='circle_box'></span>" +
                    "<span class='bold'>키워드 :</span> " +
                    data.keywords +
                "</div>" +
                "<div>" +
                    "<span class='circle_box'></span>" +
                    "<span class='bold'>총 의뢰 건수 :</span> " + data.numofrequestcompleted +
                    " / <span class='bold'>최근 문서(일자) :</span> " + data.recent_work_timestamp +
                "</div>" +
            "</div>" +
            "<button class='popup_btn'>modify</button>" +
        "</td>"
    );
    modifyView(data.id, photo, data.email, data.name, activity_id, data.is_translator, status, data.translatableLang);
    $("table tr.cursor").next().removeClass("view");
    $("table tr.cursor").next().hide();
    $this.parent().next().addClass("view");
    $this.parent().next().show();
}

//상세보기 번역가 언어설정 함수
function get_userM_translatableLang(langData) {
    $("input[name='lang']").prop("checked", false);
    $.each(langData, function(index, item) {
        $("input[name='lang']").eq(item).prop("checked", true);
    });
}

//상세보기 정보입력 함수
function modifyView(id, profile_pic_path, email, name, activity_id, is_translator, status, translatableLang) {
    $("#modify_pic img").attr("src", profile_pic_path);
    $("#user_mail").attr("value", id);
    $("#user_mail").text(email);
    $("#user_mail").next().text("(" + status + ")");
    if(is_translator) {
        $("#language_box").show();
        get_userM_translatableLang(translatableLang);
    } else {
        $("#language_box").hide();
    }
    $("#user_name").text(name);
    $("select.activity option").eq(activity_id).prop("selected", true);
}