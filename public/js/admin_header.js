$(document).ready(function () {

    /*******************************************
    
     전역변수 : 
        var identifier : salt데이터를 담은 변수

    ********************************************/
    var identifier;
    var adminPage = [];
    /*******************************************
    
        get방식 url 받아오기. (ex - ?pid=2&rid=3에서 값만 받아오는 플러그인)

    ********************************************/
    var QueryString = function () {
        // This function is anonymous, is executed immediately and 
        // the return value is assigned to QueryString!
        var query_string = {};
        var query = window.location.search.substring(1);
        var vars = query.split("&");
        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split("=");
            // If first entry with this name
            if (typeof query_string[pair[0]] === "undefined") {
                query_string[pair[0]] = decodeURIComponent(pair[1]);
                // If second entry with this name
            } else if (typeof query_string[pair[0]] === "string") {
                var arr = [query_string[pair[0]], decodeURIComponent(pair[1])];
                query_string[pair[0]] = arr;
                // If third or later entry with this name
            } else {
                query_string[pair[0]].push(decodeURIComponent(pair[1]));
            }
        }
        return query_string;
    }();

    /*******************************************
    
        로그인 체크

    ********************************************/

    $.ajax({
        url: "/api/v2",
        type: "GET",
        dataType: "JSON",
        contentType: false,
        processData: false
    }).done(function (data) {
        if (data.isAdmin) {
            $("#adminLogout").css("display", "none");
            var user_email = data.useremail;
            $.ajax({
                url: "/api/v2/user/profile",
                type: "GET",
                datType: "JSON",
                contentType: false,
                processData: false
            }).done(function (data) {
                // console.log(data);
                $("#loginInfo #user_e-mail").text(data.email);
                $(".myPic").append(
                    "<img src='/api/access_file/" + data.profile_pic_path + "' alt='이미지' />"
                );
                /*$.each(data, function(index, item) {
                    // console.log(data);
                    if(data.email == user_email) {
                        $("#loginInfo #user_e-mail").text(data.email);
                        $(".myPic").append(
                            // "<img src='/api/access_file/" + item.profile_pic_path + "' alt='이미지' />"
                            "<img src='/api/v2/" + data.profile_pic_path + "' alt='이미지' />"
                        );
                        // $("#myPic img").attr("src", "/api/access_file/" + data.profile_pic_path);
                        $.each(item.adminAccessablePage, function(index2, item2) {
                            console.log(item2.id);
                            adminPage.push(item2.id);
                        });
                    }
                });*/
                /*******************************************
    
                    페이지 들어갈 수 있는 권한 

                ********************************************/
                /*$.each(adminPage, function(index, item) {
                    $("a[value='m" + item + "']").addClass("open");
                });
                $("a[value*=m]:not(.open)").attr("href", "#");
                $("a[value*=m]:not(.open)").attr("disabled", "disabled");*/
            }).fail(function (err) {
                console.log(err);
            });
        }
    }).fail(function (err) {
        $("#adminLogout").css("display", "block");
        $("#adminLogin").css("display", "none");
        console.log(err);
        /*if(location.pathname == "/admin/main") {
            return 0;
        } else {
            location.href = "/admin/main";
        }*/
    });

    $("body").on("click", function (e) {
        // alert("fdsa");
        var $container = $("#adminLogin, .mainPic");

        // if the target of the click isn't the container nor a descendant of the container
        if (!$container.is(e.target) && $container.has(e.target).length === 0) {
            $("#adminLogin").removeClass("showAdmin");
        }
    });

    $("body").on("click", ".myPic", function (e) {
        e.preventDefault();
        var adminBox = $("#adminLogin");
        if (!adminBox.hasClass("showAdmin")) {
            adminBox.addClass("showAdmin");
        } else {
            adminBox.removeClass("showAdmin");
        }
    });

    $("body").on("click", ".fa-times-circle", function (e) {
        e.preventDefault();
        // $("#adminLogin").animate({"top":"150px","opacity":"0"});
        $("#adminLogin").removeClass("showAdmin");
    });

    /*******************************************
    
        로그아웃

    ********************************************/
    $("body").on("click", "#logout", function () {
        $.ajax({
            url: "/api/v2/logout",
            type: "GET",
            dataType: "JSON",
            contentsType: false,
            processData: false
        }).done(function (data) {
            console.log("로그아웃 하셨습니다.");
            $("#adminLogin").css("display", "none");
            $(".myPic").css("display", "none");
            $("#adminLogout").css("display", "block");
            location.href = "/admin/main";
        }).fail(function (err) {
            console.log(err);
        });
    });

    // /*******************************************

    //     자동 로그아웃(5분 후)

    // ********************************************/
    // // Logout Timer 객체 정의
    // var LogOutTimer = function() {
    //     var S = {
    //         timer : null,
    //         limit : 1000 * 60 * 5,
    //         fnc   : function() {},
    //         start : function() {
    //             S.timer = window.setTimeout(S.fnc, S.limit);
    //         },
    //         reset : function() {
    //             window.clearTimeout(S.timer);
    //             S.start();
    //         }
    //     };

    //     document.onmousemove = function() { S.reset(); };
    //     document.keydown = function() { S.reset(); };

    //     return S;
    // }();

    // // 로그아웃 체크시간 설정
    // LogOutTimer.limit = 1000 * 60 * 5;

    // // 로그아웃 함수 설정
    // LogOutTimer.fnc = function() {
    //     $.ajax({
    //         url: "/api/v2/logout",
    //         type: "GET",
    //         dataType: "JSON",
    //         contentsType: false,
    //         processData: false
    //     }).done(function(data) {
    //         console.log("로그아웃 하셨습니다.");
    //         $("#adminLogin").css("display", "none");
    //         $("#adminLogout").css("display", "block");
    //         location.href = "/admin/main";
    //     }).fail(function(err) {
    //         console.log(err);
    //     });
    // }

    // // 로그아웃 타이머 실행
    // LogOutTimer.start();

    /*******************************************
    
        메뉴 클릭시 색상 변경

    ********************************************/
    var location_info = location.pathname.split("/").pop();

    $("*").removeClass("menu_select");
    $(".menu-" + location_info).addClass("menu_select");
    $(".yet").on("click", function (e) {
        e.preventDefault();
        alert("준비 중입니다.");
    });

    $("#menu>ul>li:has(ul)>a").append("<i class='fa fa-caret-down' aria-hidden='true'></i>");
    $("#menu>ul>li>ul>li:has(ul)>a").append("<i class='fa fa-caret-right' aria-hidden='true'></i>");

    /*******************************************
    
        메뉴 클릭시 서브타이틀 생성

    ********************************************/
    function searchAdd(name, create, delId, delClass) {
        $(".menuView").append(
            "<div class='project_search'>" +
            "<span>" + name + " : </span>" +
            "<div class='searchBox'>" +
            "<input type='search' id='search' placeholder='Search' />" +
            "<span id='search_flip'>" +
            "<i class='fa fa-search' aria-hidden='true'></i>" +
            "</span>" +
            "</div>" +
            "<button  id='" + delId + "' class='" + delClass + "'>Delete</button>" +
            "<button class='" + create + "'>Create</button>" +
            "<div>"
        );
    }
    $(".menuNavi span").remove();
    $(".menuView div").remove();
    if (location_info == "commis" || location_info == "userM" || location_info == "requester" || location_info == "translator" || location_info == "admin") {
        location_info = "ciceron";
    }
    switch (location_info) {
        case "main":
            $(".menuNavi").remove(); break;
        case "ciceron":
            $(".menuView").append(
                "<div class='searchBox'>" +
                "<input type='search' id='search' placeholder='Search' />" +
                "<span id='search_flip'>" +
                "<i class='fa fa-search' aria-hidden='true'></i>" +
                "</span>" +
                "</div>"
            );
            break;
        case "projectG":
            $(".menuNavi").append("<span class='navi'>project</span>");
            searchAdd("Project", "popup_btn", "projectG_delete", "project_delete");
            break;
        case "modify":
            $(".menuNavi").append(
                "<span class='navi'>" +
                "<a href='/admin/projectG'>project</a>" +
                "</span>" +
                "<span class='arrow'> > </span>" +
                "<span class='navi'>modify</span>"
            );
            break;
        case "projectG_resource":
            $(".menuNavi").append(
                "<span class='navi'>" +
                "<a href='/admin/projectG'>project</a>" +
                "</span>" +
                "<span class='arrow'> > </span>" +
                "<span class='navi'>resource</span>"
            );
            searchAdd("Resource", "popup_btn", "projectG_resource_delete", "project_delete");
            break;
        case "view":
            $(".menuNavi").append(
                "<span class='navi'>" +
                "<a href='/admin/projectG_resource?pid=" + QueryString.pid + "'>resource</a>" +
                "</span>" +
                "<span class='arrow'> > </span>" +
                "<span class='navi'>view</span>"
            );
            break;
        case "put":
            $(".menuNavi").append(
                "<span class='navi'>" +
                "<a href='/admin/projectG_resource/view?pid=" + QueryString.pid + "&rid=" + QueryString.rid + "'>view</a>" +
                "</span>" +
                "<span class='arrow'> > </span>" +
                "<span class='navi'>put</span>"
            );
            break;
        default:
            $(".menuNavi").remove();
    }
    $.each($(".menu_select"), function (index, item) {
        if (index > 0) {
            $(".menuNavi").append(
                "<span class='navi'> " +
                "<a href='" + $(item).attr("href") + "'>" + $(item).text() + "</a>" +
                " </span>"
            );
            if (($(".menu_select").length - 1) > index) {
                $(".menuNavi").append("<span class='arrow'> > </span>");
            }
        }
    });
    // /*******************************************

    //     메뉴 클릭시 서브타이틀 생성

    // ********************************************/
    // $(".menuNavi span").remove();
    // $(".menuView div").remove();
    // if(location_info == "main") {
    //     $(".menuNavi").remove();
    // } else {
    //     /*$(".menuNavi").append(
    //         "<span class='navi'>" +
    //             "<a href='/admin/main'>home</a>" +
    //         "</span>" +
    //         "<span class='arrow'> > </span>"
    //     );*/
    // }
    // if(location_info == "commis" || location_info == "userM" || location_info == "requester" || location_info == "translator" || location_info == "admin") {
    //     $(".menuView").append(
    //         "<div class='searchBox'>" +
    //             "<input type='search' id='search' placeholder='search'>" +
    //             "<input type='image' src='/img/icon.png' width='20' id='searchFlip'>" +
    //         "<div>"
    //     );
    // }
    // function searchAdd(create, delId, delClass) {
    //     $(".menuView").append(
    //         "<div class='searchBox'>" +
    //             "<input type='search' id='search' placeholder='search' />" +
    //             "<input type='image' src='/img/icon.png' width='20' id='searchFlip' />" +
    //             "<button class='" + create + "'>Create</button>" +
    //             "<button  id=" + delId + " class='" + delClass + "'>Delete</button>" +
    //         "<div>"
    //     );
    // }
    // if(location_info == "projectG") {
    //     searchAdd("popUp", "projectG_delete", "project_delete");
    //     $(".menuNavi").append("<span class='navi'>projectG</span>");
    // }
    // if(location_info == "modify") {
    //     $(".menuNavi").append(
    //         "<span class='navi'>" +
    //             "<a href='/admin/projectG'>projectG</a>" +
    //         "</span>" +
    //         "<span class='arrow'> > </span>" +
    //         "<span class='navi'>modify</span>"
    //     );
    // }
    // if(location_info == "projectG_resource") {
    //     searchAdd("popUp", "projectG_resource_delete", "project_delete");
    //      $(".menuNavi").append(
    //         "<span class='navi'>" +
    //             "<a href='/admin/projectG'>projectG</a>" +
    //         "</span>" +
    //         "<span class='arrow'> > </span>" +
    //         "<span class='navi'>projectG_resource</span>"
    //     );
    // }
    // if(location_info == "view") {
    //      $(".menuNavi").append(
    //         "<span class='navi'>" +
    //             "<a href='/admin/projectG'>projectG</a>" +
    //         "</span>" +
    //         "<span class='arrow'> > </span>" +
    //         "<span class='navi'>" +
    //             "<a href='/admin/projectG_resource?pid=" + QueryString.pid + "'>projectG_resource</a>" +
    //         "</span>" +
    //         "<span class='arrow'> > </span>" +
    //         "<span class='navi'>view</span>"
    //     );
    // }
    // if(location_info == "put") {
    //      $(".menuNavi").append(
    //         "<span class='navi'>" +
    //             "<a href='/admin/projectG'>projectG</a>" +
    //         "</span>" +
    //         "<span class='arrow'> > </span>" +
    //         "<span class='navi'>" +
    //             "<a href='/admin/projectG_resource?pid=" + QueryString.pid + "'>projectG_resource</a>" +
    //         "</span>" +
    //         "<span class='arrow'> > </span>" +
    //         "<span class='navi'>" +
    //             "<a href='/admin/projectG_resource/view?pid=" + QueryString.pid + "&rid=" + QueryString.rid + "'>view</a>" +
    //         "</span>" +
    //         "<span class='arrow'> > </span>" +
    //         "<span class='navi'>put</span>"
    //     );
    // }
    // $.each($(".menu_select"), function(index, item){
    //     if(index > 0) {
    //         $(".menuNavi").append(
    //             "<span class='navi'> " + 
    //                 "<a href='" + $(item).attr("href") + "'>" + $(item).text() + "</a>" + 
    //             " </span>"
    //         );
    //         /*if(($(".menu_select").length - 1) == index) {
    //             $(".subMenuView").text($(item).text());
    //         }*/
    //         if(($(".menu_select").length - 1) > index) {
    //             $(".menuNavi").append("<span class='arrow'> > </span>");
    //         }
    //     }
    // });

});
var loading = function (isStart) {
    if (isStart) {
        $("body").append("<div class='loading'></div>")
    }
    else {
        $(".loading").remove();
    }
}