$(document).ready(function () {

    /*******************************************
     
        tagData
        * tag/status/0 : 분류되지 않은 태그 리스팅
        * 태그 li에 넣기.
        
    ********************************************/
    var $tagV = $("#tagView");
    var page = 1;
    $("#loading").hide();
    $("#loading2").hide();
    loadTag(page);

    $tagV.scroll(function () {
        $("#loading").show();
        if ($(this).scrollTop() >= $(this).prop("scrollHeight") - $(this).height() - 100) {
            loadTag(page);
        }
    });

    var isLoading = false;
    // !A -> A != true
    // !A -> A == false
    //  A -> A == true

    // tag 로딩
    function loadTag(pageNumber) {
        if (!isLoading) {
            isLoading = true;
            $.ajax({
                url: "/api/v2/admin/kangaroo/tag/status/0",
                type: "get",
                dataType: "json",
                contentType: false,
                processData: false,
                data: "page=" + page + "&itemPerPage=8"
            }).done(function (data) {
                $.each(data.data, function (index, item) {
                    // console.log(item);
                    $("#tagView ul").append(
                        "<li>" + "<input type='checkbox' name='tagCheck'/><span value='" + item.id + "'>" + item.name + "</span></li>"
                    );
                });
                // 선택된 태그가 없을 때 첫번째 태그 클릭
                if ($(".selected").length <= 0) {
                    $("#tagView ul li").first().find("span").click();
                }
                // setTimeout(function () {
                //     loadTag();
                // }, 10000);
                isLoading = false;
                page++;

            }).fail(function (err) {
                console.log(err);
                isLoading = false;
            });
        }
        return false;
    }


    /*******************************************
     
        tagView

    ********************************************/

    $("body").on("click", "#tagView ul li span", function () {
        $("#loading2").show();
        $("#tagName").text($(this).text());
        $("#tagView ul li span").removeClass("selected");
        $(this).addClass("selected");
        $(".selectL").addClass("is-open");
        $(".selectL").find('.placeholder').text('- 대분류 -');
        $(".selectS").find('.placeholder').text('- 소분류 -');

        var tag_check = $("input[type='checkbox']");
        tag_check.change(function () {
            var $this = $(this), $sideSpan = $(this).siblings("span");
            if ($this.is(":checked")) {
                $sideSpan.addClass("checked");
            } else {
                $sideSpan.removeClass("checked");
            }
            $("#loading2").hide();
        }).change();
    });


    /*******************************************
     
        tag 등록시키기

    ********************************************/
    $("body").on("click", function (e) {

        var $container = $(".select, #tagView li, header h1");

        // if the target of the click isn't the container nor a descendant of the container
        if (!$container.is(e.target) && $container.has(e.target).length === 0) {
            $(".select").removeClass("is-open");
        }
    });

    $("body").on("click", ".select .placeDiv", function () {
        $(".select").removeClass("is-open");
        $(this).closest(".select").addClass("is-open");
    });
    $("body").on('click', '.select ul>li', function () {
        var parent = $(this).closest('.select');
        parent.removeClass('is-open').find('.placeholder').text($(this).text());
        parent.find('input[type=hidden]').attr('value', $(this).attr('value'));
    });

    // tag submit button
    $("body").on("click", "#tagData_submit", function () {
        var tag_id = $("#tagView").find("span.selected").attr("value");
        var category_level_1 = $("input[name='changeL']").val();
        var category_level_2 = $("input[name='changeS']").val();

        var formData = new FormData();
        formData.append("tag_id", tag_id);
        formData.append("category_level_1", category_level_1);
        formData.append("category_level_2", category_level_2);

        $.ajax({
            url: "/api/v2/admin/kangaroo/tag",
            type: "post",
            dataType: "json",
            contentType: false,
            processData: false,
            data: formData
        }).done(function (data) {
            console.log("추가시킵니다.");
            location.reload();
        }).fail(function (err) {
            console.log(err);
        });
    });

    /*******************************************
     
        tag_delete

    ********************************************/
    $("body").on("click", "#tag_delete", function () {
        var checked_tag = $("input[type='checkbox']:checked");

        $.each(checked_tag, function (index, item) {
            var tag_id = $(item).siblings("span").attr("value");
            $.ajax({
                url: "/api/v2/admin/kangaroo/tag/" + tag_id,
                type: "delete",
                dataType: "json",
                contentType: false,
                processData: false
            }).done(function (data) {
                console.log("삭제하였습니다.");
                checked_tag.parent().remove();
                $("#tagView ul li").eq(0).find("span").click();
            }).fail(function (err) {
                console.log(err);
            });
        });
    });
});