define(['jquery', 'eventemitter', 'app/RequestService', 'app/AuthService', 'app/CompletedGroupItemTicketView', "jquery-ui/jquery-ui"], function($, EventEmitter, requestService, authService, completedGroupItemTicketView) {

	var requestService = requestService;
	
	function CompletedRequestListController($elContainer) {
		EventEmitter.call(this);
				
		this.$elContainer = $elContainer;
		this._htData = {
			groups : [],
			groupItems : {
				
			}
		};

		this.aoTicket = [];
		
		this.initEvents();
		
		this._fetchGroups();
		//this.buildUpElements();
	}

	EventEmitter.inherits(CompletedRequestListController, EventEmitter);

	
	CompletedRequestListController.prototype.initEvents = function() {
		this.$elContainer.find(".groupContainer").on("click", ".modifyStartBtn", $.proxy(function(self,evt) {
			evt.stopPropagation();
			$(this).hide();
			self.$elContainer.find(".modifyEndBtn").show();
			self.$elContainer.find(".overlay").show();
		}, undefined, this));
		
		this.$elContainer.find(".groupContainer").on("click", ".modifyEndBtn", $.proxy(function(self,evt) {
			evt.stopPropagation();
			$(this).hide();
			self.$elContainer.find(".modifyStartBtn").show();
			self.$elContainer.find(".overlay").hide();
		}, undefined, this));


		// 모든 이벤트는 delegation 으로 세팅한다. 
		this.$elContainer.find(".groupContainer").on("click", ".groupItem", $.proxy(function(self) {
			// draggable 을 모두 destroy 한다.
			self._destroyDraggable();
			
			self.$elContainer.find(".groupContainer .groupItem").removeClass("selected");
			$(this).addClass("selected");
			self._fetchGroupItems(parseInt($(this).attr("data-group-id")));
		}, undefined, this));
		
		// 그룹 추가하기 
		this.$elContainer.find(".addGroupBtn").on("click", function() {
			var sGroupName = prompt("생성할 그룹의 이름을 적어주세요");
			if(sGroupName && sGroupName.length > 0) {			
				requestService.addCompletedRequestGroup({
					group_name : sGroupName,
					success: function() {
						location.reload();
					}
				});
			}
		});
		
		this.$elContainer.find(".groupContainer").on("click", ".groupItem .overlay .rename", $.proxy(function(self, evt) {
			evt.stopPropagation();
			var self = self;
			var oldname = $(this).closest(".groupItem").find(".groupName").html();
			var newname = window.prompt("새로운 이름을 입력해 주세요", oldname);
			
			if(newname === "Documents") {
				alert("Documents 라는 이름은 사용할 수 없습니다");
				return;
			}
			
			if(newname === oldname) {
				return;
			}	
				
			newname = newname.trim();
				
			if(newname.length <= 0 ) {
				alert("공백이나, 길이가 0 인 문자열은 사용할 수 없습니다.");
				return;
			}		
				
			requestService.renameCompletedRequestsGroup({
				group_id: $(this).closest(".groupItem").attr("data-group-id"),
				group_name: newname ,
				success: function(){
					$(this).closest(".groupItem").find(".groupName").html(newname);
				}.bind(this)
			});
		}, undefined, this));

		this.$elContainer.find(".groupContainer").on("click", ".groupItem .overlay .delete", $.proxy(function(self, evt) {
			evt.stopPropagation();
			var self = self;
			var oldname = $(this).closest(".groupItem").find(".groupName").html();
				
			var bConfirm = window.confirm(oldname + "폴더를 정말 삭제하시겠습니까?");
			
			if(bConfirm) {
				requestService.deleteCompletedRequestsGroup({
					group_id: $(this).closest(".groupItem").attr("data-group-id"),
					success: function(){
						location.reload();
					}.bind(this)
				});
			}
		}, undefined, this));
	};	
	
	CompletedRequestListController.prototype._fetchGroups = function() {
		requestService.getCompletedRequestsGroup({
			success: function(data) {
				this._htData.groups = data.data.reverse();
				this.renderGroups();
				
				// 첫번째 그룹을 클릭 시켜서 (selected 로 만들고 group item 을 로팅해서 표시하도록 한다.);
				$(this.$elContainer.find(".groupContainer .groupItem")[0]).click();
				
				this._initDroppable();
			}.bind(this)
		});
	};
	
	CompletedRequestListController.prototype._destroyDraggable = function() {

	};
	// 
	CompletedRequestListController.prototype._initDraggable = function() {		
		var nStartOffsetLeft = 0;
		
	    this.$elContainer.find( ".donerequestsContainer .card" ).draggable({
		    handle : ".header",
		    helper : function(event) {			    			
			    return $(event.currentTarget).clone().css({
				    "transform": "scale(.2)",
				    "opacity": ".6"
				});;
		    },
		    start : function(event, ui) {
			    $(event.target).css("opacity", "0.6");
				nStartOffsetLeft = 270 - event.offsetX;
		    },
		    drag : function(event, ui) {
			  	  ui.position.top = ui.position.top - 80;
			  	  ui.position.left = ui.position.left - nStartOffsetLeft;
		    },
		    stop : function(event, ui) {
			    $(event.target).css("opacity", "1");			    
		    }
	    });
	};	
	CompletedRequestListController.prototype._initDroppable = function() {
	    this.$elContainer.find( ".groupContainer > .groupItem" ).droppable({
			tolerance: "pointer",
			over: function( event, ui ) {
			    if(!$(event.target).is(".selected")) {
				    $(event.target).css("border", "2px solid white");
				}
			},
			out: function( event, ui ) {
			    $(event.target).css("border", "none");
			},
			drop: function( event, ui ) {
				var $elDraggable = ui.draggable;
			    this.$elContainer.find( ".groupContainer > .groupItem" ).css("border", "none");
			    
			    var sCurrentGroupId = $(".groupItem.selected")[0].getAttribute("data-group-id");
			    var sTargetGroupId = event.target.getAttribute("data-group-id");
			    
			    if(sCurrentGroupId === sTargetGroupId) {
				    return;
			    }
			    
			    requestService.moveCompleteRequestToGroup({
				    group_id: parseInt(event.target.getAttribute("data-group-id")),
				    request_id:  parseInt($elDraggable[0].getAttribute("data-request-id")),
				    success: function() {
					   $elDraggable.draggable( "destroy" ).remove();
				    }
			    });
			}.bind(this)
	    });
	};
	
	CompletedRequestListController.prototype._fetchGroupItems = function(nGroupId) {
		requestService.getCompletedRequestsGroupItems({
			group_id: nGroupId,
			success: function(data) {
				this.$elContainer.find(".donerequestsContainer").html("");
				this._htData.groupItems[nGroupId] = data.data;
				this.renderGroupItems(nGroupId);
				
				setTimeout(function(){
					this._initDraggable();
				}.bind(this), 1000);
			}.bind(this)
		});
	};
	

	CompletedRequestListController.prototype.renderGroupItems = function(nGroupId) {
		var aGroupItems = this._htData.groupItems[nGroupId];
		var $groupItemContainer = this.$elContainer.find(".donerequestsContainer");

		if(aGroupItems.length === 0) {
			this.$elContainer.find(".donerequestsContainer").html("<h2>이 그룹에 완료된 번역이 없습니다.</h2>")
		} else {
			for(var i in aGroupItems) {
				var sCompletedDate = aGroupItems[i].request_submittedTime;
				var oDate = new Date(sCompletedDate.replace(/-/g,'/')+ " UTC");
				console.log(oDate.toString(),sCompletedDate)
				sCompletedDate = [oDate.getFullYear(),oDate.getMonth()+1,oDate.getDate()].join("-") + " " +
				[oDate.getHours(),oDate.getMinutes(),oDate.getSeconds()].join(":");
		
				var oTicket = new completedGroupItemTicketView($groupItemContainer, {
					isAppending: true,
					rawData: aGroupItems[i],
					hasTranslateNow: true, // Todo authService 를 통해 결정 
					data:{
						requestId: aGroupItems[i].request_id,
						completedDate: sCompletedDate,
						originalLangIndex: aGroupItems[i].request_originalLang,
						targetLangIndex: aGroupItems[i].request_targetLang,	
						formatIndex: aGroupItems[i].request_format,
						subjectIndex: aGroupItems[i].request_subject,										
						translatorName:	aGroupItems[i].request_translatorName,
						words: aGroupItems[i].request_words,
						title: (aGroupItems[i].request_title) ? aGroupItems[i].request_title : "untitled",
						translatedText: aGroupItems[i].request_translatedText,
						messages: {
							originalLang: this._messageMapperForLanguage(aGroupItems[i].request_originalLang, 0),
							targetLang: this._messageMapperForLanguage(aGroupItems[i].request_targetLang, 0),
							format: this._messageMapperForFormat(aGroupItems[i].request_format, 0),
							subject: this._messageMapperForSubject(aGroupItems[i].request_subject, 0),
						}
					},
					messages: {
						words: "단어",
						open: "확인"
					}
				});
				
				oTicket.on("clickDetail", function(htEvent) {
					this.emit("clickDetail", htEvent);
				}.bind(this));
					
				this.aoTicket.push(oTicket);			
			}
		}	
	};
			
	CompletedRequestListController.prototype.renderGroups = function() {
		var aGroups = this._htData.groups;
		var $groupContainer = this.$elContainer.find(".groupContainer");
		aGroups.reverse();

		for(var i in aGroups) {
			var OverLay = aGroups[i].name !== "Documents"? "<div class='overlay'><button class='rename'></button><button class='delete'></button></div>": "";
			
			$groupContainer.prepend("<div class='groupItem' data-group-id='"+aGroups[i].id+"'><h2 class='groupName'>"+aGroups[i].name+"</h2>"+
			OverLay + "</div>");
		}	
	};
	
	CompletedRequestListController.prototype._getRemainingHoursFromNow = function(sTargetDatetime) {
		var diff = Math.abs(new Date() - new Date(sTargetDatetime.replace(/-/g,'/')+ " GMT"));
		var sec = diff / 1000;
		var min = sec / 60;
		var hour = min / 60;
		return parseInt(hour);
	};	
	
	CompletedRequestListController.prototype.getRemainingTimeExpressionFromNow = function(sTargetDatetime, i18nIndex) {
		var label = "";
		
		if(i18nIndex === 0) {
			label = "시간"
		}
		return this._getRemainingHoursFromNow(sTargetDatetime) + " " + label;
	};
		
	CompletedRequestListController.prototype._messageMapperForLanguage = function(nLangIndex, nI18nIndex) {
		var mapper = [[
			"한국어",
			"미국영어",
			"영국영어",
			"북경어",
			"광동어"
		], [
			"Korean",
			"English (U.S.)",
			"English (U.K.)",
			"Mandarin",
			"Cantonese"
		]];
		return mapper[nI18nIndex][nLangIndex];
	};
	CompletedRequestListController.prototype._messageMapperForFormat = function(nLangIndex, nI18nIndex) {
		var mapper = [[
			"전자우편",
			"편지",
			"에세이",
			"광고",
			"초대장",
			"안부",
			"출판물",
			"대본",
			"리서치",
			"레포트",
			"메뉴",
			"지원서",
			"설명",
			"SNS",
			"기타"
		]];
		return mapper[nI18nIndex][nLangIndex];
	};


	CompletedRequestListController.prototype._messageMapperForSubject = function(nLangIndex, nI18nIndex) {
		var mapper = [[
			"예술",
			"스포츠",
			"과학",
			"건강",
			"경제",
			"음악",
			"문학",
			"정치",
			"비즈니스",
			"종교",
			"쇼핑",
			"패션"
		]];
		return mapper[nI18nIndex][nLangIndex];
	};	
	return CompletedRequestListController;
});