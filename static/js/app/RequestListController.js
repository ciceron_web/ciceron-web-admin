define(['jquery', 'eventemitter', 'app/RequestService', 'app/AuthService', 'app/QuickRequestTicketView', 'app/RequestTicketView'], function($, EventEmitter, requestService, authService, quickRequestTicketView, requestTicketView) {

	function RequestListController($elContainer) {
				
		this.$elContainer = $elContainer;
		this._htData;
		this._lastScrollTime = new Date();
		
		this._fetchRequestList();
		
		// 스크롤이 마지막까지 내려가면 _fetchRequestList 를 호출해서 글을 더 불러올 수 있다..
		// TODO: 근데 모바일도 그렇고 해서 끝까지 스크롤 된 경우를 판정하는 적절한 로직을 구현해야함 
		window.onscroll = function(ev) {
			
			var body = document.body,
			    html = document.documentElement;
			
			var height = Math.max( body.scrollHeight, body.offsetHeight, 
			                       html.clientHeight, html.scrollHeight, html.offsetHeight );
			                       
			if (($(window).innerHeight() + $(window).scrollTop() - 56) >= height) {
				var newTime = new Date();
				newTime.setSeconds(newTime.getSeconds() - 2);
				if (newTime > this._lastScrollTime){
					this._fetchRequestList();
				}
			}
		}.bind(this);
		
	}
	
	RequestListController.prototype._fetchRequestList = function() {
		requestService.getRequests({
			success: function(data) {
				this._htData = data.data;
				this.render();

			}.bind(this)
		});
	};
	
	// _htData 를 이용해 $elContainer 속에 티켓들을 렌더링한다.
	RequestListController.prototype.render = function(ahtData) {
		var ahtData = ahtData || this._htData;
		
		for(var i in this._htData) {
			if(this._htData[i].request_isSos === true) {
				new quickRequestTicketView(this.$elContainer, {
					isAppending: true,
					rawData: this._htData[i],
					hasTranslateNow: true, // Todo authService 를 통해 결정 
					data:{
						clientName: this._htData[i].request_clientName,
						originalText: this._htData[i].request_text,
						clientPicPath:	this._htData[i].request_clientPicPath,
						originalLangIndex: this._htData[i].request_originalLang,
						targetLangIndex: this._htData[i].request_targetLang,						
						translatorPicPath:	this._htData[i].request_translatorPicPath,
						translatorName:	this._htData[i].request_translatorName,
						request_id:	this._htData[i].request_id
						
					}
				});				
			} else {
				new requestTicketView(this.$elContainer, {
					isAppending: true,
					isConfirmTicket: false,
					hasTranslateNow: true, // Todo authService 를 통해 액션을
					hasAddToCart: true,
					rawData: this._htData[i],
					data:{
						clientName: this._htData[i].request_clientName,
						request_points: this._htData[i].request_points, // 금액
						remainingHours: this._getRemainingHoursFromNow(this._htData[i].request_dueTime),
						words: this._htData[i].request_words,						
						formatIndex: this._htData[i].request_format,
						subjectIndex: this._htData[i].request_subject,
						originalText: this._htData[i].request_text,
						contextText: this._htData[i].request_context,
						clientPicPath:	this._htData[i].request_clientPicPath,
						originalLangIndex: this._htData[i].request_originalLang,
						targetLangIndex: this._htData[i].request_targetLang,					
						translatorPicPath: this._htData[i].request_translatorPicPath,
						translatorName:	this._htData[i].request_translatorName,
						translatorsInQueue: this._htData[i].request_translatorsInQueue,
						request_id:	this._htData[i].request_id
					}
				});					
			}
		}
	};
	
	RequestListController.prototype._getRemainingHoursFromNow = function(sTargetDatetime) {
		var diff = Math.abs(new Date() - new Date(sTargetDatetime));
		var sec = diff / 1000;
		var min = sec / 60;
		var hour = min / 60;
		return parseFloat(hour);
	};

	return RequestListController;
});