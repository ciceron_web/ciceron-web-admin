define(['jquery', 'eventemitter', 'app/RequestService', 'app/AuthService', 'app/QuickRequestTicketView', 'app/RequestTicketView', 'app/I18nResolver'], function($, EventEmitter, requestService, authService, quickRequestTicketView, requestTicketView, i18nResolver) {

	function ProcessingRequestListController($elContainer) {
				
		this.$elContainer = $elContainer;
		this._htData;
		
		this._fetchRequestList();
		//this.buildUpElements();
		//this.initEvents();
	}
	
	ProcessingRequestListController.prototype._fetchRequestList = function() {
		requestService.getProcessingRequests({
			success: function(data) {
				console.log("getProcessingRequests", data);
				var nResultCount = 0;
				nResultCount += data.pending.length;
				nResultCount += data.ongoing.length;
				nResultCount += data.complete.length;		
				nResultCount += data.incomplete.length;		

				if(nResultCount === 0) {
					this.$elContainer.html('<h1>'+i18nResolver.getMap()["PENDING_EMPTY"]+'</h1>');
				} else {
					this._htData = data;
					this.render();					
				}

			}.bind(this)
		});
	};
	
	ProcessingRequestListController.prototype.requestTickViewFactory = function(htTicket, sStep) {
		
		
		// DATA 초기화 
		var htPayload = {
			data: {
				clientName: htTicket.request_clientName,
				originalText: htTicket.request_text,
				clientPicPath:	htTicket.request_clientPicPath || "profile_pic/2015-07-22_1.54.47.png",
				originalLangIndex: htTicket.request_originalLang,
				targetLangIndex: htTicket.request_targetLang,
				translatorPicPath:	htTicket.request_translatorPicPath || "profile_pic/2015-07-22_1.54.47.png",
				translatorName:	htTicket.request_translatorName,
				isExpired: htTicket.request_status == -1,
				// 여기부턴 일반의뢰 DATA 
				request_points: htTicket.request_points, // 금액
				remainingHours: htTicket.request_dueTime ? this._getRemainingHoursFromNow(htTicket.request_dueTime) : undefined, // "2 시간", // TODO 남은 시간을 효과적으로 표시하는 함수를 i18n 지원하도록 만들어야함
				words: htTicket.request_words,					
				formatIndex: htTicket.request_format,
				subjectIndex: htTicket.request_subject,										
				contextText: htTicket.request_context,
				translatorsInQueue: htTicket.request_translatorsInQueue
			}
		};
		
		
		if(htTicket.request_isSos === true) {
			new quickRequestTicketView(this.$elContainer, {
				isAppending: true,
				hasCancelRequest: (sStep === "pending") ? true : false,
				hasCart: false, 
				rawData: htTicket,
				isExpired: htPayload.data.isExpired,
				hasTranslateNow: false, // Todo authService 를 통해 결정 
				hasCompleteRequest: (htTicket.request_submittedTime) ? true : false,
				data: htPayload.data,
				messages: htPayload.messages,
				hasTranslatingLabel: htTicket.request_status === 1 ? true : false,
				resubmitSuccess: function(data, ticket) {
					//ticket.$elItem.remove();
					location.href="/";
				},
				deleteSuccess: function(data, ticket) {
					ticket.$elItem.remove();
				}
			});				
		} else {
			new requestTicketView(this.$elContainer, {
				isAppending: true,
				isConfirmTicket: false,
				hasCart: false,
				canToggleOriginalText: true,
				isExpired: htPayload.data.isExpired,
				hasCancelRequest: (sStep === "pending" || sStep==="incomplete") ? true : false,
				hasTranslateNow: false, // Todo authService 를 통해 결정 
				hasCompleteRequest: (htTicket.request_submittedTime) ? true : false,
				rawData: htTicket,
				data: htPayload.data,
				messages: htPayload.messages,
				hasTranslatingLabel: htTicket.request_status === 1 ? true : false,
				resubmitSuccess: function(data, ticket) {
					location.href="/";
//					ticket.$elItem.remove();
				},
				deleteSuccess: function(data, ticket) {
					ticket.$elItem.remove();
				}
			});					
		}

	};

	// _htData 를 이용해 $elContainer 속에 티켓들을 렌더링한다.
	ProcessingRequestListController.prototype.render = function() {
		var aPending = this._htData.pending;
		var aOngoing = this._htData.ongoing;
		var aComplete = this._htData.complete;
		var aIncomplete = this._htData.incomplete;
		
		aPending.forEach(function(htRequest) {
			this.requestTickViewFactory(htRequest, "pending");
		}.bind(this));

		aOngoing.forEach(function(htRequest) {
			this.requestTickViewFactory(htRequest, "ongoing");
		}.bind(this));

		aComplete.forEach(function(htRequest) {
			this.requestTickViewFactory(htRequest, "complete");
		}.bind(this));
		
		aIncomplete.forEach(function(htRequest) {
			this.requestTickViewFactory(htRequest, "incomplete");
		}.bind(this));
				
	};
	
	ProcessingRequestListController.prototype._getRemainingHoursFromNow = function(sTargetDatetime) {
		if(sTargetDatetime) {
			var diff = Math.abs(new Date() - new Date(sTargetDatetime.replace(/-/g,'/')+ " GMT"));
		} else {
			var diff = Math.abs(new Date() - new Date());
		}
		var sec = diff / 1000;
		var min = sec / 60;
		var hour = min / 60;
		return parseFloat(hour);
	};	
	
	return ProcessingRequestListController;
});