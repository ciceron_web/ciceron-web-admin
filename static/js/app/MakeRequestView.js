define(['jquery', 'eventemitter', 'app/RequestService', 'app/AuthService', 'app/TemplateMap', "app/RequestTicketView", "jquery-ui/datepicker", "jquery-ui/spinner", "jquery-ui/selectmenu"], function($, EventEmitter, requestService, authService, templateMap, requestTicketView) {
	var DEFAULT_MIN_PRICE = 10.00;
	
	function MakeRequestView($elContainer) {				
		this.$elContainer = $elContainer;
		
		this.nSectionIndex = -1;
		
		this.htData = {
			reward : DEFAULT_MIN_PRICE
		};

		this._htOption = {};
		// 데이터로 revenue 넣어야함
		this._htOption.data = {};
		this._htOption.data.revenue = authService.state.htUserProfile.user_revenue.toFixed(2);
		
		this.loadTemplate({
			callback : function() {			
				this.initAfterLoaded();
			}.bind(this)
		});		
	}
	
	MakeRequestView.prototype.loadTemplate = function(htParam) {
		templateMap.get("MakeRequestView", function(sTemplate, i18n) {
			this._htOption.i18n = i18n;
			this._template = Handlebars.compile(sTemplate);
			htParam.callback();		
		}.bind(this));
	};
	
	MakeRequestView.prototype.initAfterLoaded = function() {
		this.buildUpElements();
		this.initEvents();	
	};
		
	MakeRequestView.prototype.buildUpElements = function() {
		this.$elContainer.append(this._template(this._htOption));

		this.$elStartBtn = this.$elContainer.find(".startBtn");
		this.$elMakerequestLanding = this.$elContainer.find(".makerequestLanding");
		this.$elRequestFormContainer = this.$elContainer.find(".requestFormContainer");
		this.$elSectionContainer = this.$elContainer.find(".sectionContainer");
		this.$elProgress = this.$elContainer.find(".progress");
		this.$elFormatContainer = this.$elContainer.find(".format-container");
		this.$elSubjectContainer = this.$elContainer.find(".subject-container");
		this.$elMediaKindContainer = this.$elContainer.find(".mediakind-container");
		
		this.$elPaymentContainer = this.$elContainer.find(".payment-container");
		
		
		this.$elOriginalLang = this.$elContainer.find("#original_language_select");
		this.$elTargetLang = this.$elContainer.find("#target_language_select");
		this.$elLangSwitch = this.$elContainer.find(".js-lang-switch");
	
		this.$elOriginalTextToggleBtn = this.$elContainer.find(".toggleBtn");
	
		
/*
		
		
		
		
		this.$elSubmit = this.$elContainer.find("button");
		this.$elOriginalLang = this.$elContainer.find("#original_language_select");
		this.$elTargetLang = this.$elContainer.find("#target_language_select");
		this.$elLangSwitch = this.$elContainer.find(".js-lang-switch");
*/

		// 언어선택 메뉴 초기화 
		$.widget( "ui.selectmenu", $.ui.selectmenu, {
			_renderItem: function( ul, item ) {
				var li = $( "<li>" )
					.addClass("icon icon-"+item.element.attr("data-class"))
					.append("<h1>11</h1>");
					
				if ( item.disabled ) {
					li.addClass( "ui-state-disabled" );
				}
				
				this._setText( li, item.label);
				
				return li.appendTo( ul );
			}
		});
		
		this.$elContainer.find( "#original_language_select" ).selectmenu({  
		  width: 257,
		  icons: { button: "ui-icon-blank" }
		});
		this.$elContainer.find( "#target_language_select" ).selectmenu({  
		  width: 256,
		  icons: { button: "ui-icon-blank" }
		});
		


	};
	
	MakeRequestView.prototype.goNext = function() {
		if(this.nSectionIndex > 5) return;
		if(this.nSectionIndex === 5) {
		
		} else {
			this.nSectionIndex++;
			this.$elSectionContainer.find("section").removeClass("active");
			this.$elSectionContainer.find("section").eq(this.nSectionIndex).addClass("active");
			setTimeout(function() {
				this.$elProgress.removeClass("val" + (this.nSectionIndex));
				this.$elProgress.addClass("val" + (this.nSectionIndex + 1));
			}.bind(this), 0);			
		}
	};
	MakeRequestView.prototype.goPrev = function() {
		if(this.nSectionIndex <= 0) return;
		this.nSectionIndex--;
		this.$elSectionContainer.find("section").removeClass("active");
		this.$elSectionContainer.find("section").eq(this.nSectionIndex).addClass("active");
		setTimeout(function() {
			this.$elProgress.removeClass("val" + (this.nSectionIndex + 2));
			this.$elProgress.addClass("val" + (this.nSectionIndex + 1));
		}.bind(this), 0);	
	}	
	
	MakeRequestView.prototype.renderConfirmSubmitSection = function() {		
		new requestTicketView(this.$elContainer.find(".confirmTicketContainer"), {
			isAppending: false,
			isConfirmTicket: true,
			hasTranslateNow: false,
			data:{
				clientName: authService.state.htUserProfile.user_name,
				request_points: parseFloat(this.htData.reward), // 금액
				remainingHours: this._getRemainingHoursFromNow(this.htData.duedate + " "+ this.htData.duehour+":"+ this.htData.dueminute+":00"),
				words: this.htData.requestText.length,
				
				formatIndex: this.htData.format,
				subjectIndex: this.htData.subject,
									
				originalText: this.htData.requestText,
				contextText: this.htData.contextText,
				clientPicPath:	authService.state.htUserProfile.user_profilePicPath,
				originalLangIndex: parseInt(this.htData.originalLang),
				targetLangIndex: parseInt(this.htData.targetLang),
				translatorsInQueue: []
			}
		});		
		
		setTimeout(function(){
			this.$elContainer.find(".confirmTicketContainer").addClass("ready");	
		}.bind(this), 100);
				
		this.$elContainer.find(".originalText").html(this.htData.requestText.replace(/\n/g, "<br/>"));
	};
	
	MakeRequestView.prototype._getRemainingHoursFromNow = function(sTargetDatetime) {
		var diff = Math.abs(new Date() - new Date(sTargetDatetime));
		var sec = diff / 1000;
		var min = sec / 60;
		var hour = min / 60;
		return parseInt(hour);
	};	
	
	MakeRequestView.prototype.initEvents = function() {
		this.$elStartBtn.on("click", function() {
			// 입력 폼으로 이동
			this.$elMakerequestLanding.removeClass("active");
			this.$elRequestFormContainer.addClass("active");
			
			// 0 페이지로 이동
			this.goNext();
		}.bind(this));

		this.$elContainer.find(".submitBtn").on("click", function() {
			// 완료 폼으로 이동
			this.$elRequestFormContainer.removeClass("active");
			this.$elContainer.find(".makerequestConfirm").addClass("active");

			this.renderConfirmSubmitSection();
		}.bind(this));		
				
		
		
		this.$elContainer.find(".restartBtn").on("click", function(evt) {
			location.reload();
		}.bind(this));
		
		this.$elContainer.find(".nextBtn").on("click", function(evt) {
			this.goNext();
		}.bind(this));
		this.$elContainer.find(".previousBtn").on("click", function(evt) {
			this.goPrev();
		}.bind(this));
		
		// 의뢰물 형식 선택 섹션 
		this.$elFormatContainer.on("click", $.proxy(function(self){
			self.htData.format = $(this).data("format-index");
			self.$elFormatContainer.removeClass("selected");
			$(this).addClass("selected");
			if(self.htData.format !== null) {
				$(this).closest(".sectionContainer").find("section").eq(0).find(".nextBtn").removeClass("noshow");
			}
		}, undefined, this));
		
		// 의뢰물 분야 선택 섹션 
		this.$elSubjectContainer.on("click", $.proxy(function(self){
			self.htData.subject = $(this).data("subject-index");
			self.$elSubjectContainer.removeClass("selected");
			$(this).addClass("selected");
			
			if(self.htData.subject !== null) {
				$(this).closest(".sectionContainer").find("section").eq(1).find(".nextBtn").removeClass("noshow");
			}
		}, undefined, this));
		
		
		this.htData.mediakind = 0;
		$(this.$elMediaKindContainer[0]).addClass("selected");
		
		// 의뢰물 미디어
		this.$elMediaKindContainer.on("click", $.proxy(function(self, evt){
			var nMediaKindIndex = parseInt($(this).data("mediakind-index"));
			if(nMediaKindIndex !== 0) {
				alert("현재는 텍스트만 가능합니다");
				return;
			}
			
			self.htData.mediakind = nMediaKindIndex;
			self.$elMediaKindContainer.removeClass("selected");
			$(this).addClass("selected");
			
			if(self.htData.mediakind !== null) {
				$(this).closest(".sectionContainer").find("section").eq(1).find(".nextBtn").removeClass("noshow");
			}
			
		}, undefined, this));		
		
		// 지불수단 선택 섹션 
		this.$elPaymentContainer.on("click", $.proxy(function(self){
			self.htData.payment = $(this).data("payment-index");
			self.$elPaymentContainer.removeClass("selected");
			$(this).addClass("selected");
			
			if(self.htData.payment !== null) {
				$(this).closest(".makerequestCheckout").find(".checkoutBtn").removeClass("disabled");
			}
		}, undefined, this));
				
		this.$elContainer.find(".requestText").on("change keyup",  $.proxy(function(self){
			self.htData.requestText = $(this).val();

			if(!isEmpty($(this).val())) {
				$(this).closest(".sectionContainer").find("section").eq(2).find(".nextBtn").removeClass("noshow");
			} else {
				$(this).closest(".sectionContainer").find("section").eq(2).find(".nextBtn").addClass("noshow");
			}
		}, undefined, this));

		function isEmpty(str) {
			if(!str || str.length === 0 || (str.length > 0 && str.trim().length === 0)) {
				return true;
			}
			return false;
		}
		
		// 언어의 종류와, 의뢰문 섹션
		this.$elOriginalLang.attr("data-old-value", this.$elOriginalLang.val());
		this.$elTargetLang.attr("data-old-value", this.$elTargetLang.val());
		this.htData.originalLang =this.$elOriginalLang.val();
		this.htData.targetLang = this.$elTargetLang.val();
		
		
		this.$elOriginalLang.on("selectmenuchange", $.proxy(function(self){
			self.htData.originalLang = $(this).val();
			console.log($(this).val(), self.$elTargetLang.val(), $(this).val() === self.$elTargetLang.val())
			if($(this).val() === self.$elTargetLang.val()) {
//				self.$elTargetLang.val($(this).attr("data-old-value"));
				self.$elTargetLang.find("option").attr( "selected", false );
				self.$elTargetLang.find("option:nth-child("+(parseInt($(this).attr("data-old-value"))+1)+")").attr( "selected", true );

				self.$elTargetLang.selectmenu( "refresh" );
			

				self.$elTargetLang.attr("data-old-value", self.$elTargetLang.val());

				self.htData.originalLang = self.$elOriginalLang.val();
				self.htData.targetLang = self.$elTargetLang.val();
			}
			$(this).attr("data-old-value", $(this).val());
		}, undefined, this));

		this.$elTargetLang.on("selectmenuchange", $.proxy(function(self){
			self.htData.targetLang = $(this).val();
			
			if($(this).val() === self.$elOriginalLang.val()) {
				self.$elOriginalLang.find("option").attr( "selected", false );
				self.$elOriginalLang.find("option:nth-child("+(parseInt($(this).attr("data-old-value"))+1)+")").attr( "selected", true );
					console.log("option:nth-child("+(parseInt($(this).attr("data-old-value"))+1)+")")

				self.$elOriginalLang.selectmenu( "refresh" );
				self.$elOriginalLang.attr("data-old-value", self.$elOriginalLang.val());

				self.htData.originalLang = self.$elOriginalLang.val();
				self.htData.targetLang = self.$elTargetLang.val();

			}
			$(this).attr("data-old-value", $(this).val());
		}, undefined, this));

		this.$elLangSwitch.on("click", $.proxy(function(self) {
			var tempTargetIndex = self.$elTargetLang.val();
			var tempTargetOldIndex = self.$elTargetLang.attr("data-old-value");
			var tempOriginalIndex = self.$elOriginalLang.val();
			var tempOriginalOldIndex = self.$elOriginalLang.attr("data-old-value");		
	
			self.$elTargetLang.find("option").attr( "selected", false );
        	self.$elTargetLang.find("option:nth-child("+(parseInt(tempOriginalIndex)+1)+")").attr( "selected", true );
        	self.$elTargetLang.selectmenu( "refresh" );
			
			//self.$elTargetLang.val(self.$elOriginalLang.val());
			self.$elTargetLang.attr("data-old-value", tempTargetIndex);
			
			
			//self.$elOriginalLang.val(tempTargetIndex);
			self.$elOriginalLang.find("option").attr( "selected", false );
        	self.$elOriginalLang.find("option:nth-child("+(parseInt(tempTargetIndex)+1)+")").attr( "selected", true );
        	self.$elOriginalLang.selectmenu( "refresh" );
        	
			self.$elOriginalLang.attr("data-old-value", tempOriginalIndex);
			
			self.htData.originalLang = self.$elOriginalLang.val();
			self.htData.targetLang = self.$elTargetLang.val();
			
		}, undefined, this));
		
		// 당부의 말 섹션
		this.$elContainer.find(".contextText").on("change keyup", $.proxy(function(self){
			self.htData.contextText = $(this).val();

			if(!isEmpty($(this).val())) {
				$(this).closest(".sectionContainer").find("section").eq(5).find(".submitBtn").removeClass("noshow");
			} else {
				$(this).closest(".sectionContainer").find("section").eq(5).find(".submitBtn").addClass("noshow");
			}
		}, undefined, this));
		
		// 금액 섹션
		
		$( ".requestReward" ).spinner({
			min: DEFAULT_MIN_PRICE,
			step: 0.5,
			numberFormat: "C",
			change: $.proxy(function( self, event, ui ) {
				console.log("change:");
				var nVal = this.value;
				if(nVal < 10) {
					$( ".requestReward" ).spinner( "value", 10 );			
					self.htData.reward = parseFloat( $( ".requestReward" ).spinner( "value") );			
				}		
			}, undefined, this),
			stop: $.proxy(function( self, event, ui ) {
				console.log("stop:");
		
				self.htData.reward = this.value;
				$( ".pointToUse" ).spinner( "option", "max", self.htData.reward );
				
				// 최소금액(10) 이상의 숫자가 아닌경우에는 다음으로 넘어가는 버튼을 비활성화  

				var isValidNum = typeof parseInt(self.htData.reward) === "number" && parseFloat(self.htData.reward) >= 10
				 && /^[0-9]+(.[0-9]+)?$/.test(self.htData.reward);
				 
				if(isValidNum) {
					self.htData.reward = parseFloat( this.value );
					$(this).closest(".sectionContainer").find("section").eq(4).find(".nextBtn").removeClass("noshow");
				} else {
					$(this).closest(".sectionContainer").find("section").eq(4).find(".nextBtn").addClass("noshow");
				}
				
				$( ".pointToUse" ).spinner( "value",  Math.min(self.htData.reward, self._htOption.data.revenue) );		
				
			}, undefined, this)
		});

		var pointStopHandler = $.proxy(function( self, event, ui ) {
				console.log("stop", this.value);
				// 결제해야되는 값 뷰를  this.htData.reward - this.value 로 계속 업데이트한다.
				var min = Math.min(self.htData.reward, self._htOption.data.revenue);
				if(this.value > min) {
					this.value = min;
				}

				if(this.value < 0) {
					this.value = 0;
				}
				
				var priceToPay = (self.htData.reward.toFixed(2) - (new Number(this.value)).toFixed(2));
				var remainingPoint = ((new Number(self._htOption.data.revenue)).toFixed(2) - (new Number(this.value)).toFixed(2));
				self.htData.usePoint = this.value;
				
				$(".priceContainer > .amount").html(priceToPay.toFixed(2));
				$(".pointContainer > .amount").html(remainingPoint.toFixed(2));

				var stop =  function(evt){
					evt.stopPropagation();		
				};			
				
				// 결제 필요 금액이 0 이라면 			
				if(priceToPay == 0 ) {
					self.htData.payment = "-1";
					self.$elPaymentContainer.removeClass("selected");	
					self.$elPaymentContainer.hide();

					$(this).closest(".makerequestCheckout").find(".checkoutBtn").removeClass("disabled");					
				} 

				// 결제 필요 금액이 0 초과라면
				else {
					self.$elPaymentContainer.show();
					if(self.htData.payment == "-1") {
						// 선택 초기화	
						$(this).closest(".makerequestCheckout").find(".checkoutBtn").addClass("disabled");					
					}
				}
		
				/*
				self.htData.reward = parseFloat( this.value );	
				*/	
			}, undefined, this);
		
		$( ".pointToUse" ).spinner({
			min: 0,
			step: 0.01,
			numberFormat: "C",
			change: $.proxy(function( self, event, ui ) {
				var nVal = this.value;
				console.log("change", nVal);
				// 값이 바뀔 때 마다 
				/*
				if(nVal < 10) {
					$( ".requestReward" ).spinner( "value", 10 );			
					self.htData.reward = parseFloat( $( ".requestReward" ).spinner( "value") );			
				}*/		
			}, undefined, this),
			stop: pointStopHandler
		});
		
		$(".pointToUse").on("spinstop", pointStopHandler);	
		
		$(".sectionContainer").find("section").eq(4).find(".nextBtn").removeClass("noshow");

		
		
				
		$( ".requestReward" ).spinner("value", 10.00);
	    
	    $(".spinnerContinaer .ui-spinner-button").css("visibility", "hidden");
		$(".spinnerContinaer").append("<div class='paymoreBtn'>pay<br>more</div>");
		$(".paymoreBtn").on("click", function() {
			$(".spinnerContinaer .ui-spinner-button").css("visibility", "visible");
			$(this).hide();
		});
		
		// 마감일 섹션
		$( ".duedatePicker" ).datepicker({ 
			minDate: 0,
			onSelect: $.proxy(function(self, dateText) {
				self.htData.duedate = dateText;
				var month = dateText.split('/')[0];
				var date = dateText.split('/')[1];
				var year = dateText.split('/')[2];
				
				$(this).closest(".sectionContainer").find("section").eq(3).find(".dueyearLabel").html(year);
				$(this).closest(".sectionContainer").find("section").eq(3).find(".duemonthLabel").html(month);
				$(this).closest(".sectionContainer").find("section").eq(3).find(".duedateLabel").html(date);
				
				// SUBMIT 버튼 활성화 시도
				if(self.htData.duedate) {
					$(this).closest(".sectionContainer").find("section").eq(3).find(".nextBtn").removeClass("noshow");
				}
			}, undefined, this)
		});
		
		$( ".duehourPicker" ).spinner({
		  min: 0,
		  max: 23,
		  step: 1,
		  value: 0,
		  numberFormat: "n",
		  stop: $.proxy(function(self, event, ui) {
				self.htData.duehour = this.value;
			  
		  }, undefined, this)
		});
		$( ".duehourPicker" ).spinner("value", 23);
		this.htData.duehour = $( ".duehourPicker" ).val();

		$( ".dueminutePicker" ).spinner({
		  min: 0,
		  max: 59,
		  step: 1,
		  numberFormat: "n",
		  stop: $.proxy(function(self,  event, ui) {
				self.htData.dueminute = this.value;
		  }, undefined, this)
		});
		$( ".dueminutePicker" ).spinner("value", 59);
		this.htData.dueminute = $( ".dueminutePicker" ).val();

		if(this.htData.duedate && this.htData.duehour && this.htData.dueminute) {
			$(".sectionContainer").find("section").eq(3).find(".submitBtn").removeClass("noshow");
		}		
			
		$(".confirmSubmitBtn").on("click", function(){
			// 결제 화면으로 가기 
			requestService.postRequest({
				userEmail: authService.state.htUserProfile.user_email,
				originalLang: parseInt(this.htData.originalLang),
				targetLang: parseInt(this.htData.targetLang),
				format: this.htData.format,
				subject: this.htData.subject,
				words: this.htData.requestText.length,
				dueTime: yyyymmdd(this.htData.duedate) + " "+ this.htData.duehour+":"+ this.htData.dueminute+":00",
				reward: parseFloat(this.htData.reward),
				text: this.htData.requestText,
				context: this.htData.contextText,
				success: function(data) {
					this.htData.request_id = parseInt(data.request_id);
					
					sessionStorage.setItem('banner', JSON.stringify({
						request_id: this.htData.request_id,
						message: this._htOption.i18n.BANNER_REQUEST_SUCCESS
					}));
			
					$(".priceContainer > .amount").html(this.htData.reward.toFixed(2));
					$(".calcul > .amount").html(this.htData.reward.toFixed(2));
					
					$( ".pointToUse" ).spinner( "value", this.htData.reward.toFixed(2) );
					
					$(".pointToUse").trigger("spinstop");
										
					var pointToUse = $( ".pointToUse" ).spinner( "value" ).toFixed(2);
					
					$(".calcul > .point").html(pointToUse);



					$(".makerequestConfirm").removeClass("active");
					$(".makerequestCheckout").addClass("active");					
				}.bind(this)
			});
			
		}.bind(this));
		
			
		$(".checkoutBtn").on("click", function(){
			if(this.htData.payment) {
				console.log("this.htData.payment", this.htData.payment, this.htData.reward, this.htData.usePoint);
				var sVia;
				if(this.htData.payment == 1) {
					sVia = "paypal";
				} else if(this.htData.payment == 0) {
					sVia = "alipay";
				} else if(this.htData.payment == -1) {
					sVia = "point_only";
				}
				
				requestService.initiateCheckout({
					request_id : this.htData.request_id,
					via: sVia,
					amount: this.htData.reward,
					use_point: this.htData.usePoint,
					success: function(data) {
						location.href= data.link;				
					}
				});
			}
		}.bind(this));
		
		var bOriginalExpanded = false;
		this.$elOriginalTextToggleBtn.on("click", function(){
			bOriginalExpanded = !bOriginalExpanded;

			$(this).toggleClass("expand", bOriginalExpanded);			
			
			if(bOriginalExpanded) {
				var nOriginalTextHeight = $(this).closest(".originalTextContainer").find(".originalText").height();
				nOriginalTextHeight = nOriginalTextHeight * (120/180);					
				$(".makerequestConfirm").css("margin-top", "-"+nOriginalTextHeight+"px");
				$(this).closest(".originalTextContainer").find(".originalText").slideDown(300);
			} else {
				$(".makerequestConfirm").css("margin-top", "0");				
				$(this).closest(".originalTextContainer").find(".originalText").slideUp(300);
			}
		});
		
		function yyyymmdd(duedate) {
			var month= duedate.split("/")[0];
			var date= duedate.split("/")[1];
			var year= duedate.split("/")[2];
			return [year, month, date].join("-");
		}
	};
		
	MakeRequestView.prototype._validateRequest = function() {
		console.log(this.$elTextarea.length)
		if(this.$elTextarea.length > parseInt(this.$elMsgMax.html())) {
			alert("너무길다");
		} else {
			
			/*
			requestService.postQuickRequest({
				userEmail: authService.state.userEmail,
				originalLang: this.$elOriginalLang.val(),
				targetLang: this.$elTargetLang.val(),
				text: this.$elTextarea.val(),
				success: function(data){
					console.log(data);
					// 이렇게 하면 안된다!!!
					location.reload();
				},
				error: function(){
					
				}
			});
			*/
		}
	};
	
	return MakeRequestView;
});