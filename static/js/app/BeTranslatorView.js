//define(['jquery', 'eventemitter', 'app/RequestService', 'app/AuthService'], function($, EventEmitter, requestService, authService) {
define(['jquery', 'app/AuthService', "jquery-ui/selectmenu", "jquery-ui/magnific-popup"], function($, authService) {
	/*
	번역가 아닐경우 
			모국어 없는 티켓 클릭 시
			"현재는 모국어가 포함된 티켓만 번역가능합니다.” 메시지  팝업
		모국어 있는 티켓 클릭 시
			모국어가 아닌 해당언어의 번역권한을 신청할 수 있는 팝업 ***
		번역가 메뉴 클릭 시
			“번역에 참여하세요” 모국어를 제외한 하나의 언어를 선택해서 번역권한을 신청 팝업 ***
 		번역가(모국어가 아닌 언어의 번역권한을 하나이상 가짐) 인 경우
		 두 언어 모두 번역권한을 가진 티켓 클릭 시
			의도했던 기능 작동 
		번역권한이 부족한 티켓 클릭 시 
			“번역 권한이 부족합니다. 님은 ***, *** 만 번역가능하십니다. 번역권한 추가는 추후 지원 예정입니다” 메시지 팝업
	*/
	
	function BeTranslatorView($elContainer, htOption) {
		// jquery ui Selectmenu
						
		this.$elContainer = $elContainer;
				
		this.htOption = $.extend({}, htOption); // 무슨 옵션이 필요한지 아직 모르겠음
		
		this._buildUpElements();
		this._initEvents();
	}
	
	BeTranslatorView.prototype._buildUpElements = function() {

	};
	
	BeTranslatorView.prototype._initEvents = function() {
		

	};
	
	BeTranslatorView.prototype.showTranslatorPopup = function(nTargetLanguageIndex) {
		// nTargetLanguageIndex 가 undefined 면 옵션을 넣은 팝업을 보여준다.
		console.log(authService.state.htUserProfile);
		$.magnificPopup.open({
			items: {
			src: './BeTranslatorView_popup.html'
			},
	        callbacks: {
	            updateStatus: function(data) {
		            
		            // 권한 추가 요청이 필요한 attribute 를 삽입해준다.
		            if(data.status === "ready") {
			            // 옵션 여기에서 nTargetLanguageIndex 값에 따라 바뀌어야 하는것 처리를 해줍니다. 
	                	console.log('Status changed', data.status);
	                }
	            }
	        },
			type: 'ajax'
		});
	};
	
	BeTranslatorView.prototype._messageMapperForLanguage = function(nLangIndex, nI18nIndex) {
		var mapper = [[
			"한국어",
			"미국영어",
			"영국영어",
			"북경어",
			"광동어"
		], [
			"Korean",
			"English (U.S.)",
			"English (U.K.)",
			"Mandarin",
			"Cantonese"
		]];
		return mapper[nI18nIndex][nLangIndex];
	};

	BeTranslatorView.prototype._messageMapperForFormat = function(nLangIndex, nI18nIndex) {
		var mapper = [[
			"전자우편",
			"편지",
			"에세이",
			"광고",
			"초대장",
			"안부",
			"출판물",
			"대본",
			"리서치",
			"레포트",
			"메뉴",
			"지원서",
			"설명",
			"SNS",
			"기타"
		]];
		return mapper[nI18nIndex][nLangIndex];
	};


	BeTranslatorView.prototype._messageMapperForSubject = function(nLangIndex, nI18nIndex) {
		var mapper = [[
			"예술",
			"스포츠",
			"과학",
			"건강",
			"경제",
			"음악",
			"문학",
			"정치",
			"비즈니스",
			"종교",
			"쇼핑",
			"패션"
		]];
		return mapper[nI18nIndex][nLangIndex];
	};
		
	BeTranslatorView.prototype._validateRequest = function() {
		if(this.$elTextarea.length > parseInt(this.$elMsgMax.html())) {
			alert("너무길다");
		} else {
			
			/*
			requestService.postQuickRequest({
				userEmail: authService.state.userEmail,
				originalLang: this.$elOriginalLang.val(),
				targetLang: this.$elTargetLang.val(),
				text: this.$elTextarea.val(),
				success: function(data){
					console.log(data);
					// 이렇게 하면 안된다!!!
					location.reload();
				},
				error: function(){
					
				}
			});
			*/
		}
	};
	
	return BeTranslatorView;
});