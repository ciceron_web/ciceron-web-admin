define(['jquery', 'app/domain', 'app/AuthService', 'app/TemplateMap', 'app/RequestService', "jquery-ui/selectmenu", 'jquery-ui/magnific-popup'], function($, domain, authService, templateMap, RequestService) {
	var authService = authService;
	var ApplyHeroView = ApplyHeroView;

	function PointView($elContainer, htUserProfile) {
		this._htOption = {};
		this._htUserProfile = htUserProfile;
		
		this.$elContainer = $elContainer;
		
		this.loadTemplate({
			callback : function() {
				// incomplete 데이터를 불러와 테이블 표시를 위해 가공 후 
				// this._htOption.data 의 프로퍼티로 저장 후 	this.initAfterLoaded(); 가 실행되도록 한다.
				RequestService.getIncompleteRequests({
					success: function(data) {
						var aExpiredRequests = data.data.filter(function(v) {
							v.request_dueTime = v.request_dueTime.split(" ")[0];
							v.request_points = v.request_points.toFixed(2);
							return v.request_status === -1 ? true : false;
						});
						this._htOption.data.expiredRequests = aExpiredRequests;
						this.initAfterLoaded();
					}.bind(this)
				})		
			}.bind(this)
		});
	}
	
	PointView.prototype.loadTemplate = function(htParam) {
		templateMap.get("PointView", function(sTemplate, i18n) {
			this._htOption.i18n = $.extend({}, i18n);

			this._htOption.data = {};
			this._htOption.data.revenue = this._htUserProfile.user_revenue.toFixed(2);
						
			// this._htOption.data 를 이용해서 i18n 모든항목을 한번 더 컴파일한다.
			for(var sKey in this._htOption.i18n) {
				if(sKey !== "LAUGUAGE_KEY") {
					this._htOption.i18n[sKey] = Handlebars.compile(this._htOption.i18n[sKey])(this._htOption.data);
				} else {
					this._htOption.i18n[sKey] = JSON.parse(this._htOption.i18n[sKey]);
				}
			}

			// i18n 엔터 교체 
			for(var i in this._htOption.i18n) {
				this._htOption.i18n[i] = this._htOption.i18n[i].replace(/\n/g, "<br/>");
			}
									
			this._template = Handlebars.compile(sTemplate);
			htParam.callback();		
		}.bind(this));
	};

	PointView.prototype.initAfterLoaded = function() {
		this.render();
		this.initEvents();
	};
		
	PointView.prototype.render = function() {
		console.log(this._htOption, this._htUserProfile);
		var $elRenderedHtml = $(this._template(this._htOption));
		this.$elContainer.html($elRenderedHtml);
	};
	
	PointView.prototype.openRefundPopup = function() {
		var sRenderedHtml = Handlebars.compile($("#refundPopup").html())({
			data: this._htOption,
			i18n: this._htOption.i18n
		});
		
		$.magnificPopup.open({
			items: [{
				src: $(sRenderedHtml),// Dynamically created element
				type: 'inline'
			}]
		});	
		this.initPopupEvents();		
	};
	
	PointView.prototype.initPopupEvents = function() {
		// 이메일 값 
		$(".sendBtn").on("click", function() {
			RequestService.requestPaybackEmail({
				success: function() {
					alert("환급메일이 전송되었습니다.");
					$.magnificPopup.close();
				}
			});
		});
	};
	
	PointView.prototype.initEvents = function() {
		this.$elContainer.find(".detail").hide();
		
		this.$elContainer.find(".toggleContainer .expand").on("click", function() {
			this.$elContainer.find(".detail").show();
			this.$elContainer.find(".toggleContainer").addClass("expand");
		}.bind(this));
		this.$elContainer.find(".toggleContainer .fold").on("click", function() {
			this.$elContainer.find(".detail").hide();
			this.$elContainer.find(".toggleContainer").removeClass("expand");

		}.bind(this));
		
		this.$elContainer.find(".requestRefund").on("click", function() {
			this.openRefundPopup();
		}.bind(this));
	};

	return PointView;
});