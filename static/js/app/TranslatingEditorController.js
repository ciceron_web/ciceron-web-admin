define(['jquery', 'eventemitter', 'app/RequestService', 'app/AuthService', 'app/QuickRequestTicketView', 'app/RequestTicketView', 'app/I18nResolver', 'app/TemplateMap'], function($, EventEmitter, requestService, authService, quickRequestTicketView, requestTicketView, i18nResolver, templateMap) {

	function TranslatingEditorView($elContainer) {
				
		this.$elContainer = $elContainer;
		this._htData;
		this._htOption = {};
		
		this.loadTemplate({
			callback : function() {			
				this.initAfterLoaded();
			}.bind(this)
		});
	}
	
	TranslatingEditorView.prototype.loadTemplate = function(htParam) {
		templateMap.get("TranslatingEditorView", function(sTemplate, i18n) {
			this._htOption.i18n = $.extend({}, i18n);				
			this._template = Handlebars.compile(sTemplate);
			htParam.callback();		
		}.bind(this));
	};

	TranslatingEditorView.prototype.initAfterLoaded = function() {
		this._fetchTranslatingRequest({
			callback: function() {
			
			}.bind(this)
		});
	};	
	
	TranslatingEditorView.prototype.render = function() {		
		var $elRenderedHtml = $(this._template(this._htOption));
		this.$elContainer.html($elRenderedHtml);
		this.renderTicket();
	};
	
	TranslatingEditorView.prototype.initEvents = function() {
		// 남은 시간이 계속 업데이트 되어야함
		// 버튼을 클릭하면 임시저장 요청을 보냄 
		this.$elContainer.find(".saveBtn").on("click", function() {
			var nTone = parseInt(this.$elContainer.find(".translatedToneOption.selected").attr("data-tone-value"));
			requestService.saveTranslating({
				request_id: this._htData.request_id,
				request_translatedText: this.$elContainer.find(".targetText > textarea").val(),
				request_comment: this.$elContainer.find(".comment-container textarea").val(),
				request_tone: nTone,
				success: function() {
					alert(i18nResolver.getMap()["TRANSLATING_ALERT_TEMPORARY_SAVED"]);
				}
			});
		}.bind(this));
		
		// submit 클릭하면 최소한의 validation 을 거친 후 서밋 요청을 보낸다. 성공 시 -> 활동기록 페이지로 리다이렉트 
		this.$elContainer.find(".submitBtn").on("click", function() {
			var nTone = parseInt(this.$elContainer.find(".translatedToneOption.selected").attr("data-tone-value"));
			var translatedText = this.$elContainer.find(".targetText > textarea").val();
			var comment = this.$elContainer.find(".comment-container textarea").val();
			
			// validation 조건 
			if(!isEmpty(translatedText) && !isEmpty(comment) && !!nTone) {
				requestService.submitTranslating({
					request_id: this._htData.request_id,
					request_translatedText: translatedText,
					request_comment: comment,
					request_tone: nTone,
					success: function() {
						alert(i18nResolver.getMap()["TRANSLATING_ALERT_SUBMMITTED"]);
						location.href = "/activity";
					}
				});				
			} else {
				if(isEmpty(translatedText)) {
					alert(i18nResolver.getMap()["TRANSLATED_TEXT_EMPTY"]);
				} else if(isEmpty(comment)) {
					alert(i18nResolver.getMap()["COMMENT_EMPTY"]);
				} else if(!nTone) {
					alert(i18nResolver.getMap()["TONE_EMPTY"]);
				}
			}
		}.bind(this));	

		function isEmpty(str) {
			if(!str || str.length === 0 || (str.length > 0 && str.trim().length === 0)) {
				return true;
			}
			return false;
		}
				
		// 어조 선택 이벤트 초기화
		this.$elContainer.on("click", ".translatedToneOption", $.proxy(function(self){
			self.$elContainer.find(".translatedToneOption").removeClass("selected");
			$(this).addClass("selected");		
		}, undefined, this));
		
		$(".targetText textarea").height($(".targetText textarea")[0].scrollHeight);
		$(".originalText").css("min-height", $(".targetText").css("height"));
		$(".targetText textarea").height($(".originalText").height());

		$(".targetText textarea").on("keyup", function(){
			$(".targetText textarea").height($(".targetText textarea")[0].scrollHeight);
			
			
			$(".originalText").height($(".targetText").height());			
		});
	};
	
	TranslatingEditorView.prototype._fetchTranslatingRequest = function(htParam) {
		requestService.getTranslatingRequest({
			success: function(data) {
				if(data.data.length === 0) {
					this.$elContainer.html("<div class='container js-translating'><h1 class='empty-message'>"+i18nResolver.getMap()["TRANSLATING_EMPTY"]+"</h1></div>");
				} else {
					this._htData = data.data[0];					
					// 템플릿에 넣을 값 다시 초기화 
					this._htOption.data = {};
					this._htOption.data.remainingMinutes = this.getRemainingMinutes();

					// 맥락 엔터 교체 
					if(this._htData.request_context)
						this._htData.request_context = this._htData.request_context.replace(/\n/g, "<br/>");
							
					// 원문 엔터 교체 
					if(this._htData.request_text)
						this._htData.request_text = this._htData.request_text.replace(/\n/g, "<br/>");	
					if(this._htData.request_translatorComment)
						this._htData.request_translatorComment = this._htData.request_translatorComment.replace(/\n/g, "<br/>");
					/*		
					if(this._htData.request_translatedText)
						this._htData.request_translatedText = this._htData.request_translatedText.replace(/\n/g, "<br/>");	
					*/
					for(var sKey in this._htOption.i18n) {
						this._htOption.i18n[sKey] = Handlebars.compile(this._htOption.i18n[sKey])(this._htOption.data);
					}
					
					this.render();	
					this.initEvents();
				}

			}.bind(this)
		});
	};
		
	// _htData 를 이용해 $elContainer 속에 티켓들을 렌더링한다.
	TranslatingEditorView.prototype.renderTicket = function() {
		if(this._htData.request_isSos === true) {
			new quickRequestTicketView(this.$elContainer.find(".request-ticket-container"), {
				isAppending: false,
				rawData: this._htData,
				hasTranslatingLabel: false,
				hasTranslateNow: true, // Todo authService 를 통해 결정 
				data:{
					clientName: this._htData.request_clientName,
					originalText: this._htData.request_text,
					clientPicPath:	this._htData.request_clientPicPath || "profile_pic/2015-07-22_1.54.47.png",
					originalLangIndex: this._htData.request_originalLang,
					targetLangIndex: this._htData.request_targetLang,						
					translatorPicPath:	this._htData.request_translatorPicPath || "profile_pic/2015-07-22_1.54.47.png",
					translatorName:	this._htData.request_translatorName,
				}
			});				
		} else {
			new requestTicketView(this.$elContainer.find(".request-ticket-container"), {
				isAppending: false,
				isConfirmTicket: false,
				hasTranslatingLabel: false,
				hasTranslateNow: true, // Todo authService 를 통해 액션을
				hasAddToCart: true,
				rawData: this._htData,
				data:{
					clientName: this._htData.request_clientName,
					request_points: this._htData.request_points, // 금액
					remainingHours: this._getRemainingHoursFromNow(this._htData.request_dueTime), // "2 시간", // TODO 남은 시간을 효과적으로 표시하는 함수를 i18n 지원하도록 만들어야함
					words: this._htData.request_words,
					
					formatIndex: this._htData.request_format,
					subjectIndex: this._htData.request_subject,
										
					originalText: this._htData.request_text,
					contextText: this._htData.request_context,
					clientPicPath:	this._htData.request_clientPicPath || "profile_pic/2015-07-22_1.54.47.png",
					originalLangIndex: this._htData.request_originalLang,
					targetLangIndex: this._htData.request_targetLang,					
					translatorPicPath:	this._htData.request_translatorPicPath || "profile_pic/2015-07-22_1.54.47.png",
					translatorName:	this._htData.request_translatorName,
					translatorsInQueue: this._htData.request_translatorsInQueue
				},
			});						
		}
		
		// process-container 채우기 		
		//var nRemainingMin = this.getRemainingMinutes();
		var nTranslatingProcess = this.getTranslatingProcess();
		//this.$elContainer.find(".process-remining-min").html(nRemainingMin);
		this.$elContainer.find(".process-value").css("width", nTranslatingProcess + "%");
		
		// flag 세팅하기 
		this.$elContainer.find(".originalLang > img").attr("src", this.resourceMapperLangFlag(this._htData.request_originalLang));
		this.$elContainer.find(".targetLang > img").attr("src", this.resourceMapperLangFlag(this._htData.request_targetLang));
		
		// 원문 세팅
		this.$elContainer.find(".originalText > p").html(this._htData.request_text);
		
		// 번역문 세팅 
		if(this._htData.request_translatedText)
			this.$elContainer.find(".targetText > textarea").val(this._htData.request_translatedText);

		// 코멘트 세팅 
		if(this._htData.request_translatorComment)
			this.$elContainer.find(".comment-container > .content > textarea").val(this._htData.request_translatorComment);

		// 톤 세팅
		if(this._htData.request_translatedTone) {
			this.$elContainer.find(".tone-container > .content > .translatedToneOption[data-tone-value='"+this._htData.request_translatedTone+"']").click();
		}
	};
	
	function convertDateStringToISO(sDate) {
		return sDate.replace(" ","T")+"Z";
	}
	
	TranslatingEditorView.prototype.getRemainingMinutes = function() {
		var sDueTime = convertDateStringToISO(this._htData.request_dueTime);
		var sStartTime = convertDateStringToISO(this._htData.request_transStartTime);
		
		var nTotal = new Date(sDueTime) - new Date(sStartTime);
		var nProcess = new Date((new Date()).getTime()) - new Date(sStartTime);
		return parseInt((nTotal - nProcess) / 1000 / 60);
	}
		
	TranslatingEditorView.prototype.getTranslatingProcess = function() {
		var sDueTime = convertDateStringToISO(this._htData.request_dueTime);
		var sStartTime = convertDateStringToISO(this._htData.request_transStartTime);
		
		var nTotal = new Date(sDueTime) - new Date(sStartTime);
		
		var nRemainingMin = this.getRemainingMinutes();
		var nTotalMin = parseInt((nTotal) / 1000 / 60);
			
		return parseInt(((nTotalMin - nRemainingMin) / nTotalMin ) * 100);
	}
		
	TranslatingEditorView.prototype._getRemainingHoursFromNow = function(sTargetDatetime) {
		var diff = Math.abs(new Date((new Date()).getTime()) - new Date(sTargetDatetime));
		var sec = diff / 1000;
		var min = sec / 60;
		var hour = min / 60;
		return parseFloat(hour);
	};	
		
	TranslatingEditorView.prototype.resourceMapperLangFlag = function(nLangIndex) {
		var map = [
			"/static/img/flags/translating/Korean.png",
			"/static/img/flags/translating/English_American.png",
			"/static/img/flags/translating/English_british.png",
			"/static/img/flags/translating/Chinese.png",
			"/static/img/flags/translating/cantonese.png"
		];		
		
		return map[nLangIndex];
	};
	
	return TranslatingEditorView;
});