define(['jquery', 'eventemitter', 'app/RequestService', 'app/I18nResolver'], function($, EventEmitter, requestService, i18nResolver) {
	var i18nResolver = i18nResolver;
	// 초기 화 시에 본 클로저에 모든 템플릿을 로딩한다.
	var htTemplates = {
		"QuickRequestTicketView": null,
		"RequestTicketView": null,
		"QuickRequestView": null,
		"NotiItemView": null,
		"CompletedGroupItemTicketView": null
	};
	
	var aTemplateKeys = Object.keys(htTemplates);
	aTemplateKeys.forEach(function(templateName, index, array) {
		$.ajax({
			type: "GET",
			url: "/static/js/templates/"+templateName+".html",
			success: function(data, status, xhr) {
				htTemplates[templateName] = data;
			}.bind(this)
		});
	}.bind(this));

	function TemplateMap() {
		
	}
	
	TemplateMap.prototype.get = function(sTemplateName, fnCallback, bIsJsonResource) {
		var fnCallback = fnCallback,
		sReourcePath = bIsJsonResource ? "/static/js/json/"+sTemplateName+".json" : "/static/js/templates/"+sTemplateName+".html";
		
		if(htTemplates[sTemplateName] == null) {
			$.ajax({
				type: "GET",
				url: sReourcePath,
				success: function(data, status, xhr) {
					htTemplates[sTemplateName] = data;
					fnCallback(htTemplates[sTemplateName], i18nResolver.map);
					
				}.bind(this)
			});				
		} else {
			if(fnCallback){
				fnCallback(htTemplates[sTemplateName], i18nResolver.map);
			}
			return htTemplates[sTemplateName];
		}

	};

	TemplateMap.prototype.resourceMapperLangFlag = function(nLangIndex) {
		var map = [
			"/static/img/flags/ticket/Korean.png",
			"/static/img/flags/ticket/English_american.png",
			"/static/img/flags/ticket/English_british.png",
			"/static/img/flags/ticket/Chinese.png",
			"/static/img/flags/ticket/cantonese.png"
		];
		
		
		return map[nLangIndex];
	};

	TemplateMap.prototype.resourceMapperFormatIcon = function(nFormatIndex) {
		var map = [
			"/static/img/ICONS_CICERON-05.png",
			"/static/img/ICONS_CICERON-06.png",
			"/static/img/ICONS_CICERON-07.png",
			"/static/img/ICONS_CICERON-08.png",
			
			"/static/img/ICONS_CICERON-09.png",
			"/static/img/ICONS_CICERON-10.png",
			"/static/img/ICONS_CICERON-11.png",
			"/static/img/ICONS_CICERON-12.png",
			
			"/static/img/ICONS_CICERON-13.png",
			"/static/img/ICONS_CICERON-14.png",
			"/static/img/ICONS_CICERON-15.png",
			"/static/img/ICONS_CICERON-16.png",
			
			"/static/img/ICONS_CICERON-17.png",
			"/static/img/ICONS_CICERON-18.png",
			"/static/img/ICONS_CICERON-19.png",
		];
		
		return map[nFormatIndex];
	};
	TemplateMap.prototype.resourceMapperSubjectIcon = function(nSubjectIndex) {
		var map = [
			"/static/img/ICONS_CICERON-20.png",
			"/static/img/ICONS_CICERON-21.png",
			"/static/img/ICONS_CICERON-22.png",
			"/static/img/ICONS_CICERON-23.png",
			
			"/static/img/ICONS_CICERON-24.png",
			"/static/img/ICONS_CICERON-25.png",
			"/static/img/ICONS_CICERON-26.png",
			"/static/img/ICONS_CICERON-27.png",
			
			"/static/img/ICONS_CICERON-28.png",
			"/static/img/ICONS_CICERON-29.png",
			"/static/img/ICONS_CICERON-30.png",
			"/static/img/ICONS_CICERON-31.png"
		];
		
		return map[nSubjectIndex];
	};
	
	TemplateMap.prototype.getKeyForLanguage = function(nLangIndex) {
		var mapper = [
			"KOREAN",
			"ENGLISH_US",
			"ENGLISH_UK",
			"CHINESE_MANDARIN",
			"CHINESE_CANTONESE",
			"TAIWANESE"
		];
		return mapper[nLangIndex];
	};
	
	TemplateMap.prototype.getKeyForFormat = function(nFormatIndex) {
		var mapper = [
			"MAKE_REQUEST_FORMAT_EMAIL",
			"MAKE_REQUEST_FORMAT_LETTER",
			"MAKE_REQUEST_FORMAT_ESSAY",
			"MAKE_REQUEST_FORMAT_AD",
			"MAKE_REQUEST_FORMAT_INVITATION",
			"MAKE_REQUEST_FORMAT_GREETINGS",	
			"MAKE_REQUEST_FORMAT_PUBLICATION",
			"MAKE_REQUEST_FORMAT_SCRIPT",
			"MAKE_REQUEST_FORMAT_RESEARCH",
			"MAKE_REQUEST_FORMAT_REPORT",
			"MAKE_REQUEST_FORMAT_MENU",
			"MAKE_REQUEST_FORMAT_APPLICATION",
			"MAKE_REQUEST_FORMAT_MANUAL",
			"MAKE_REQUEST_FORMAT_SNS",
			"MAKE_REQUEST_FORMAT_OTHER"
		];
		return mapper[nFormatIndex];
	};


	TemplateMap.prototype.getKeyForSubject = function(nSubjectIndex) {
		var mapper = [
			//"MAKE_REQUEST_WHAT_SUBJECT",
			"MAKE_REQUEST_SUBJECT_ARTS",
			"MAKE_REQUEST_SUBJECT_SPORTS",
			"MAKE_REQUEST_SUBJECT_SCIENCE",
			"MAKE_REQUEST_SUBJECT_HEALTH",
			"MAKE_REQUEST_SUBJECT_ECONOMY",
			"MAKE_REQUEST_SUBJECT_MUSIC",
			"MAKE_REQUEST_SUBJECT_LITERATURE",
			"MAKE_REQUEST_SUBJECT_POLITICS",
			"MAKE_REQUEST_SUBJECT_BUSINESS",
			"MAKE_REQUEST_SUBJECT_RELIGION",
			"MAKE_REQUEST_SUBJECT_SHOPPING",
			"MAKE_REQUEST_SUBJECT_FASHION",
		];
		return mapper[nSubjectIndex];
	};		

	TemplateMap.prototype.getKeyForTone = function(nToneIndex) {
		var mapper = [
			"TONE_RESENTFUL",
			"TONE_URGENT",
			"TONE_NETURAL",
			"TONE_RESPECTFUL",
			"TONE_APPEALING",
			"TONE_CRITICAL",
			"TONE_APOLOGTIC",
			"TONE_FRANK",
			"TONE_EMOTIONAL",
			"TONE_GRATEFUL"
		];
		return mapper[nToneIndex];
	};

	TemplateMap.prototype.getKeyForFeedback = function(nFeedbackIndex) {
		var mapper = [
			"FEEDBACK_LOVE",
			"FEEDBACK_SOSO",
			"FEEDBACK_BAD"
		];
		return mapper[nFeedbackIndex];
	};	

	return new TemplateMap();
});