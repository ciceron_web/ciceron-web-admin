requirejs.config({
    baseUrl: '/static/js/lib',
    paths: {
        app: '../app'
    }
});


requirejs(['jquery', 'app/domain', 'app/AuthService', 'app/MakeRequestView', 'app/GnbView', 'app/SideNavView' ], function($, domain, authService, makeRequestView, GnbView, SideNavView) {
	authService.setUserProfile(null);

	authService.getUserProfile({
		success: function(data) { 
			var htUserProfile = data.htUserProfile;

			var oGnbView = new GnbView($('.topbar'));
			var oSideNavView = new SideNavView($("#page-container"));

/*
			$(".translator-nav").on("click", "a", function(event) {
				// 번역가 메뉴를 클릭했을 때 유저가 번역가가 아니라면
				if(htUserProfile.user_isTranslator === false) {
					// 번역가가 되세요 팝업을 띄움 
					// 팝업은 현재 API 가 없으므로 추후 구현 
					event.preventDefault();
					alert("번역가가 되세요 \n\n라는 팝업을 띄운다.\n원하는 언어를 선택해서 신청하도록 함 \n(추후구현)");
				}	 
			});
*/

			new makeRequestView($("#page-container > .content-main > .js-makerequest"));		
		}.bind(this),
		error: function(){
			//location.href='/about';
		}
	});  
});
