/**
 * About PageController
 * ( 본문 편집 Component )
 *
 * @class se.editor.canvas.view.Paragraph
 * @author happyhj@gmail.com
 * @since 2015.05.15
 *
 */
requirejs.config({
    baseUrl: '/static/js/lib',
    paths: {
        app: '../app'
    }
});

requirejs(['jquery', 'app/domain', 'app/AuthService', 'app/ApplyHeroView', 'app/I18nResolver'], function($, domain, authService, ApplyHeroView, I18nResolver) {
	
	var ApplyHeroView = ApplyHeroView;
	var i18n = I18nResolver.getMap();

    ///////////////////////////////////////////////////////////////////////////////////////////////
    
    
    if($(this).scrollTop()>$('#login').height()-100){
        $("#header_background").css("visibility","visible");
    }
    else{
        $("#header_background").css("visibility","hidden");
    }
    
    
    $("#learn_more").click(function(e){
        e.preventDefault();
        var targetOffset = $("#session1").offset().top;
        $('html,body').animate({scrollTop: targetOffset}, 500);
    });
    
    $(document).scroll(function(e){
        if($(this).scrollTop()>$('#login').height()-100){
            $("#header_background").css("visibility","visible");
        }
        else{
            $("#header_background").css("visibility","hidden");
        }
    });
    
    
    
    ///////////////////////////////////////////////////////////////////////////////////////////////
    





	authService.checkLogin(function(state){
		if(state.isLoggedIn) {
			location.href='/';
		}
		
		if(!state.isLoggedIn && !authService.state.salt) {
			authService.fetchSalt();
		}
	}.bind(this));
	
	authService.on('saltFetched', function(){
		console.log(authService.state);
	}.bind(this));
	
	$(".signin_wrap input[name='submit']").on("click", function(){
 	   	tryLogin();
    });

	$(".signup_wrap input[name='submit']").on("click", function(){    	
		trySignup();
    });
 
  	$(".signin_wrap input[type='password']").on("keyup", function(event){    	
        if (event.keyCode == 13) {
            tryLogin();
        }
    });
      
	/*
     $('.commentarea').keydown(function(event) {

    });*/
    
    var email = "";
	$(".requestReset_wrap input[name='submit']").on("click", function(){
    	email = $(".requestReset_wrap input[name='email']").val();
    	
    	if(email.length === 0) {
	    	alert(i18n.AUTH_INPUT_MISSED);
	    	return;
    	}
    	console.log("requestRecoveryCode");
		authService.requestRecoveryCode({
			"email" : email,
			success : function(data) {
				    	console.log("requestRecoveryCode", data);

				// 메시지를 다음 페이지의 메시지에 입력시키고, 
				$(".recoverEmail").html(email);
				// 페이지를 넘긴다.
				$(".authPanel-container").removeClass("forgot0 forgot1 forgot2 forgot3 forgot4");
				nAuthPage = 2;
				$(".authPanel-container").addClass("forgot3");
			},
			error : function(data) {
				// 메시지를 다음 페이지의 메시지에 입력시키고, 
				alert(i18n.AUTH_EMAIL_NOT_SIGNUP);
			}
		});
		 	   	
    });
    
    
	$(".success_wrap input[type='button']").on("click", function(){
		location.href="/";
	});
	
 	$(".requestEnter_wrap input[name='submit']").on("click", function(){
    	var recoverCode = $(".requestEnter_wrap .recoverCode").val();
    	var sNewPw = $(".requestEnter_wrap .newPw").val();
    	var sNewPwConfirm = $(".requestEnter_wrap .newPwConfirm").val();

    	if(recoverCode.length === 0 || sNewPw.length === 0 || sNewPwConfirm.length === 0) {
	    	alert(i18n.AUTH_INPUT_MISSED);
	    	return;
    	}
    	
    	if(sNewPw !== sNewPwConfirm) {
	    	alert(i18n.AUTH_PASSWORD_WRONG);
	    	return;
    	}
    	
		authService.sendRecoveryCode({
			"code" : recoverCode,
			"email" : email,
			"new_password": sNewPw,
			success : function(data) {
				$(".authPanel-container").removeClass("forgot0 forgot1 forgot2 forgot3 forgot4");
				nAuthPage = 3;
				$(".authPanel-container").addClass("forgot3");
			},
			error : function(data) {
				// 메시지를 다음 페이지의 메시지에 입력시키고, 
				alert(i18n.AUTH_RECOVERY_WRONG);
			}
		}); 	   	
    });
    
    function tryLogin() {
    	var email = $(".signin_wrap input[name='email']").val();
    	var password = $(".signin_wrap input[name='password']").val();
    	
    	if(email.length === 0 || password.length === 0) {
	    	alert(i18n.AUTH_INPUT_MISSED); // YOU SHOULD ENTER email and password both.
	    	return;
    	}
    	
		authService.login({
			"email" : email,
			"password" : password
		});
		
		authService.once("loggedIn", function(){
			authService.removeAllListeners("logInFail");
			location.href='/';
		});
		authService.once("logInFail", function(){
	    	alert(i18n.AUTH_LOGIN_FAIL);
			authService.removeAllListeners("loggedIn");
		});  	    
    }
    
    function trySignup() {
   		var name = $(".signup_wrap input[name='name']").val()
    	var email = $(".signup_wrap input[name='email']").val();
    	var password = $(".signup_wrap input[name='password']").val();
      	var passwordConfirm = $(".signup_wrap input[name='passwordConfirm']").val();

    	var mother_language_id = $("#mother_language_select").val();
    	
    	if(email.length === 0 || password.length === 0 || name.length === 0 || passwordConfirm.length === 0 || mother_language_id == -1) {
	    	alert(i18n.AUTH_INPUT_MISSED);
	    	return;
    	}
		
		if(passwordConfirm !== password) {
	    	alert(i18n.AUTH_PASSWORD_WRONG);
			return;	
		}
		
		authService.signup({
			"email" : email,
			"password" : password,
			"name" : name,
			"mother_language_id" : mother_language_id,
			success: function() {
				alert(i18n.AUTH_SIGNUP_COMPLETE);
			},
			error: function(xhr){
				if(xhr.status == 412) {
					alert(i18n.AUTH_EMAIL_DUPLICATED);
				} else if (xhr.status == 400){
					alert(i18n.AUTH_WRONG_PARAMETERS);
				}
			}
		});
		
		authService.once("signedUp", function(){
			if(mother_language_id == 0)	
				localStorage.setItem('menuLanguageIndex', 0);
			if(mother_language_id == 1 || mother_language_id == 2)	
				localStorage.setItem('menuLanguageIndex', 1);
			if(mother_language_id == 3)	
				localStorage.setItem('menuLanguageIndex', 2);

			authService.removeAllListeners("signedUp");

			authService.login({
				"email" : email,
				"password" : password
			});
			
			authService.once("loggedIn", function(){
				location.href='/';
			});
	    }.bind(this));	    
    }
    
});