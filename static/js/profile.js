requirejs.config({
    baseUrl: '/static/js/lib',
    paths: {
        app: '../app'
    }
});

requirejs(['jquery', 'app/domain', 'app/AuthService', 'app/GnbView', 'app/SideNavView', 'app/ProfileView', 'app/PointView'], function($, domain, authService, GnbView, SideNavView, ProfileView, PointView) {
	authService.setUserProfile(null);

	authService.getUserProfile({
		success: function(data) { 
			var htUserProfile = data.htUserProfile;

			var oGnbView = new GnbView($('.topbar'));		
			var oSideNavView = new SideNavView($("#page-container"));
							
			new ProfileView($(".content-main > .container"), htUserProfile);

			new PointView($(".content-main > .point-container"), htUserProfile);
		},
		error: function(){
			location.href='/about';
		}
	});


});
