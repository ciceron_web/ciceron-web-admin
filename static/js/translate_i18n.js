$(document).ready(function(){

	var originalFiles = [];
	var translatedFiles = [];
	var nowSelectedFile = null;
	var managers = [];
	var manager = new BufferedUndoManager();
	var $request_infomation = null;
	var $request_realData = null;

	i18n.setLanguage($.cookie($userInfomation.user_email + "translate"));
	if($_GET('id') == ''){
		alert("잘못된 접근입니다.");
		location.href = '/';
	}

	$.ajax({
		url: $apiURL + '/ongoing/i18n/' + $_GET('id'),
		data: '',
		processData: false,
		contentType: false,
		cache : false,
		dataType: 'JSON',
		type: 'GET',
		success: function (data) {
			if(!data.data[0].request_isI18n){
				alert("잘못된 접근입니다.");
				location.href = '/';
				return;
			}
			console.log(data.data);
			$request_infomation = data.data[0];
			$request_realData = data.realData;
			$(".translate_i18n_work").empty();
			$(".translate_i18n_tools").empty();

			$.each($request_realData, function(key, value){
				var s = "";
				var t = "";

				$(".translate_i18n_tools").append('<div class="translate_i18n_tools_line" variable_id="'+escapeHtml(value.variable_id)+'"><div class="translate_i18n_tools_options"><span class="translate_i18n_tools_options_btnContext">View the client\'s needs...</span><span class="translate_i18n_tools_options_btnComment">Add/Edit comment...</span></div></div>');
				$.each(value.texts, function(key2, value2){
					s = s +	" " + value2.sentence
					t = t + " " + value2.translations;
					// console.log(value2)

					$(".translate_i18n_tools_line[variable_id="+escapeHtml(value.variable_id)+"]").append(
						'<div class="translate_i18n_tools_sentence" paragraph_seq="'+value2.paragraph_seq+'" sentence_seq="'+value2.sentence_seq+'">'
                            +'<span class="translate_i18n_tools_sentence_source" title="'+escapeHtml(value2.sentence)+'">'+escapeHtml(value2.sentence)+'</span>'
                            +'<span class="translate_i18n_tools_sentence_draft"></span>'
                            +'<span class="translate_i18n_tools_sentence_translation" contenteditable="plaintext-only" placeholder="Enter text here...">'+escapeHtml(value2.translations)+'</span>'
                        +'</div>'
					);
				});
				$(".translate_i18n_tools_line[variable_id="+escapeHtml(value.variable_id)+"]").append(
					'<div class="translate_i18n_tools_comment"><img class="img-circle translate_i18n_tools_comment_profile" style="width: 24px; height: 24px;" src="/api/access_file/'+$request_infomation.request_translatorPicPath+'" /><span class="translate_i18n_tools_comment_text" contenteditable="plaintext-only">'+(value.comment?escapeHtml(value.comment):'')+'</span></div>'
				);				
				$(".translate_i18n_work").append(
					'<div class="translate_i18n_work_line buttons" variable_id="'+escapeHtml(value.variable_id)+'">'
                        +'<div class="translate_i18n_work_name">'
                            + escapeHtml(key)
                        +'</div>'
                        +'<div class="translate_i18n_work_from" title="'+escapeHtml(s)+'">'
                            + escapeHtml(s)
                        +'</div>'
                        +'<div class="translate_i18n_work_to">'
                            + escapeHtml(t)
                        +'</div>'
                    +'</div>'
				);
				// $clamp($(".translate_i18n_work .translate_i18n_work_line div"), {clamp: 3});	
			});
			$(".translate_i18n_work_line:first-child").click();
		},
		error: function (data) {
			alert("잘못된 접근입니다.");
			location.href = '/';
			return;
		}
	});



	$("body").on("click", ".translate_i18n_tools_sentence", function(){
		if($(this).hasClass("selected")) {
			return;
		};
		$(".translate_i18n_tools_sentence").removeClass("selected");
		$(this).addClass("selected");
		$(this).find(".translate_i18n_tools_sentence_translation").focus();
		// $(this).find(".translate_i18n_tools_sentence_source").click();

	});

	$("body").on("click", ".translate_i18n_work_line", function(e){
		// if($(this).hasClass("selected")) {
		// 	return
		// };
		
		$(".translate_i18n_work_line").removeClass("selected");
		$(this).addClass("selected");
		$(".translate_i18n_tools_line").removeClass("selected");
		$(".translate_i18n_tools_line[variable_id="+$(this).attr("variable_id")+"]").addClass("selected");
		setTimeout(function() {
			$(".translate_i18n_tools_line.selected .translate_i18n_tools_sentence:first-child").click();
		}, 10);
	});

	// $("body").on("click", ".translate_i18n_tools_sentence_source", function(){
	// 	$sentence = $(this).parent(".translate_i18n_tools_sentence");
	// 	$sentence.find(".translate_i18n_tools_sentence_translation").focus();

	// });

	$("body").on("focus", ".translate_i18n_tools_sentence_translation", function(){
		$sentence = $(this).parent(".translate_i18n_tools_sentence");
		if($sentence.find(".translate_i18n_tools_sentence_draft").text() == ""){
			$sentence.find(".translate_i18n_tools_sentence_draft").html('<img class="loading" src="/static/img/loading.gif" style="height: 10px;"/>');
			getDraft($sentence.find(".translate_i18n_tools_sentence_source").text(), $sentence.find(".translate_i18n_tools_sentence_draft"));
		}
	});

	$("body").on("focusout", ".translate_i18n_tools_sentence_translation", function(){
		///api/user/translations/ongoing/i18n/<request_id>/variable/<variable_id>/paragraph/<paragraph_seq>/sentence/<sentence_seq>
		$sentence = $(this).parent(".translate_i18n_tools_sentence");
		$line = $sentence.parent(".translate_i18n_tools_line");

		var formData = new FormData();
		formData.append("text", $(this).text());

		$.ajax({
			url: '/api/user/translations/ongoing/i18n/'+$request_infomation.request_id+'/variable/'+ $line.attr("variable_id"
			) +'/paragraph/'+$sentence.attr("paragraph_seq")+'/sentence/'+$sentence.attr("sentence_seq"),
			data: formData,
			processData: false,
			contentType: false,
			dataType: 'JSON',
			type: 'PUT',
			success: function (data) {
				console.log(data);
			}
		});

	});
	
	$("body").on("focusout", ".translate_i18n_tools_comment_text", function(){
		// console.log(":fdsa");
		///api/user/translations/ongoing/i18n/<request_id>/variable/<variable_id>/paragraph/<paragraph_seq>/sentence/<sentence_seq>
		// $sentence = $(this).parent(".translate_i18n_tools_sentence");
		// $line = $sentence.parent(".translate_i18n_tools_line");
		$line = $(".translate_i18n_tools_line.selected");
		$sentence = $line.find(".translate_i18n_tools_sentence");
		var formData = new FormData();
		formData.append("comment_string", $(this).text());

		$.ajax({
			url: '/api/user/translations/ongoing/i18n/'+$request_infomation.request_id+'/variable/'+ $line.attr("variable_id"
			) +'/comment',
			data: formData,
			processData: false,
			contentType: false,
			dataType: 'JSON',
			type: 'PUT',
			success: function (data) {
				console.log(data);
			}
		});

	});

	$("body").on("keydown keyup", ".translate_i18n_tools_sentence_translation", function(e){
		$sentence = $(this).parent(".translate_i18n_tools_sentence");
		$line = $sentence.parent(".translate_i18n_tools_line");
		console.log($sentence);
		console.log($line);
		console.log($line.attr("variable_id"));
		console.log($line.find(".translate_i18n_tools_sentence_translation").text());
		$(".translate_i18n_work_line[variable_id="+$line.attr("variable_id")+"] .translate_i18n_work_to").text($line.find(".translate_i18n_tools_sentence_translation").map(function () {
			return $(this).text();
		}).get().join(" "));

	});	

	$("body").on("focus", ".translate_i18n_tools_sentence_translation", function(e){
		$sentence = $(this).parent(".translate_i18n_tools_sentence");
		$line = $sentence.parent(".translate_i18n_tools_line");

	});	

	$("body").on("click", ".translate_i18n_tools_options_btnComment", function(e){
		// $sentence = $(this).parents(".translate_i18n_tools_sentence");
		$line = $(".translate_i18n_tools_line.selected");
		$line.find(".translate_i18n_tools_comment_text").focus();
		console.log($(".translate_i18n_tools_comment_text").offset().top);
		// console.log($(".translate_i18n_tools_comment_text").offset().top);
		$('html,body').animate({scrollTop: ($line.find(".translate_i18n_tools_comment_text").offset().top - ($(window).height() / 2))}, 500);
	});

	var getDraft = function($sentence, $target){
		var s = [$sentence];

		var formData = new FormData();
		formData.append("user_email", $userInfomation.user_email);
		formData.append("request_id", $request_infomation.request_id);
		formData.append("sentence", JSON.stringify(s));
		formData.append("source_lang_id", $request_infomation.request_originalLang);
		formData.append("target_lang_id", $request_infomation.request_targetLang);

		$.ajax({
			url: '/api/tool/initial_translate',
			data: formData,
			processData: false,
			contentType: false,
			dataType: 'JSON',
			type: 'POST',
			success: function (data) {
				// console.log(data);
				if($target)
				{
					$target.text($("<div />").html(data.cand1[0]).text());
				}
				return data.cand1;
			}
		});
	}

});