$(document).ready(function(){

	var originalFiles = [];
	var translatedFiles = [];
	var nowSelectedFile = null;
	var managers = [];
	var manager = new BufferedUndoManager();
	var $request_infomation = null;

	i18n.setLanguage($.cookie($userInfomation.user_email + "translate"));
	if($_GET('id') == ''){
		alert("잘못된 접근입니다.");
		location.href = '/';
	}
	// var formData = new FormData();
	// formData.append("user_email", $userInfomation.user_email);
	// formData.append("request_id", $_GET('id'));
	// formData.append("sentence", "어제 보내드린 건 잘 받으셨나요?");
	// formData.append("source_lang_id", 1);
	// formData.append("target_lang_id", 2);

	// $.ajax({
	// 	url: '/api/initial_translate',
	// 	data: formData,
	// 	processData: false,
	// 	contentType: false,
	// 	dataType: 'JSON',
	// 	type: 'POST',
	// 	success: function (data) {
	// 		console.log(data);
	// 	}
	// })

	$.ajax({
		url: $apiURL + '/' + $_GET('id'),
		data: '',
		processData: false,
		contentType: false,
		cache : false,
		dataType: 'JSON',
		type: 'GET',
		success: function (data) {
			if(!data.data[0].request_isFile){
				alert("잘못된 접근입니다.");
				location.href = '/';
				return;
			}
			$request_infomation = data.data[0];

			var xhr = new XMLHttpRequest(); 
			// xhr.open("GET", "/api/access_file/" + data.data[0].request_filePath, true); 
			xhr.open("GET", "/" + data.data[0].request_filePath, true); 
			xhr.responseType = "blob";//force the HTTP response, response-type header to be blob
			xhr.onload = function()
			{
				
				unZipModel.getEntries(xhr.response, function(entries) {
					// originalFiles = entries;
					$('.translate_file_choose').empty();
					// $('.translate_file_choose').width(entries.length * 20 * 200);
					for (var i = 0; i < entries.length; i++) {
						originalFiles.push(new File([""], "requestedFile" + i));
						translatedFiles.push(new File([""], "translatedFile" + i));
						managers.push(new BufferedUndoManager());
						$('.translate_file_choose').append('<div class="translate_file_file buttons">'+
						'<span class="translate_file_fileName"><img class="loading" src="/static/img/loading.gif" style="height: 10px;"/></span>'+
						'<div class="translate_file_filePreview"></div>'+
						'</div>');
						$('.translate_file_work').append('<div class="translate_file_work_from" />');
						$('.translate_file_work').append('<div class="translate_file_work_to" />');
					}
					$.each(entries, function(i, entry){
						entry.getData(new zip.BlobWriter(), function(blob){
							var tempFile = new File([blob], entry.filename);
							originalFiles[i] = tempFile;
							$('.translate_file_file:eq('+i+') .translate_file_fileName').text(tempFile.name);
							$('.translate_file_file:eq('+i+')').attr("title", tempFile.name);
							detectFile(tempFile, $('.translate_file_file:eq('+i+')'));
						});
						
						// alert(entry.filename);
					});
				});
			};
			xhr.send();
		},
		error: function (data) {
			
		}
	});

	//translate_file_choose

    var startCoords = 0;
    var endCoords = 0;
	var originalX = 0;
	// $("body").on('touchstart', '.translate_file_choose', function(e){
    //     // e.preventDefault();
    //     startCoords = endCoords = e.originalEvent.targetTouches[0].pageX;
    //     originalX = parseInt($(this).css('transform').split('(')[1].split(')')[0].split(',')[4]);
    //     $(this).css({'-webkit-transition': 'none',
    //                 '-moz-transition': 'none',
    //                 'transition': 'none'});

    // });

    // $("body").on('touchmove', '.translate_file_choose', function(e){
        
    //     endCoords = e.originalEvent.targetTouches[0].pageX;

    //     if(startCoords - endCoords > 60 || endCoords - startCoords > 60){
    //         $(this).css('transform', 'translateX(' + ( originalX + endCoords - startCoords) +'px)');
	// 		console.log($(this).css('transform').split('(')[1].split(')')[0].split(','));
    //     }
        
    //     if(startCoords - endCoords > 60 || endCoords - startCoords > 60){
    //         e.preventDefault();
    //     }

    // });
    // $("body").on('touchend', '.translate_file_choose', function(e){
    //     e.preventDefault();
    //     $(this).css({'-webkit-transition': 'all 0.2s ease-in-out',
    //                 '-moz-transition': 'all 0.2s ease-in-out',
    //                 'transition': 'all 0.2s ease-in-out'});
    // });


	$('body').on('click', '.translate_file_file', function(){
		if($(this).hasClass('selected')){
			return;
		}

		$('.translate_file_file').removeClass('selected');
		$(this).addClass('selected');
		$('html,body').animate({scrollTop: $('.translate_file_work').offset().top - $("#header").outerHeight() - 10}, 500);


		switch (detectFile(originalFiles[$(this).index()])) {
			case "image":
				showImage($(this).index());
				break;
			
			case 'json':
				showJson($(this).index());
				break;

			case 'xml': 
				showXml($(this).index());
				break;

			case 'txt':
				showTxt($(this).index());
				break;

			default:
				break;
		}
		


	});

	// $('body').on('mousewheel DOMMouseScroll', '.translate_file_choose', function(e){

    //     var delta = Math.max(-1, Math.min(1, (e.originalEvent.wheelDelta || -e.originalEvent.detail)));

    //     $(this).scrollLeft( $(this).scrollLeft() - ( delta * 20 ) );
	// 	if($(this).scrollLeft() > 0 && $(this).scrollLeft() < $(this)[0].scrollWidth - $(this).outerWidth()){
	// 		e.preventDefault();
	// 	}
    // });
	$('body').on('click', '.scroll_button', function(e){
		var $targetDiv = $($(this).parent().find('div').get(0));
		var s = parseInt($targetDiv.width() / 300) * 300;
		if(s<300)s=300;
		if($(this).hasClass('scroll_left')){
			$targetDiv.animate({scrollLeft: $targetDiv.scrollLeft() - s}, 500, "easeInOutQuad", function(){});
		}
		else if($(this).hasClass('scroll_right')){
			$targetDiv.animate({scrollLeft: $targetDiv.scrollLeft() + s}, 500, "easeInOutQuad", function(){});
		}
	});

	function detectFile($file, $previewDiv){
		
		var $type = "";

		switch ($file.name.split('.').pop().toLowerCase()) {
			case 'jpg':
			case 'jpeg':
			case 'png':
			case 'bmp':
			case 'gif':
				$type = "image";
				break;
			
			case 'json':
				$type = "json";
				break;

			case 'xml': 
				$type = "xml";
				break;

			case 'txt':
				$type = "txt";
				break;

			default:
				$type = "unknown";
				break;
		}

		if($previewDiv){
			switch ($type) {
				case "image":
					previewImage($file, $previewDiv);
					break;
			
				case 'json':
					previewJson($file, $previewDiv);
					break;

				case 'xml': 
					previewXml($file, $previewDiv);
					break;

				case 'txt':
					previewTxt($file, $previewDiv);
					break;

				default:
					break;
			}
		}
		return $type;
	}

	function previewImage($file, $previewDiv){
		
        var reader = new FileReader();
        reader.onload = function (e) {
			$previewDiv.find('.translate_file_filePreview').append('<div style="width: 100%; height: 100%; text-align: center; background-image: url('+e.target.result+'); background-size: cover; background-position: center;"></div>');

		}
		reader.readAsDataURL($file);

	}

	function previewJson($file, $previewDiv){
		
        var reader = new FileReader();
        reader.onload = function (e) {
			var data = JSON.parse(e.target.result);
			// $previewDiv.find('.translate_file_filePreview').append('<div style="width: 100%; height: 100%; text-align: center;"><img src="'+e.target.result+'" style="height: 100%;"></div>');
			$previewDiv.find('.translate_file_filePreview').append('<pre><code class="json">' + JSON.stringify(data, null, 4) + '</code></pre>');
			hljs.highlightBlock($previewDiv.find('.translate_file_filePreview code').get(0));
			
			// $previewDiv.find('.translate_file_filePreview').css('height','auto');
			$clamp($previewDiv.find('.translate_file_filePreview')[0], {clamp: '100%'});

		}
		reader.readAsText($file);
	}

	function previewXml($file, $previewDiv){
		
        var reader = new FileReader();
        reader.onload = function (e) {
			var data = e.target.result;
			// $previewDiv.find('.translate_file_filePreview').append('<div style="width: 100%; height: 100%; text-align: center;"><img src="'+e.target.result+'" style="height: 100%;"></div>');
			$previewDiv.find('.translate_file_filePreview').append('<pre><code class="xml">' + $('<html>').text(data).html() + '</code></pre>');
			hljs.highlightBlock($previewDiv.find('.translate_file_filePreview code').get(0));
			
			// $previewDiv.find('.translate_file_filePreview').css('height','auto');
			$clamp($previewDiv.find('.translate_file_filePreview')[0], {clamp: '100%'});

		}
		reader.readAsText($file);
	}

	function previewTxt($file, $previewDiv){
		
        var reader = new FileReader();
        reader.onload = function (e) {
			var data = e.target.result;
			// $previewDiv.find('.translate_file_filePreview').append('<div style="width: 100%; height: 100%; text-align: center;"><img src="'+e.target.result+'" style="height: 100%;"></div>');
			$previewDiv.find('.translate_file_filePreview').append('<pre>'+data+'</pre>');
			
			// $previewDiv.find('.translate_file_filePreview').css('height','auto');
			$clamp($previewDiv.find('.translate_file_filePreview')[0], {clamp: '100%'});

		}
		reader.readAsText($file);
	}

	function clearWorkspace(){
		// $(".translate_file_work_from").empty();
		// $(".translate_file_work_to").empty();
		$(".translate_file_work_from").removeClass("selected");
		$(".translate_file_work_to").removeClass("selected");
		console.log('이벤트삭제');

		$('.translate_file_work').off('click', '**');
		$('.translate_file_work').off('focus', '**');
		$('.translate_file_work').off('focusout', '**');
		$('.translate_file_work').off('keydown', '**');
		$('.translate_file_work').off('keyup', '**');

		$('.translate_file_work').off('click', '.translate_file_work *, .translate_file_work');
		$('.translate_file_work').off('focus', '.translate_file_work *, .translate_file_work');
		$('.translate_file_work').off('focusout', '.translate_file_work *, .translate_file_work');
		$('.translate_file_work').off('keydown', '.translate_file_work *, .translate_file_work');
		$('.translate_file_work').off('keyup', '.translate_file_work *, .translate_file_work');
		$('.translate_file_work *').off();
	}

	function showImage($file){
		clearWorkspace();
		
		if($.isNumeric($file)){
			$file = originalFiles[parseInt($file)];
		}

		var $fileIndex = originalFiles.indexOf($file);

		var $divFrom = $($('.translate_file_work_from').get($fileIndex));
		var $divTo = $($('.translate_file_work_to').get($fileIndex));
		
		$divFrom.addClass("selected");
		$divTo.addClass("selected");

		if($divFrom.html() == ""){

			// $('.translate_file_work_from').css('margin', 0);
			// $('.translate_file_work_from').css('padding', 0);
			// $('.translate_file_work').css('margin', 0);
			// $('.translate_file_work').css('padding', 0);
			$divFrom.append('<div class="translate_work_imagePreview"><img class="translate_work_imagePreview_img" /></div>');
			// var c = $('.translate_file_work_from .translate_work_imagePreview').get(0);
			// var ctx = c.getContext("2d");
			// c.width = c.width;
			
			var reader = new FileReader();
			reader.onload = function (e) {

				// $('.content_photo_preview').attr('src', e.target.result);
				
				var img = new Image();
				img.src = e.target.result;
				$divFrom.find('.translate_work_imagePreview_img').attr("src", e.target.result);
				$divFrom.find('.translate_work_imagePreview').css("width", "100%");
				// $('.translate_work_imagePreview').css("height", (parseFloat(img.height) / parseFloat(img.width) * 100.00) + "%");
				$divFrom.find('.translate_work_imagePreview').css("height", "auto");

				var formData = new FormData();
				formData.append("data",$file);
				$.ajax({
					// url: 'https://api.projectoxford.ai/vision/v1.0/ocr?language=unk&detectOrientation=true',
					url: '/static/request_doc/ocr_dummy.json',
					// beforeSend: function (request)
					// {
					// 	request.setRequestHeader("Ocp-Apim-Subscription-Key", "550f774dcb774819884b4fafdd3f6683");
					// },
					// data: formData,
					processData: false,
					contentType: false,
					dataType: "JSON",
					// type: 'POST',
					type: 'GET',
					error: function(xhr, ajaxOptions, thrownError){
						var data = JSON.parse(xhr.responseText);
						alert("사진 분석을 실패하였습니다.\n" + data.message);
					}
				}).done(function(data){
				
					// c.width = img.width;
					// c.height = img.height;
					
					
					// ctx.drawImage(img, 0,0);
					// ctx.translate(img.width / 2 , img.height / 2);
					// ctx.rotate((data.textAngle * Math.PI / 180));
					// ctx.translate(-img.width / 2 , -img.height / 2);
					$(data.regions).each(function(i, regions){
						$box = regions.boundingBox.split(',');

						$divFrom.find(".translate_work_imagePreview").append('<div class="translate_work_imagePreview_section region" region="'+i+'" style="left: '+(parseFloat($box[0]) / parseFloat(img.width) * 100.00)+'%; top: '+(parseFloat($box[1]) / parseFloat(img.height) * 100.00)+'%; width: '+(parseFloat($box[2]) / parseFloat(img.width) * 100.00)+'%; height: '+(parseFloat($box[3]) / parseFloat(img.height) * 100.00)+'%; transform: rotate('+(data.textAngle * Math.PI / 180)+'deg)"></div>')

						$(regions.lines).each(function(j, line){
							$(line.words).each(function(k, word){
								// $box = word.boundingBox.split(',');
								// $divFrom.find(".translate_work_imagePreview").append('<div class="translate_work_imagePreview_section" region="'+i+'" line="'+j+'" word="'+k+'" style="left: '+(parseFloat($box[0]) / parseFloat(img.width) * 100.00)+'%; top: '+(parseFloat($box[1]) / parseFloat(img.height) * 100.00)+'%; width: '+(parseFloat($box[2]) / parseFloat(img.width) * 100.00)+'%; height: '+(parseFloat($box[3]) / parseFloat(img.height) * 100.00)+'%; transform: rotate('+(data.textAngle * Math.PI / 180)+'deg)"></div>');

								$divFrom.find(".translate_work_imagePreview .translate_work_imagePreview_section.region[region="+i+"]").text($divFrom.find(".translate_work_imagePreview .translate_work_imagePreview_section.region[region="+i+"]").text() + " " + word.text);
								// ctx.fillStyle="red";
								// ctx.globalAlpha=0.5;
								// ctx.fillRect($box[0],$box[1],$box[2],$box[3]);
							});
							$divFrom.find(".translate_work_imagePreview .translate_work_imagePreview_section.region[region="+i+"]").text($divFrom.find(".translate_work_imagePreview .translate_work_imagePreview_section.region[region="+i+"]").text() + "\n");
							
						});
						$divTo.append('<div class="divSection" style="width: 100%; height: auto;"><span class="spanSection" style="display: block; width: 100%; height: auto;">섹션 ' + i + '</span><span class="spanContent" contenteditable="plaintext-only" style="min-width: 100px; width: auto; height: auto;"> </span></div>')
							
					}); 
					// ctx.stroke();
				});
			}

			reader.readAsDataURL($file);
		}


		$('.translate_file_work').off('click', '.translate_file_work_from.selected .translate_work_imagePreview_section.region').on('click', 'translate_file_work_from.selected .translate_work_imagePreview_section.region', function(){
			// alert($(this).text())
			// $divFrom.append('<span></span>');
			var $divFromSpan = $($('.translate_file_work_to.selected .spanContent').get($('.translate_file_work_from.selected .region').index($(this))));
			$divFromSpan.focus();
		});

		$('.translate_file_work').off('click', '.translate_file_work_to.selected .divSection').on('click', '.translate_file_work_to.selected .divSection', function(){
			// alert($(this).text())
			// $divFrom.append('<span></span>');
			$(this).find('.spanContent').focus();
		});

		$('.translate_file_work').off('focus', '.translate_file_work_to.selected .spanContent').on('focus', '.translate_file_work_to.selected .spanContent', function(){

			var $divFromSection = $($('.translate_file_work_from.selected .region').get($('.translate_file_work_to.selected .spanContent').index($(this))));

			// $divFrom.find('.translate_work_imagePreview').css('transform-origin', (parseFloat($divFromSection[0].style.left)) + "% " + (parseFloat($divFromSection[0].style.top)) + "%");
			$divFrom.find('.translate_work_imagePreview').css('transform-origin', '0% 0%');
			var $scale = $divFrom.find('.translate_work_imagePreview').width() / $divFromSection.width();
			if($scale > 2.0) $scale = 2.0;
			$divFrom.find('.translate_work_imagePreview').css({'transform': 'scaleX('+($scale)+') scaleY('+($scale)+') translateX(-'+$divFromSection[0].style.left+') translateY(-'+$divFromSection[0].style.top+')'});
			$divFrom.find('.translate_work_imagePreview').scrollTop(0);


			// $('.translate_file_work_from .region').addClass('selected');
			$('.translate_file_work_from.selected .region').removeClass('selected');
			$divFromSection.addClass('selected');
			if($(this).text() == ""){
				$(this).text(" ");
			}
			var $t = $(this).text();
			var $th = $(this);
			$(this).text("");
			
			setTimeout(function() {
				$th.text($t);
			}, 100);
			// alert('scale('+($divFrom.find('.translate_work_imagePreview').width() / $divFromSection.width())+') translate('+$divFromSection[0].style.left+' '+ $divFromSection[0].style.top +')')

			// console.log("width: " + parseFloat($($('.translate_file_work_from .region').get($('.translate_file_work_to .spanContent').index($(this))))[0].style.width));
			// console.log("height: " + $($('.translate_file_work_from .region').get($('.translate_file_work_to .spanContent').index($(this))))[0].style.height);
			// console.log("left: " + $($('.translate_file_work_from .region').get($('.translate_file_work_to .spanContent').index($(this))))[0].style.left);
			// console.log("top: " + $($('.translate_file_work_from .region').get($('.translate_file_work_to .spanContent').index($(this))))[0].style.top);
			// console.log("pixelWidth: " + $divFrom.find('.translate_work_imagePreview').width());
			// console.log("pixelWidth2: " + $divFromSection.width());
			// console.log("scale:" + ($divFrom.find('.translate_work_imagePreview').width() / $divFromSection.width()))
			

		});

		$('.translate_file_work').off('focusout', '.translate_file_work_to.selected .spanContent').on('focusout', '.translate_file_work_to.selected .spanContent', function(){
			$divFrom.find('.translate_work_imagePreview').css({'transform': 'scaleX(1) scaleY(1) translateX(0) translateY(0)'});
			$('.translate_file_work_from.selected .region').removeClass('selected');
			
		})		
		$('.translate_file_work').off('keydown keyup', '.translate_file_work_to.selected .spanContent').on('keydown keyup', '.translate_file_work_to.selected .spanContent', function(e){
			if($(this).text().indexOf("\n\n") >= 0) {
				
				e.preventDefault();
				$(this).text($(this).text().replace("\n\n", ""));
				$('.translate_file_work_to.selected .spanContent').get($('.translate_file_work_to.selected .spanContent').index($(this))+1).focus();
			}
			
		});

	}

	function showJson($file){
		clearWorkspace();
		var $translatedFile;
		if($.isNumeric($file)){
			$translatedFile = translatedFiles[$file];
			$file = originalFiles[parseInt($file)];
		}
		else{
			$translatedFile = translatedFiles[originalFiles.indexOf($file)];
		}
		
		if($translatedFile.size < 1){
			$translatedFile = $file;
		}

		var $fileIndex = originalFiles.indexOf($file);

		var $divFrom = $($('.translate_file_work_from').get($fileIndex));
		var $divTo = $($('.translate_file_work_to').get($fileIndex));
		
		$divFrom.addClass("selected");
		$divTo.addClass("selected");
		
		if($divFrom.html() == ""){

			var reader = new FileReader();
			reader.onload = function (e) {
				var data = JSON.parse(e.target.result);
				// $previewDiv.find('.translate_file_filePreview').append('<div style="width: 100%; height: 100%; text-align: center;"><img src="'+e.target.result+'" style="height: 100%;"></div>');
				$divFrom.append('<pre><code class="json">' + JSON.stringify(data, null, 4) + '</code></pre>');
				hljs.highlightBlock($divFrom.find('code').get(0));
				
				// $previewDiv.find('.translate_file_filePreview').css('height','auto');
				// $clamp($previewDiv.find('.translate_file_filePreview')[0], {clamp: '100%'});

			}
			reader.readAsText($file);

			var reader2 = new FileReader();
			reader2.onload = function (e) {
				var data = JSON.parse(e.target.result);
				// $previewDiv.find('.translate_file_filePreview').append('<div style="width: 100%; height: 100%; text-align: center;"><img src="'+e.target.result+'" style="height: 100%;"></div>');
				$divTo.append('<pre><code class="json">' + JSON.stringify(data, null, 4) + '</code></pre>');
				hljs.highlightBlock($divTo.find('code').get(0));
				
				$divTo.find('code .hljs-string').attr("contenteditable", "plaintext-only");
				managers[$fileIndex].reset($divTo.html());
				$divTo.find('code .hljs-string').each(function(i, item){
					var formData = new FormData();
					formData.append("user_email", $userInfomation.user_email);
					formData.append("request_id", $request_infomation.request_id);
					formData.append("sentence", $(item).text());
					formData.append("source_lang_id", $request_infomation.request_originalLang);
					formData.append("target_lang_id", $request_infomation.request_targetLang);

					$.ajax({
						url: '/api/tool/initial_translate',
						data: formData,
						processData: false,
						contentType: false,
						dataType: 'JSON',
						type: 'POST',
						success: function (data) {
							console.log(data);
							$(item).text($("<div />").html(data.cand1).text());
						}
					});
				});
				
				// $previewDiv.find('.translate_file_filePreview').css('height','auto');
				// $clamp($previewDiv.find('.translate_file_filePreview')[0], {clamp: '100%'});

			}
			reader2.readAsText($translatedFile);

		}

		$('.translate_file_work').off('focus','.translate_file_work_to code .hljs-string').on('focus','.translate_file_work_to code .hljs-string', function(e){
			// $(".translate_file_work_from").scrollTop($(".translate_file_work_to").scrollTop() - $(this).position().top + $($('.translate_file_work_from code .hljs-string').get($('.translate_file_work_to code .hljs-string').index($(this)))).position().top);
			
			$(".translate_file_work_from.selected").scrollTop($(".translate_file_work_to.selected").scrollTop());
			$(".translate_file_work_from.selected").scrollTop($(".translate_file_work_to.selected").scrollTop() - $(this).position().top + $($('.translate_file_work_from.selected span').get($('.translate_file_work_to.selected span').index($(this)))).position().top);

			$('.translate_file_work_from code .hljs-string').removeClass('selected');
			$($('.translate_file_work_from code .hljs-string').get($('.translate_file_work_to code .hljs-string').index($(this)))).addClass('selected');
			if (document.selection) document.selection.empty(); 
			else if (window.getSelection) window.getSelection().removeAllRanges();

			if (document.selection) {
				var range = document.body.createTextRange();
				range.moveToElementText(this);
				range.select();
			}
			else if (window.getSelection) {
				var range = document.createRange();
				range.selectNodeContents(this);
				window.getSelection().addRange(range);
			}
		
		});


		$('.translate_file_work').off('keydown').on('keydown', function(e){
			if((e.metaKey || e.ctrlKey) && String.fromCharCode(e.which).toLowerCase() === 'z'){
				e.preventDefault();
				if(managers[$fileIndex].canUndo()){
					managers[$fileIndex].undo();
					$divTo.html(managers[$fileIndex].state);
					// window.getSelection().removeAllRanges();
					// window.getSelection().addRange(managers[$fileIndex].state['cursor']);
					$divTo.find('code .hljs-string').each(function(i, item){
						if(!$(item).text().startsWith('"')){
							$(item).text('"' + $(item).text());
						}
						if(!$(item).text().endsWith('"')){
							$(item).text($(item).text() + '"');
						}
						if($(item).text() == '"'){
							$(item).text('""');
						}
					});
				}
			}
			else if((e.metaKey || e.ctrlKey) && String.fromCharCode(e.which).toLowerCase() === 'y'){
				e.preventDefault();
				if(managers[$fileIndex].canRedo()){
					managers[$fileIndex].redo();
					$divTo.html(managers[$fileIndex].state);
					$divTo.find('code .hljs-string').each(function(i, item){
						if(!$(item).text().startsWith('"')){
							$(item).text('"' + $(item).text());
						}
						if(!$(item).text().endsWith('"')){
							$(item).text($(item).text() + '"');
						}
						if($(item).text() == '"'){
							$(item).text('""');
						}
					});
					// window.getSelection().removeAllRanges();
					// window.getSelection().addRange(managers[$fileIndex].state['cursor']);
				}
			}
		})
		
		$('.translate_file_work').off('keydown','.translate_file_work_to code .hljs-string').on('keydown','.translate_file_work_to code .hljs-string', function(e){
			if(e.which == 13){
				e.preventDefault();
				$('.translate_file_work_to code .hljs-string').get($('.translate_file_work_to code .hljs-string').index($(this))+1).focus();
				// managers[$fileIndex].update($divTo.html(), {force: true});
			}
			else{
				// managers[$fileIndex].update({"html": $divTo.html(), "cursor": window.getSelection().getRangeAt(0)});
				managers[$fileIndex].update($divTo.html());
			}
		});

		$('.translate_file_work').off('focusout','.translate_file_work_to code .hljs-string').on('focusout','.translate_file_work_to code .hljs-string', function(e){
			if(!$(this).text().startsWith('"')){
				$(this).text('"' + $(this).text());
			}
			if(!$(this).text().endsWith('"')){
				$(this).text($(this).text() + '"');
			}
			if($(this).text() == '"'){
				$(this).text('""');
			}
			managers[$fileIndex].update($divTo.html(), {force: true});
			
		});
		
	}

	function showXml($file){
		clearWorkspace();
		var $translatedFile;
		if($.isNumeric($file)){
			$translatedFile = translatedFiles[$file];
			$file = originalFiles[parseInt($file)];
		}
		else{
			$translatedFile = translatedFiles[originalFiles.indexOf($file)];
		}
		
		if($translatedFile.size < 1){
			$translatedFile = $file;
		}

		var $fileIndex = originalFiles.indexOf($file);

		var $divFrom = $($('.translate_file_work_from').get($fileIndex));
		var $divTo = $($('.translate_file_work_to').get($fileIndex));
		
		$divFrom.addClass("selected");
		$divTo.addClass("selected");
		
		if($divFrom.html() == ""){

			var reader = new FileReader();
			reader.onload = function (e) {
				var data = e.target.result;
				// $previewDiv.find('.translate_file_filePreview').append('<div style="width: 100%; height: 100%; text-align: center;"><img src="'+e.target.result+'" style="height: 100%;"></div>');
				$divFrom.append('<pre><code class="xml">' + $('<html>').text(data).html() + '</code></pre>');
				hljs.highlightBlock($divFrom.find('code').get(0));
				
				// $previewDiv.find('.translate_file_filePreview').css('height','auto');
				// $clamp($previewDiv.find('.translate_file_filePreview')[0], {clamp: '100%'});

			}
			reader.readAsText($file);

			var reader2 = new FileReader();
			reader2.onload = function (e) {
				var data = e.target.result;
				// $previewDiv.find('.translate_file_filePreview').append('<div style="width: 100%; height: 100%; text-align: center;"><img src="'+e.target.result+'" style="height: 100%;"></div>');
				$divTo.append('<pre><code class="xml">' + $('<html>').text(data).html() + '</code></pre>');
				hljs.highlightBlock($divTo.find('code').get(0));
				// var re = />.*<\//g;
				// $divTo.find('code').html($divTo.find('code').html().replace(re, function(a, b){
				// 	return '><span>'+a+'</span></'
				// }));
				$divTo.find('code').attr("contenteditable", "plaintext-only");
				$divTo.find('code span').attr("contenteditable", "false");
				$divTo.find('code .hljs-string').attr("contenteditable", "plaintext-only");
				managers[$fileIndex].reset($divTo.html());
				$divTo.find('code .hljs-string').each(function(i, item){
					var formData = new FormData();
					formData.append("user_email", $userInfomation.user_email);
					formData.append("request_id", $request_infomation.request_id);
					formData.append("sentence", $(item).text());
					formData.append("source_lang_id", $request_infomation.request_originalLang);
					formData.append("target_lang_id", $request_infomation.request_targetLang);

					$.ajax({
						url: '/api/tool/initial_translate',
						data: formData,
						processData: false,
						contentType: false,
						dataType: 'JSON',
						type: 'POST',
						success: function (data) {
							console.log(data);
							$(item).text($("<div />").html(data.cand1).text());
						}
					})
				});
				
				// $previewDiv.find('.translate_file_filePreview').css('height','auto');
				// $clamp($previewDiv.find('.translate_file_filePreview')[0], {clamp: '100%'});

			}
			reader2.readAsText($translatedFile);

		}

		$('.translate_file_work').off('focus','.translate_file_work_to.selected code .hljs-string').on('focus','.translate_file_work_to.selected code .hljs-string', function(e){
			// $(".translate_file_work_from").scrollTop($(".translate_file_work_to").scrollTop() - $(this).position().top + $($('.translate_file_work_from code .hljs-string').get($('.translate_file_work_to code .hljs-string').index($(this)))).position().top);
			
			$(".translate_file_work_from.selected").scrollTop($(".translate_file_work_to.selected").scrollTop());
			$(".translate_file_work_from.selected").scrollTop($(".translate_file_work_to.selected").scrollTop() - $(this).position().top + $($('.translate_file_work_from.selected span').get($('.translate_file_work_to.selected span').index($(this)))).position().top);

			$('.translate_file_work_from.selected code .hljs-string').removeClass('selected');
			$($('.translate_file_work_from.selected code .hljs-string').get($('.translate_file_work_to.selected code .hljs-string').index($(this)))).addClass('selected');
			if (document.selection) document.selection.empty(); 
			else if (window.getSelection) window.getSelection().removeAllRanges();

			if (document.selection) {
				var range = document.body.createTextRange();
				range.moveToElementText(this);
				range.select();
			}
			else if (window.getSelection) {
				var range = document.createRange();
				range.selectNodeContents(this);
				window.getSelection().addRange(range);
			}
		
		});


		$('.translate_file_work').off('keydown').on('keydown', function(e){
			if((e.metaKey || e.ctrlKey) && String.fromCharCode(e.which).toLowerCase() === 'z'){
				e.preventDefault();
				if(managers[$fileIndex].canUndo()){
					managers[$fileIndex].undo();
					$divTo.html(managers[$fileIndex].state);
					// window.getSelection().removeAllRanges();
					// window.getSelection().addRange(managers[$fileIndex].state['cursor']);
					$divTo.find('code .hljs-string').each(function(i, item){
						if(!$(item).text().startsWith('"')){
							$(item).text('"' + $(item).text());
						}
						if(!$(item).text().endsWith('"')){
							$(item).text($(item).text() + '"');
						}
						if($(item).text() == '"'){
							$(item).text('""');
						}
					});
				}
			}
			else if((e.metaKey || e.ctrlKey) && String.fromCharCode(e.which).toLowerCase() === 'y'){
				e.preventDefault();
				if(managers[$fileIndex].canRedo()){
					managers[$fileIndex].redo();
					$divTo.html(managers[$fileIndex].state);
					$divTo.find('code .hljs-string').each(function(i, item){
						if(!$(item).text().startsWith('"')){
							$(item).text('"' + $(item).text());
						}
						if(!$(item).text().endsWith('"')){
							$(item).text($(item).text() + '"');
						}
						if($(item).text() == '"'){
							$(item).text('""');
						}
					});
					// window.getSelection().removeAllRanges();
					// window.getSelection().addRange(managers[$fileIndex].state['cursor']);
				}
			}
		})
		
		$('.translate_file_work').off('keydown','.translate_file_work_to.selected code .hljs-string').on('keydown','.translate_file_work_to.selected code .hljs-string', function(e){
			if(e.which == 13){
				e.preventDefault();
				$('.translate_file_work_to.selected code .hljs-string').get($('.translate_file_work_to.selected code .hljs-string').index($(this))+1).focus();
				// managers[$fileIndex].update($divTo.html(), {force: true});
			}
			else{
				// managers[$fileIndex].update({"html": $divTo.html(), "cursor": window.getSelection().getRangeAt(0)});
				managers[$fileIndex].update($divTo.html());
			}
		});

		$('.translate_file_work').off('focusout','.translate_file_work_to.selected code .hljs-string').on('focusout','.translate_file_work_to.selected code .hljs-string', function(e){
			if(!$(this).text().startsWith('"')){
				$(this).text('"' + $(this).text());
			}
			if(!$(this).text().endsWith('"')){
				$(this).text($(this).text() + '"');
			}
			if($(this).text() == '"'){
				$(this).text('""');
			}
			managers[$fileIndex].update($divTo.html(), {force: true});
			
		});
	}

	function showTxt($file){
		clearWorkspace();
		var $translatedFile;
		if($.isNumeric($file)){
			$translatedFile = translatedFiles[$file];
			$file = originalFiles[parseInt($file)];
		}
		else{
			$translatedFile = translatedFiles[originalFiles.indexOf($file)];
		}
		
		if($translatedFile.size < 1){
			$translatedFile = $file;
		}

		var $fileIndex = originalFiles.indexOf($file);

		var $divFrom = $($('.translate_file_work_from').get($fileIndex));
		var $divTo = $($('.translate_file_work_to').get($fileIndex));
		
		$divFrom.addClass("selected");
		$divTo.addClass("selected");
		
		if($divFrom.html() == ""){

			var reader = new FileReader();
			reader.onload = function (e) {
				var data = e.target.result;
				// $previewDiv.find('.translate_file_filePreview').append('<div style="width: 100%; height: 100%; text-align: center;"><img src="'+e.target.result+'" style="height: 100%;"></div>');
				// data = getSentences(data);
				

				var formData = new FormData();
				// formData.append("user_email", $userInfomation.user_email);
				// formData.append("request_id", $request_infomation.request_id);
				formData.append("sentences", data);
				// formData.append("source_lang_id", $request_infomation.request_originalLang);
				// formData.append("target_lang_id", $request_infomation.request_targetLang);

				$.ajax({
					url: '/api/tool/sentence_tokenize',
					data: formData,
					processData: false,
					contentType: false,
					dataType: 'JSON',
					type: 'POST',
					success: function (data) {
						console.log(data);
						$.each(data.sentences, function(i, item){
							$divFrom.append("<pre><span style='display: block; margin-bottom: 20px;'>"+item+"</span></pre>");
						})
					}
				})
				
				// $previewDiv.find('.translate_file_filePreview').css('height','auto');
				// $clamp($previewDiv.find('.translate_file_filePreview')[0], {clamp: '100%'});

			}
			reader.readAsText($file);

			var reader2 = new FileReader();
			reader2.onload = function (e) {
				// var data = e.target.result;
				var data = e.target.result;
				
				var formData = new FormData();
				// formData.append("user_email", $userInfomation.user_email);
				// formData.append("request_id", $request_infomation.request_id);
				formData.append("sentences", data);
				// formData.append("source_lang_id", $request_infomation.request_originalLang);
				// formData.append("target_lang_id", $request_infomation.request_targetLang);

				$.ajax({
					url: '/api/tool/sentence_tokenize',
					data: formData,
					processData: false,
					contentType: false,
					dataType: 'JSON',
					type: 'POST',
					success: function (data) {
						console.log(data);
						$.each(data.sentences, function(i, item){
							$divTo.append("<pre><span style='display: block; margin-bottom: 20px;' contenteditable='plaintext-only' >"+item+"</span></pre>");
						})

						$.each($divTo.find('span'), function(i, item){
							var formData = new FormData();
							formData.append("user_email", $userInfomation.user_email);
							formData.append("request_id", $request_infomation.request_id);
							formData.append("sentence", $(item).text());
							formData.append("source_lang_id", $request_infomation.request_originalLang);
							formData.append("target_lang_id", $request_infomation.request_targetLang);

							$.ajax({
								url: '/api/tool/initial_translate',
								data: formData,
								processData: false,
								contentType: false,
								dataType: 'JSON',
								type: 'POST',
								success: function (data) {
									console.log(data);
									// $divTo.append("<span>"+data.cand1+"</span> ");
									$(item).text(data.cand1);
								}
							});
							
						})
					}
				})
				// $previewDiv.find('.translate_file_filePreview').append('<div style="width: 100%; height: 100%; text-align: center;"><img src="'+e.target.result+'" style="height: 100%;"></div>');
				
				// $previewDiv.find('.translate_file_filePreview').css('height','auto');
				// $clamp($previewDiv.find('.translate_file_filePreview')[0], {clamp: '100%'});

			}
			reader2.readAsText($translatedFile);

		}

		$('.translate_file_work').off('focus','.translate_file_work_to.selected span').on('focus','.translate_file_work_to.selected span', function(e){
			$(".translate_file_work_from.selected").scrollTop($(".translate_file_work_to.selected").scrollTop());
			$(".translate_file_work_from.selected").scrollTop($(".translate_file_work_to.selected").scrollTop() - $(this).position().top + $($('.translate_file_work_from.selected span').get($('.translate_file_work_to.selected span').index($(this)))).position().top);

			// $(".translate_file_work_to").scrollTop($(this).position().top);
			// $(".translate_file_work_from").scrollTop($($('.translate_file_work_from span').get($('.translate_file_work_to span').index($(this)))).position().top);
			// console.log($(this).offset().top);
			// console.log($($('.translate_file_work_from span').get($('.translate_file_work_to span').index($(this)))).offset().top);

			$('.translate_file_work_from span').removeClass('selected');
			$($('.translate_file_work_from.selected span').get($('.translate_file_work_to.selected span').index($(this)))).addClass('selected');
			if (document.selection) document.selection.empty(); 
			else if (window.getSelection) window.getSelection().removeAllRanges();

			if (document.selection) {
				var range = document.body.createTextRange();
				range.moveToElementText(this);
				range.select();
			}
			else if (window.getSelection) {
				var range = document.createRange();
				range.selectNodeContents(this);
				window.getSelection().addRange(range);
			}
		
		});


		$('.translate_file_work').off('keydown').on('keydown', function(e){
			if((e.metaKey || e.ctrlKey) && String.fromCharCode(e.which).toLowerCase() === 'z'){
				e.preventDefault();
				if(managers[$fileIndex].canUndo()){
					managers[$fileIndex].undo();
					$divTo.html(managers[$fileIndex].state);
					// window.getSelection().removeAllRanges();
					// window.getSelection().addRange(managers[$fileIndex].state['cursor']);
					// $divTo.find('span').each(function(i, item){
					// 	if(!$(item).text().startsWith('"')){
					// 		$(item).text('"' + $(item).text());
					// 	}
					// 	if(!$(item).text().endsWith('"')){
					// 		$(item).text($(item).text() + '"');
					// 	}
					// 	if($(item).text() == '"'){
					// 		$(item).text('""');
					// 	}
					// });
				}
			}
			else if((e.metaKey || e.ctrlKey) && String.fromCharCode(e.which).toLowerCase() === 'y'){
				e.preventDefault();
				if(managers[$fileIndex].canRedo()){
					managers[$fileIndex].redo();
					$divTo.html(managers[$fileIndex].state);
					$divTo.find('span').each(function(i, item){
						if(!$(item).text().startsWith('"')){
							$(item).text('"' + $(item).text());
						}
						if(!$(item).text().endsWith('"')){
							$(item).text($(item).text() + '"');
						}
						if($(item).text() == '"'){
							$(item).text('""');
						}
					});
					// window.getSelection().removeAllRanges();
					// window.getSelection().addRange(managers[$fileIndex].state['cursor']);
				}
			}
		})
		
		$('.translate_file_work').off('keydown','.translate_file_work_to span').on('keydown','.translate_file_work_to span', function(e){
			if(e.which == 13){
				e.preventDefault();
				$('.translate_file_work_to span').get($('.translate_file_work_to span').index($(this))+1).focus();
				// managers[$fileIndex].update($divTo.html(), {force: true});
			}
			else{
				// managers[$fileIndex].update({"html": $divTo.html(), "cursor": window.getSelection().getRangeAt(0)});
				managers[$fileIndex].update($divTo.html());
			}
		});

		$('.translate_file_work').off('focusout','.translate_file_work_to span').on('focusout','.translate_file_work_to span', function(e){
			// if(!$(this).text().startsWith('"')){
			// 	$(this).text('"' + $(this).text());
			// }
			// if(!$(this).text().endsWith('"')){
			// 	$(this).text($(this).text() + '"');
			// }
			// if($(this).text() == '"'){
			// 	$(this).text('""');
			// }
			$(".translate_file_work_from.selected span").removeClass("selected");
			$(".translate_file_work_to.selected span").removeClass("selected");
			managers[$fileIndex].update($divTo.html(), {force: true});
		});
		
	}


	function getSentences(txt){
		// this.sentences = this.entry.split(/[\.!]\s/);
		var words = txt.replace(/(?:\r\n|\r|\n)/g, ' ').split(' ');
		var endingWords = words.filter(function(w) {
			return w.endsWith('.')||w.endsWith('!')||w.endsWith('?')||w.endsWith('\n')||w.endsWith('↵');
			

		});

		var botnameRegExp = new RegExp("\\W?" + "this.botname.normalize()" + "\\W?");
		var usernameRegExp = new RegExp("\\W?" + "this.username.normalize()" + "\\W?");
		var lastSentence = "";
		var sentences = [];
		$.each(words, function(i, cur){
			var curNormalized = cur.normalize();
			var curReplaced = cur;
			if (curNormalized.search(botnameRegExp) !== -1) {
				curReplaced = cur.replace("this.botname","{yourname}");
			}
			else if (curNormalized.search(usernameRegExp) !== -1) {
				curReplaced = cur.replace("this.username","{myname}");
			}

			if (endingWords.indexOf(words[words.indexOf(cur) - 1]||-1) != -1) {
				sentences.push($.trim(lastSentence));
				lastSentence = "";
			}
			lastSentence = $.trim(lastSentence + " " + curReplaced);
			return $.trim(cur);

		})

		// words.reduce(function (prev, cur, index, array) {
		// 	var curNormalized = cur.normalize();
		// 	var curReplaced = cur;
		// 	if (curNormalized.search(botnameRegExp) !== -1) {
		// 		curReplaced = cur.replace("this.botname","{yourname}");
		// 	}
		// 	else if (curNormalized.search(usernameRegExp) !== -1) {
		// 		curReplaced = cur.replace("this.username","{myname}");
		// 	}

		// 	if (endingWords.indexOf(prev) != -1) {
		// 		sentences.push(lastSentence);
		// 		lastSentence = "";
		// 	}
		// 	lastSentence = lastSentence + " " + curReplaced;
		// 	return cur;
		// });
		sentences.push(lastSentence);
		console.log(sentences);
		return sentences;
	}
	// console.log(getSentences("Mr. Kim Said 'hello.'"))
});