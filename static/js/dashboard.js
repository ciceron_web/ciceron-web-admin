
$(document).ready(function(){
    i18n.setLanguage($.cookie($userInfomation.user_email + "translate"));
    var isEdit = false;
    
    $("body").on("click", ".dashboard-item", function(e){
        showTicket($(this).attr("value"));
    });
    
    $.ajax({
        url: $apiURL + '/complete/groups',
        processData: false,
        contentType: false,
        cache : false,
        dataType: "JSON",
        type: 'GET',
        error: function(xhr, ajaxOptions, thrownError){
            // if (xhr.status == 417) {
            //     alert("올바르지 않은 이메일 주소입니다.");
            // }
            // else if(xhr.status == 412){
            //     alert("이미 존재하는 이메일 주소입니다.");
            // }
            // alert("에러!");
        }
    }).done(function(data){
        $.ajax({
            url: $apiURL + '/complete/groups/' + data.data[0].id,
            processData: false,
            contentType: false,
            cache : false,
            dataType: "JSON",
            type: 'GET',
            error: function(xhr, ajaxOptions, thrownError){
                // if (xhr.status == 417) {
                //     alert("올바르지 않은 이메일 주소입니다.");
                // }
                // else if(xhr.status == 412){
                //     alert("이미 존재하는 이메일 주소입니다.");
                // }
                // alert("에러!");
            }
        }).done(function(data){
            $.each(data.data, function(i, item){
                $(".dashboard-completed").append('<div class="dashboard-item x2" value="'+item.request_id+'">'
                        +'<div class="dashboard-item-completed-language">'
                            +'<img src="/static/img/flags/round/'+item.request_originalLang+'.png">'
                            +'<img src="/static/img/flags/round/arrow.png" style="width: 10px; height: 10px;margin-bottom: 10px;">'
                            +'<img src="/static/img/flags/round/'+item.request_targetLang+'.png">'
                            +'<br />'
                            +'<img src="/static/img/format/'+item.request_format+'.png">'
                            +'<img src="/static/img/flags/round/arrow.png" style="visibility: collapse; width: 10px; height: 10px;margin-bottom: 10px;">'
                            +'<img src="/static/img/subject/'+item.request_subject+'.png">'
                        +'</div>'
                        +'<span class="dashboard-item-completed-text">'
                            +item.request_context
                        +'</span>'
                    +'</div>');
            });

        });
        
    });
    

    $.ajax({
        url: $apiURL + '/incomplete?page=1',
        processData: false,
        contentType: false,
        cache : false,
        dataType: "JSON",
        type: 'GET',
        error: function(xhr, ajaxOptions, thrownError){
            // if (xhr.status == 417) {
            //     alert("올바르지 않은 이메일 주소입니다.");
            // }
            // else if(xhr.status == 412){
            //     alert("이미 존재하는 이메일 주소입니다.");
            // }
            // alert("에러!");
        }
    }).done(function(data){
        $.each(data.data, function(i, item){
            $(".dashboard-status").append('<div class="dashboard-item" value="'+item.request_id+'">'
                        +'<div class="dashboard-item-translator" style="background-image: url(/api/access_file/'+item.request_translatorPicPath+')">'
                            +'<span class="dashboard-item-percent">'+item.request_translatorName+'</span>'
                        +'</div>'
                        +'<span class="dashboard-item-text">'
                            +item.request_context
                        +'</span>'
                        +'<div class="dashboard-item-language">'
                            +'<img src="/static/img/flags/round/'+item.request_originalLang+'.png">'
                            +'<img src="/static/img/flags/round/arrow.png" style="width: 10px; height: 10px;margin-bottom: 10px;">'
                            +'<img src="/static/img/flags/round/'+item.request_targetLang+'.png">'
                            +'<img src="/static/img/format/'+item.request_format+'.png">'
                            +'<img src="/static/img/subject/'+item.request_subject+'.png">'
                        +'</div>'
                    +'</div>');
        });
    });
    
    
});

