var express = require('express');
var router = express.Router();
var proxy = require('express-http-proxy');
var request = require('request');
var fs = require('fs');
var urlencode = require('urlencode');
var path = require('path');


var htmlConvert = require('html-convert');
var convert = htmlConvert({
	tmp: path.join(__dirname, '../temp'),
	pool: 1000,
	maxErrors: 10,
	phantomFlags: ['--ignore-ssl-errors=true']
});

setTimeout(() => {
	convert('http://example.com', {
		width: 600,
		height: 300,
		format: "jpeg",
	});
}, 5000);


var adminOnly = function (req, res, next) {
	request({
		url: req.protocol + '://' + req.get('host') + '/api/v2',
		// url: req.protocol + '://secure.c.i' + '/api/user/profile',
		headers: req.headers
	}, function (err, res2, body) {
		// console.log(body);
		// body = JSON.parse(body);
		body = JSON.parse(body);
		if (body.isAdmin) {
			next();
		}
		else {
			res.redirect('/admin/login');
		}
	});
}


/* GET home page. */
// router.get('/', function(req, res, next) {
//   viewFile(req, res, 'main2');
// });

// router.get('/request', function(req, res, next) {
//   viewFile(req, res, 'request');
// });

// router.get('/request/languages', function(req, res, next) {
//   viewFile(req, res, 'request-languages');
// });

// router.get('/', function (req, res, next) {
// 		res.end('index');
// });

router.get('/terms', function (req, res, next) {
	fs.readFile("./static" + "/terms.html", "utf8", function (err, file) {
		res.writeHead(200, {
			"Content-Type": "text/html;charset=UTF-8"
		});
		res.write(file, "utf8");
		res.end();
	});
});
router.get('/privacy', function (req, res, next) {
	fs.readFile("./static" + "/privacy.html", "utf8", function (err, file) {
		res.writeHead(200, {
			"Content-Type": "text/html;charset=UTF-8"
		});
		res.write(file, "utf8");
		res.end();
	});
});

// router.get('/stoa', function (req, res, next) {
// 	fs.readFile("./static" + "/stoa.1.html", "utf8", function (err, file) {
// 		res.writeHead(200, {
// 			"Content-Type": "text/html;charset=UTF-8"
// 		});
// 		res.write(file, "utf8");
// 		res.end();
// 	});
// });

// router.get('/translate', function (req, res, next) {
// 	fs.readFile("./static" + "/translate.html", "utf8", function (err, file) {
// 		res.writeHead(200, {
// 			"Content-Type": "text/html;charset=UTF-8"
// 		});
// 		res.write(file, "utf8");
// 		res.end();
// 	});
// });

// router.get('/translate_i18n', function (req, res, next) {
// 	fs.readFile("./static" + "/translate_i18n.html", "utf8", function (err, file) {
// 		res.writeHead(200, {
// 			"Content-Type": "text/html;charset=UTF-8"
// 		});
// 		res.write(file, "utf8");
// 		res.end();
// 	});
// });

// router.get('/teaser', function (req, res, next) {
// 	fs.readFile("./static" + "/index.html", "utf8", function (err, file) {
// 		res.writeHead(200, {
// 			"Content-Type": "text/html;charset=UTF-8"
// 		});
// 		res.write(file, "utf8");
// 		res.end();
// 	});
// });

// router.get('/stoa/:id', function (req, res, next) {
// 	fs.readFile("./static" + "/stoa.html", "utf8", function (err, file) {
// 		res.writeHead(200, {
// 			"Content-Type": "text/html;charset=UTF-8"
// 		});
// 		res.write(file, "utf8");
// 		res.end();
// 	});
// });

// /*router.get('/about', function (req, res, next) {
// 	fs.readFile("./static" + "/about.2.html", "utf8", function (err, file) {
// 		res.writeHead(200, {
// 			"Content-Type": "text/html;charset=UTF-8"
// 		});
// 		res.write(file, "utf8");
// 		res.end();
// 	});
// });*/

// router.get('/apply', function (req, res, next) {
// 	fs.readFile("./static" + "/apply.html", "utf8", function (err, file) {
// 		res.writeHead(200, {
// 			"Content-Type": "text/html;charset=UTF-8"
// 		});
// 		res.write(file, "utf8");
// 		res.end();
// 	});
// });

// router.get('/notification', function (req, res, next) {
// 	fs.readFile("./static" + "/notification.html", "utf8", function (err, file) {
// 		res.writeHead(200, {
// 			"Content-Type": "text/html;charset=UTF-8"
// 		});
// 		res.write(file, "utf8");
// 		res.end();
// 	});
// });

// router.get('/profile', function (req, res, next) {
// 	fs.readFile("./static" + "/profile.html", "utf8", function (err, file) {
// 		res.writeHead(200, {
// 			"Content-Type": "text/html;charset=UTF-8"
// 		});
// 		res.write(file, "utf8");
// 		res.end();
// 	});
// });

// router.get('/makerequest', function (req, res, next) {
// 	fs.readFile("./static" + "/makerequest.html", "utf8", function (err, file) {
// 		res.writeHead(200, {
// 			"Content-Type": "text/html;charset=UTF-8"
// 		});
// 		res.write(file, "utf8");
// 		res.end();
// 	});
// });


// router.get('/ticket/:id', function (req, res, next) {
// 	fs.readFile("./static" + "/ticket.html", "utf8", function (err, file) {
// 		res.writeHead(200, {
// 			"Content-Type": "text/html;charset=UTF-8"
// 		});
// 		res.write(file, "utf8");
// 		res.end();
// 	});
// });

// router.get('/request', function (req, res, next) {
// 	fs.readFile("./static" + "/request.html", "utf8", function (err, file) {
// 		res.writeHead(200, {
// 			"Content-Type": "text/html;charset=UTF-8"
// 		});
// 		res.write(file, "utf8");
// 		res.end();
// 	});
// });

// router.get('/dashboard', function (req, res, next) {
// 	fs.readFile("./static" + "/dashboard.html", "utf8", function (err, file) {
// 		res.writeHead(200, {
// 			"Content-Type": "text/html;charset=UTF-8"
// 		});
// 		res.write(file, "utf8");
// 		res.end();
// 	});
// });

// router.get('/mypage', function (req, res, next) {
// 	fs.readFile("./static" + "/mypage.html", "utf8", function (err, file) {
// 		res.writeHead(200, {
// 			"Content-Type": "text/html;charset=UTF-8"
// 		});
// 		res.write(file, "utf8");
// 		res.end();
// 	});
// });

// //

// router.get('/status', function (req, res, next) {
// 	fs.readFile("./static" + "/status.html", "utf8", function (err, file) {
// 		res.writeHead(200, {
// 			"Content-Type": "text/html;charset=UTF-8"
// 		});
// 		res.write(file, "utf8");
// 		res.end();
// 	});
// });

// router.get('/completed', function (req, res, next) {
// 	fs.readFile("./static" + "/completed.html", "utf8", function (err, file) {
// 		res.writeHead(200, {
// 			"Content-Type": "text/html;charset=UTF-8"
// 		});
// 		res.write(file, "utf8");
// 		res.end();
// 	});
// });

// router.get('/processingrequests', function (req, res, next) {
// 	fs.readFile("./static" + "/processingrequests.html", "utf8", function (err, file) {
// 		res.writeHead(200, {
// 			"Content-Type": "text/html;charset=UTF-8"
// 		});
// 		res.write(file, "utf8");
// 		res.end();
// 	});
// });

// router.get('/processingrequests/:id', function (req, res, next) {
// 	fs.readFile("./static" + "/processingrequests.html", "utf8", function (err, file) {
// 		res.writeHead(200, {
// 			"Content-Type": "text/html;charset=UTF-8"
// 		});
// 		res.write(file, "utf8");
// 		res.end();
// 	});
// });

// router.get('/cart', function (req, res, next) {
// 	fs.readFile("./static" + "/cart.html", "utf8", function (err, file) {
// 		res.writeHead(200, {
// 			"Content-Type": "text/html;charset=UTF-8"
// 		});
// 		res.write(file, "utf8");
// 		res.end();
// 	});
// });

// router.get('/translating', function (req, res, next) {
// 	fs.readFile("./static" + "/translating.html", "utf8", function (err, file) {
// 		res.writeHead(200, {
// 			"Content-Type": "text/html;charset=UTF-8"
// 		});
// 		res.write(file, "utf8");
// 		res.end();
// 	});
// });

// router.get('/translating/:id', function (req, res, next) {
// 	fs.readFile("./static" + "/translating.html", "utf8", function (err, file) {
// 		res.writeHead(200, {
// 			"Content-Type": "text/html;charset=UTF-8"
// 		});
// 		res.write(file, "utf8");
// 		res.end();
// 	});
// });

// router.get('/donerequests', function (req, res, next) {
// 	fs.readFile("./static" + "/donerequests.html", "utf8", function (err, file) {
// 		res.writeHead(200, {
// 			"Content-Type": "text/html;charset=UTF-8"
// 		});
// 		res.write(file, "utf8");
// 		res.end();
// 	});
// });

// router.get('/donerequests/:id', function (req, res, next) {
// 	fs.readFile("./static" + "/donerequests.html", "utf8", function (err, file) {
// 		res.writeHead(200, {
// 			"Content-Type": "text/html;charset=UTF-8"
// 		});
// 		res.write(file, "utf8");
// 		res.end();
// 	});
// });

// router.get('/activity', function (req, res, next) {
// 	fs.readFile("./static" + "/activity.html", "utf8", function (err, file) {
// 		res.writeHead(200, {
// 			"Content-Type": "text/html;charset=UTF-8"
// 		});
// 		res.write(file, "utf8");
// 		res.end();
// 	});
// });

// router.get('/activity/:id', function (req, res, next) {
// 	fs.readFile("./static" + "/activity.html", "utf8", function (err, file) {
// 		res.writeHead(200, {
// 			"Content-Type": "text/html;charset=UTF-8"
// 		});
// 		res.write(file, "utf8");
// 		res.end();
// 	});
// });

// router.get('/landing', function (req, res, next) {
// 	fs.readFile("./static" + "/landing.html", "utf8", function (err, file) {
// 		res.writeHead(200, {
// 			"Content-Type": "text/html;charset=UTF-8"
// 		});
// 		res.write(file, "utf8");
// 		res.end();
// 	});
// });

// 번역기 뷰 
router.get('/ai1', function (req, res, next) {
	viewFile(req, res, 'transView1');
});
router.get('/ai2', function (req, res, next) {
	viewFile(req, res, 'transView2');
});
router.get('/ai/login', function (req, res, next) {
	viewFile(req, res, 'transView_login');
});
router.get('/ai/signUp', function (req, res, next) {
	viewFile(req, res, 'transView_signUp');
});
router.get('/ai/myInfo_find', function (req, res, next) {
	viewFile(req, res, 'transView_myInfo_find');
});



router.get('/file/:id', function (req, res, next) {
	request({
		url: req.protocol + '://' + req.get('host') + '/api/v2/user/pretranslated/project/' + req.params.id + '/resource',
		// url: req.protocol + '://secure.c.i' + '/api/user/profile',
		headers: req.headers
	}, function (err, res2, body) {
		// console.log(body);
		body = JSON.parse(body);

		try {
			res.redirect(301, '/file/' + req.params.id + "/" + body.data[0].target_language);
		} catch (error) {
			next(error);
			// res.render("www/" + filename, { title: 'CICERON', filename: filename, user: null, url: urlencode(fullUrl), req: req, param });
		}
	});

});


router.get('/translated/old/:id/:id2', function (req, res, next) {
	// var fileList = [
	// 	{
	// 		name:"샤오미 전동 스쿠터 매뉴얼",
	// 		description:"XIAOMI ELECTRIC SCOOTER"
	// 	},{
	// 		name:"中国历朝代服饰",
	// 		description: "CHINESE DYNASTY DRESS"
	// 	}
	// ]

	request({
		url: req.protocol + '://' + req.get('host') + '/api/v2/user/pretranslated/project/' + req.params.id + '/resource',
		// url: req.protocol + '://secure.c.i' + '/api/user/profile',
		headers: req.headers
	}, function (err, res2, body) {
		// console.log(body);
		try {

			body = JSON.parse(body);

			body.data.filter(trans => trans.id == req.params.id2).map(function (trans) {
				var previews = trans.resource_info.filter(resource => resource.preview_permission == 0 || resource.preview_permission == 2).map(function (file) {
					file.file_url = file.file_url.split('/').pop();
					return file;
				});
				viewFile(req, res, 'file', { file: { name: trans.theme, description: trans.description, date: new Date(trans.register_timestamp).toLocaleDateString(), time: new Date(trans.register_timestamp).toLocaleTimeString(), project_id: req.params.id, language_id: trans.id, preview_count: previews.length, cover: body.cover_photo_url, previewFileList: previews } });
			});
			return;
		}
		catch (e) {
			next();
		}
	});

});

router.get('/nepick/test/:id/:id2', function (req, res, next) {

	request({
		url: req.protocol + '://' + req.get('host') + '/api/v2/user/pretranslated/project/' + req.params.id + '/resource',
		// url: req.protocol + '://secure.c.i' + '/api/user/profile',
		headers: req.headers
	}, function (err, res2, body) {
		// console.log(body);
		try {

			body = JSON.parse(body);

			body.data.filter(trans => trans.id == req.params.id2).map(function (trans) {
				var previews = trans.resource_info.filter(resource => resource.preview_permission == 0 || resource.preview_permission == 2).map(function (file) {
					file.file_url = file.file_url.split('/').pop();
					return file;
				});
				viewFile(req, res, 'nepick/test/newsView', { file: { name: trans.theme, description: trans.description, date: new Date(trans.register_timestamp).toLocaleDateString(), time: new Date(trans.register_timestamp).toLocaleTimeString(), project_id: req.params.id, language_id: trans.id, preview_count: previews.length, cover: body.cover_photo_url, previewFileList: previews } });
			});
			return;
		}
		catch (e) {
			next();
		}
	});

});


router.get('/translated/:id/:id2', function (req, res, next) {

	request({
		url: req.protocol + '://' + req.get('host') + '/api/v2/user/pretranslated/project/' + req.params.id + '/resource',
		// url: req.protocol + '://secure.c.i' + '/api/user/profile',
		headers: req.headers
	}, function (err, res2, body) {
		// console.log(body);
		try {

			body = JSON.parse(body);

			body.data.filter(trans => trans.id == req.params.id2).map(function (trans) {
				var previews = trans.resource_info.filter(resource => resource.preview_permission == 0 || resource.preview_permission == 2).map(function (file) {
					file.file_url = file.file_url.split('/').pop();
					return file;
				});
				viewFile(req, res, 'nepick/newsView', { file: { name: trans.theme, description: trans.description, date: new Date(trans.register_timestamp).toLocaleDateString(), time: new Date(trans.register_timestamp).toUTCString(), project_id: req.params.id, resource_id: trans.id, language_id: trans.target_language_id, preview_count: previews.length, cover: body.cover_photo_url, previewFileList: previews, author: body.author, source: body.quote_url } });
			});
			return;
		}
		catch (e) {
			next();
		}
	});

});


router.get('/translated/:id/:id2/cover', function (req, res, next) {

	request({
		url: req.protocol + '://' + req.get('host') + '/api/v2/user/pretranslated/project/' + req.params.id + '/resource',
		// url: req.protocol + '://secure.c.i' + '/api/user/profile',
		headers: req.headers
	}, function (err, res2, body) {
		// console.log(body);
		try {

			body = JSON.parse(body);

			body.data.filter(trans => trans.id == req.params.id2).map(function (trans) {
				var previews = trans.resource_info.filter(resource => resource.preview_permission == 0 || resource.preview_permission == 2).map(function (file) {
					file.file_url = file.file_url.split('/').pop();
					return file;
				});
				viewFile(req, res, 'nepick/renderCover', { file: { name: trans.theme, description: trans.description, date: new Date(trans.register_timestamp).toLocaleDateString(), time: new Date(trans.register_timestamp).toLocaleTimeString(), project_id: req.params.id, resource_id: trans.id, language_id: trans.target_language_id, preview_count: previews.length, cover: body.cover_photo_url, previewFileList: previews, author: body.author, source: body.quote_url }, skipProfile: true });
			});
			return;
		}
		catch (e) {
			next();
		}
	});

});

router.get('/translated/:id/:id2/cover.jpg', function (req, res, next) {

	console.log("start");


	request({
		url: req.protocol + '://' + req.get('host') + '/api/v2/user/pretranslated/project/' + req.params.id + '/resource',
		// url: req.protocol + '://secure.c.i' + '/api/user/profile',
		headers: req.headers
	}, function (err, res2, body) {
		// console.log(body);
		try {

			body = JSON.parse(body);

			body.data.filter(trans => trans.id == req.params.id2).map(function (trans) {
				res.redirect(body.cover_photo_url);
				return;
			});
			next();
		}
		catch (e) {
			next();
		}
	});
	// try {
	// 	console.log("try");

	// 	res.writeHead(200, { 'Content-Type': 'image/jpeg' });
	// 	console.log("convert");
	// 	convert(req.protocol + '://' + req.get('host') + '/translated/' + req.params.id + '/' + req.params.id2 + '/cover', {
	// 		width: 600,
	// 		height: 300,
	// 		tmp: path.join(__dirname, 'temp'),
	// 		format: "jpeg",
	// 		pool: 10,
	// 		maxErrors: 10,
	// 		phantomFlags: ['--ignore-ssl-errors=true']
	// 	}).pipe(res);
	// 	console.log("pipe");

	// } catch (error) {
	// 	request({
	// 		url: req.protocol + '://' + req.get('host') + '/api/v2/user/pretranslated/project/' + req.params.id + '/resource',
	// 		// url: req.protocol + '://secure.c.i' + '/api/user/profile',
	// 		headers: req.headers
	// 	}, function (err, res2, body) {
	// 		// console.log(body);
	// 		try {

	// 			body = JSON.parse(body);

	// 			body.data.filter(trans => trans.id == req.params.id2).map(function (trans) {
	// 				res.redirect(body.cover_photo_url);
	// 			});
	// 			return;
	// 		}
	// 		catch (e) {
	// 			next();
	// 		}
	// 	});
	// }
	// }).pipe(fs.createWriteStream('out.jpg')).pipe(res);
})

router.get('/translated/:id/curation/:language_id', function (req, res, next) {

	request({
		url: req.protocol + '://' + req.get('host') + '/api/v2/user/pretranslated/project/' + req.params.id + '/curation/' + req.params.language_id,
		// url: req.protocol + '://secure.c.i' + '/api/user/profile',
		headers: req.headers
	}, function (err, res2, body) {
		// console.log(body);
		try {

			body = JSON.parse(body);
			body.data = body.data ? body.data : [];


			res.json(body.data.map(project => (
				{
					theme: project.data[0].theme,
					description: project.data[0].description,
					cover: project.cover_photo_url,
					project_id: project.id,
					resource_id: project.data[0].id
				})
			));
			return;
		}
		catch (e) {
			next();
		}
	});
});


router.get('/file/:id/lang/:id2/preview/:page', function (req, res, next) {



	request({
		url: req.protocol + '://' + req.get('host') + '/api/v2/user/pretranslated/project/' + req.params.id + '/resource',
		// url: req.protocol + '://secure.c.i' + '/api/user/profile',
		headers: req.headers
	}, function (err, res2, body) {
		// console.log(body.data);
		// console.log(Object.keys(body.data).length);
		try {
			body = JSON.parse(body);
			body.data.filter(trans => trans.id == req.params.id2).map(function (trans) {
				var f = trans.resource_info.filter(resource => resource.id == req.params.page && (resource.preview_permission == 0));
				// var f = trans.resource_info.filter(resource => resource.id == req.params.page && (resource.preview_permission == 0 || resource.preview_permission == 2));
				// var preview = trans.resource_info.filter(resource => resource.preview_permission == 0 || resource.preview_permission == 2);
				request({
					url: req.protocol + '://' + req.get('host') + f[0].file_url,
					headers: req.headers
				}).pipe(res);
			});
			return;

		} catch (error) {
			// console.log(error);
			next();
			// res.render("www/" + filename, { title: 'CICERON', filename: filename, user: null, url: urlencode(fullUrl), req: req, param });
		}
	});


	//   var mime = ".jpg";

	//   if(fs.existsSync("views/www/file/" + req.params.id + "/" + req.params.page + ".jpg")){
	//     mime = ".jpg";
	//   }
	//   else if(fs.existsSync("views/www/file/" + req.params.id + "/" + req.params.page + ".png")){
	//     mime = ".png";
	//   }
	//   else if(fs.existsSync("views/www/file/" + req.params.id + "/" + req.params.page + ".gif")){
	//     mime = ".gif";
	//   }

	//   try {

	//     if(parseInt(req.params.page) < 5 || req.params.page == "cover" ){
	//       res.sendfile("views/www/file/" + req.params.id + "/" + req.params.page + mime);
	//       return;
	//     }

	//     if(fs.existsSync("views/www/file/" + req.params.id + "/preview/" + req.params.page + mime)){
	//       res.sendfile("views/www/file/" + req.params.id + "/preview/" + req.params.page + mime);

	//     }
	//     else{
	//       var Jimp = require("jimp");

	//       Jimp.read("views/www/file/" + req.params.id + "/" + req.params.page + mime, function (err, lenna) {
	//         // if (err) throw err;
	//         if (!err){
	//           lenna.blur(7).write("views/www/file/" + req.params.id + "/preview/" + req.params.page + mime, function(){
	//             res.sendfile("views/www/file/" + req.params.id + "/preview/" + req.params.page + mime);
	//           }); // save
	//           // lenna.blur(3).getBuffer(Jim.MIME_JPEG,  // save 


	//         }
	//         else{
	//           next(err);
	//         }
	//       });

	//     }

	//   } catch (error) {
	//     next(error);

	//   }
});

router.get('/admin/attend', function (req, res, next) {


	var gsjson = require('google-spreadsheet-to-json');

	gsjson({
		spreadsheetId: '1KEW5CwxZ4_z4VoWERyJsQYg8F_FyR5idY1z1BMGbLkw',
		// other options... 
	})
		.then(function (result) {
			// console.log(result);
			viewFile(req, res, 'attend', { data1: result });

		})
		.catch(function (err) {
			console.log(err.message);
			console.log(err.stack);
			next(error);
		});



});
router.get('/admin/attend/in', function (req, res, next) {
	res.writeHead(302, { 'Location': 'https://docs.google.com/a/ciceron.me/forms/d/e/1FAIpQLSekHLQEjm-A_EaG7w_H4wm2rMcLKqp884V20taPmmLf07i0Xw/viewform?entry.749571135=' + new Date().format("yyyy-MM-dd") + '&entry.1992406128=%EC%B6%9C%EA%B7%BC&entry.1130475430=10:00' });
	res.end();
});
router.get('/admin/attend/out', function (req, res, next) {
	res.writeHead(302, { 'Location': 'https://docs.google.com/a/ciceron.me/forms/d/e/1FAIpQLSekHLQEjm-A_EaG7w_H4wm2rMcLKqp884V20taPmmLf07i0Xw/viewform?entry.749571135=' + new Date().format("yyyy-MM-dd") + '&entry.1992406128=%ED%87%B4%EA%B7%BC(%EC%A1%B0%ED%87%B4)&entry.1130475430=18:00' });
	res.end();
});

Date.prototype.format = function (f) {
	if (!this.valueOf()) return " ";

	var weekName = ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"];
	var d = this;



	return f.replace(/(yyyy|yy|MM|dd|E|hh|mm|ss|a\/p)/gi, function ($1) {
		switch ($1) {
			case "yyyy": return d.getFullYear();
			case "yy": return (d.getFullYear() % 1000).zf(2);
			case "MM": return (d.getMonth() + 1).zf(2);
			case "dd": return d.getDate().zf(2);
			case "E": return weekName[d.getDay()];
			case "HH": return d.getHours().zf(2);
			case "hh": return ((h = d.getHours() % 12) ? h : 12).zf(2);
			case "mm": return d.getMinutes().zf(2);
			case "ss": return d.getSeconds().zf(2);
			case "a/p": return d.getHours() < 12 ? "오전" : "오후";
			default: return $1;
		}
	});
};

String.prototype.string = function (len) { var s = '', i = 0; while (i++ < len) { s += this; } return s; };
String.prototype.zf = function (len) { return "0".string(len - this.length) + this; };
Number.prototype.zf = function (len) { return this.toString().zf(len); };




router.get('/admin', function (req, res, next) {
	res.redirect('/admin/main');
});

router.get('/admin/main', adminOnly, function (req, res, next) {
	res.render('www/admin/admin_main', { title: "CICERON" });
});

router.get('/admin/login', function (req, res, next) {
	res.render('www/admin/admin_login', { title: "CICERON" });
});

router.get('/admin/commis', adminOnly, function (req, res, next) {
	res.render('www/admin/admin_commis', { title: "CICERON" });
});

router.get('/admin/userM', adminOnly, function (req, res, next) {
	res.render('www/admin/admin_userM', { title: "CICERON" });
});

router.get('/admin/userM/requester', adminOnly, function (req, res, next) {
	res.render('www/admin/admin_userM_requester', { title: "CICERON" });
});

router.get('/admin/userM/translator', adminOnly, function (req, res, next) {
	res.render('www/admin/admin_userM_translator', { title: "CICERON" });
});

router.get('/admin/userM/admin', adminOnly, function (req, res, next) {
	res.render('www/admin/admin_userM_admin', { title: "CICERON" });
});
router.get('/admin/attend/rss', function (req, res, next) {
	var Feed = require('feed');
	var feed = new Feed({
		title: 'Feed Title',
		description: 'This is my personnal feed!',
		id: 'http://example.com/',
		link: 'http://example.com/',
		image: 'http://example.com/image.png',
		copyright: 'All rights reserved 2013, John Doe',
		updated: new Date(2013, 06, 14), // optional, default = today 

		author: {
			name: 'John Doe',
			email: 'johndoe@example.com',
			link: 'https://example.com/johndoe'
		}
	});
	feed.addItem({
		title: new Date(),
		id: new Date(),
		link: new Date(),
		description: "posts[key].description",
		author: [{
			name: 'Jane Doe',
			email: 'janedoe@example.com',
			link: 'https://example.com/janedoe'
		}, {
			name: 'Joe Smith',
			email: 'joesmith@example.com',
			link: 'https://example.com/joesmith'
		}],
		contributor: [{
			name: 'Shawn Kemp',
			email: 'shawnkemp@example.com',
			link: 'https://example.com/shawnkemp'
		}, {
			name: 'Reggie Miller',
			email: 'reggiemiller@example.com',
			link: 'https://example.com/reggiemiller'
		}],
		date: new Date()
	});
	res.set('Content-Type', 'application/rss+xml');
	res.send(feed.render('rss-2.0'));
	// viewFile(req, res, 'attendRss');
})

router.get('/admin/tagData', adminOnly, function (req, res, next) {
	res.render('www/admin/admin_tagData', { title: "CICERON" });
});

router.get('/admin/photoData', adminOnly, function (req, res, next) {
	res.render('www/admin/admin_photoData', { title: "CICERON" });
});

router.get('/admin/pretranslated', adminOnly, function (req, res, next) {
	res.render('www/admin/admin_pretranslated_projectList', { title: "CICERON" });
});

router.get('/admin/pretranslated/:pid', adminOnly, function (req, res, next) {
	res.render('www/admin/admin_pretranslated_resourceList', { title: "CICERON", project_id: parseInt(req.params.pid) });
});

router.get('/admin/pretranslated/:pid/:rid', adminOnly, function (req, res, next) {
	res.render('www/admin/admin_pretranslated_fileList', { title: "CICERON", project_id: parseInt(req.params.pid), resource_id: parseInt(req.params.rid) });
});

router.get('/admin/projectG', adminOnly, function (req, res, next) {
	res.redirect('/admin/pretranslated');
	// res.render('www/admin/admin_projectG', { title: "CICERON" });
});

router.get('/admin/projectG/modify', adminOnly, function (req, res, next) {
	res.render('www/admin/admin_projectG_modify', { title: "CICERON" });
});

router.get('/admin/projectG_resource', adminOnly, function (req, res, next) {
	res.render('www/admin/admin_projectG_resource', { title: "CICERON" });
});

router.get('/admin/projectG_resource/view', adminOnly, function (req, res, next) {
	res.render('www/admin/admin_projectG_resource_view', { title: "CICERON" });
});

router.get('/admin/projectG_resource/put', adminOnly, function (req, res, next) {
	res.render('www/admin/admin_projectG_resource_put', { title: "CICERON" });
});

router.get('/admin/DBsubmit', adminOnly, function (req, res, next) {
	res.render('www/admin/admin_DBsubmit', { title: "CICERON" });
});

router.get('/admin/DBexport', adminOnly, function (req, res, next) {
	res.render('www/admin/admin_DBexport', { title: "CICERON" });
});





/*  b2b ciceron page   */

router.get('/', function (req, res, next) {
	viewFile(req, res, 'new_ciceron/main_ciceron');
});

/* b2c ciceron page */

router.get('/product', function (req, res, next) {
	viewFile(req, res, 'main_renewal/main_renewal');
});


// router.get('/docs/:id', function(req, res, next) {

//  var fs = require('fs');
//  fs.readFile("views/docs/" + req.params.id + ".html", "utf8", function(err, data){
//     if(err){
//       console.log("error!!!");
//       res.send(404);
//     }
//     else{
//       viewFile(req, res, 'docs', {docs: data});

//     }
//   });

// });


router.get('/div/:id', function (req, res, next) {

	viewFile(req, res, 'div/' + req.params.id);

});


var viewFile = function (req, res, filename, param) {
	var fullUrl = req.protocol + '://' + req.get('host') + req.path;
	param = param ? param : {};
	param.skipProfile = param.skipProfile ? param.skipProfile : false;
	if (!param.skipProfile) {
		request({
			url: req.protocol + '://' + req.get('host') + '/api/v2/user/profile',
			// url: req.protocol + '://secure.c.i' + '/api/user/profile',
			headers: req.headers
		}, function (err, res2, body) {
			// console.log(body);

			try {
				// console.log(JSON.parse(body));
				res.render("www/" + filename, { title: 'CICERON', filename: filename, user: JSON.parse(body), url: fullUrl, encodedUrl: urlencode(fullUrl), req: req, param });

			} catch (error) {
				res.render("www/" + filename, { title: 'CICERON', filename: filename, user: null, url: fullUrl, encodedUrl: urlencode(fullUrl), req: req, param });
			}
		});
		//   // console.log(req);
		// console.log(req.headers);
	}
	else {
		res.render("www/" + filename, { title: 'CICERON', filename: filename, user: null, url: fullUrl, encodedUrl: urlencode(fullUrl), req: req, param });

	}
}

module.exports = router;
