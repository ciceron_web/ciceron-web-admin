var express = require('express');
var router = express.Router();
var proxy = require('express-http-proxy');
var request = require('request');
var fs = require('fs');
var urlencode = require('urlencode');

router.get('/access_file/profile_pic/:id1/:id2', function(req, res, next) {
  res.redirect("/api/v2/user/profile/" + req.params.id1 + "/profilePic/" + req.params.id2);
});


//access_file/user/profile/4/profilePic/20170508163333680111.png

router.get('/access_file/user/profile/:id1/profilePic/:id2', function(req, res, next) {
  res.redirect("/api/v2/user/profile/" + req.params.id1 + "/profilePic/" + req.params.id2);
});


module.exports = router;
